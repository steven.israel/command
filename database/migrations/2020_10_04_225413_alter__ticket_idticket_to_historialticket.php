<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTicketIdticketToHistorialticket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historialticket', function (Blueprint $table) {
            $table->foreign('Ticket_idticket')->references('idticket')->on('ticket');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historialticket', function (Blueprint $table) {
            $table->dropForeign('Ticket_idticket');
        });
    }
}
