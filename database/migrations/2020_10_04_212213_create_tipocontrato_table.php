<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipocontratoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipocontrato', function (Blueprint $table) {
            $table->increments('idtipoContrato');
            $table->string('nombre');
            $table->longtext('descripcion');
            $table->integer('estado')->default(1);
            $table->unsignedInteger('prioridadticket_idprioridadticket');
            $table->foreign('prioridadticket_idprioridadticket')->references('idPrioridadTicket')->on('prioridadticket');

            $table->string('color');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipocontrato');
    }
}
