<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanillanotificacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planillanotificacion', function (Blueprint $table) {
            $table->increments('idplantilla_notificacion');
            $table->string('titulo');
            $table->string('subject');
            $table->string('descripcion');
            $table->longtext('content');
            $table->unsignedinteger('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planillanotificacion');
    }
}
