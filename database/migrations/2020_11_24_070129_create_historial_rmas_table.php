<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialRmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_rmas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fecha');
            $table->string('hora');
            $table->text('comentario')->nullable();
            $table->integer('id_estado_anterior');
            $table->integer('id_estado_actual');
            $table->unsignedInteger('id_user');
            $table->unsignedInteger('id_rma');
            $table->foreign('id_user')->references('id')->on('user');
            $table->foreign('id_rma')->references('codigo_rma')->on('rma');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_rmas');
    }
}
