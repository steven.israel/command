<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->increments('idcliente');
            $table->string('nombres')->nullable();
            $table->string('apellidos')->nullable();
            $table->string('email');
            $table->string('telefono');
            $table->string('celular')->nullable();
            $table->text('password');
            $table->string('cargo');
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('idempresa');
            $table->string('avatar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
