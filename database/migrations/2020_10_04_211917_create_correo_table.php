<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorreoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correo', function (Blueprint $table) {
            $table->increments('idCorreo');
            $table->unsignedInteger('id');
            $table->string('fromName');
            $table->string('fromAddres');
            $table->string('subject');
            $table->text('textPlain');
            $table->text('textHtml');
            $table->date('date');
            $table->unsignedInteger('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('correo');
    }
}
