<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketProgramadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_programado', function (Blueprint $table) {
            $table->increments('idprogramado');
            $table->unsignedInteger('idticket');
            $table->longtext('descripcion');
            $table->date('fecha_programacion');
            $table->date('fechaIni')->nullable();
            $table->date('fechaFin')->nullable();
            $table->unsignedInteger('tiempo');
            $table->integer('conteoNotificacion')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_programado');
    }
}
