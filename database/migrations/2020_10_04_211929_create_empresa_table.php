<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('idempresa');
            $table->string('nombre')->nullable();
            $table->string('razon')->nullable();
            $table->string('rut')->nullable();
            $table->integer('telefono1')->nullable();
            $table->integer('telefono2')->nullable();
            $table->string('direccion1')->nullable();
            $table->string('direccion2')->nullable();
            $table->string('email')->nullable();
            $table->string('codigo')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('pais')->nullable();
            $table->string('cp')->nullable();
            $table->string('notas')->nullable();
            $table->integer('ticket')->default(0);
            $table->date('fechaInicio')->nullable();
            $table->date('fechaFin')->nullable();
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('idtipocontrato')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa');
    }
}
