<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('password_reset');
            $table->string('email');
            $table->string('avatar');

            $table->UnsignedInteger('role')->default(2);
            $table->UnsignedInteger('status')->default(2);
            $table->UnsignedInteger('idempresa');

            $table->string('nombres');
            $table->string('apellidos');
            $table->rememberToken();
            $table->date('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
