<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadoticketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estadoticket', function (Blueprint $table) {
            $table->increments('idEstadoTicket');
            $table->string('nombre');
            $table->string('descripcion');
            $table->unsignedInteger('idplantilla');
            $table->char('color',7)->default('#000000');          
            $table->unsignedInteger('tipo_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estadoticket');
    }
}
