<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function (Blueprint $table) {
            $table->increments('idticket');
            $table->string('numeroDeCaso');
            $table->unsignedInteger('PrioridadTicket_idPrioridadTicket');
            $table->foreign('PrioridadTicket_idPrioridadTicket')->references('idPrioridadTicket')->on('prioridadticket');
            $table->unsignedInteger('tipoCierre_idtipoCierre');
   //LLAVE FALLANDO  pero sintaxis buena        $table->foreign('tipoCierre_idtipoCierre')->references('idtipoCierre')->on('tipocierre');
            $table->unsignedInteger('Cliente_idcliente');
            $table->foreign('Cliente_idcliente')->references('idcliente')->on('cliente');
            $table->unsignedInteger('EstadoTicket_idEstadoTicket');
            $table->foreign('EstadoTicket_idEstadoTicket')->references('idEstadoTicket')->on('estadoticket');
            $table->longtext('descripcion');
            $table->string('titulo');
            $table->unsignedInteger('tecnico')->default(0);
            $table->unsignedInteger('comercial')->default(0);
            $table->unsignedInteger('idusuario');
            $table->longtext('comentario');
            $table->unsignedInteger('archivado')->default(0);
            $table->unsignedInteger('tipo_ingreso');
            $table->unsignedInteger('abierto')->default(0);
            $table->unsignedInteger('idingreso');
            $table->unsignedInteger('horas')->default(0);
            $table->unsignedInteger('programado')->default(0);
            $table->unsignedInteger('estadorespuesta')->default(0);
            $table->unsignedInteger('intentosrespuesta')->default(0);
            $table->timestamps();
            $table->unsignedInteger('conteoInicio')->default(0);
            $table->timestamp('inicioTiempo')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));});
    
    
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket');
    }
}
