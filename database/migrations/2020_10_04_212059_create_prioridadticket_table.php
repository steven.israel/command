<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrioridadticketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prioridadticket', function (Blueprint $table) {
            $table->increments('idPrioridadTicket');
            $table->string('nombre')->nullable();
            $table->string('descripcion')->nullable();
            $table->unsignedInteger('tiempoMinimoContacto')->nullable();
            $table->unsignedInteger('tiempoMaximoContacto')->nullable();
            $table->string('color')->nullable();
            $table->unsignedInteger('tiempo_notificacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prioridadticket');
    }
}
