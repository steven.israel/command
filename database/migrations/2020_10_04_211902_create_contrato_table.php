<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrato', function (Blueprint $table) {
            $table->increments('idcontrato');
            $table->date('fechaInicio')->nullable();
            $table->date('fechaFin')->nullable();
            $table->unsignedInteger('TipoContrato_idtipoContrato');
            $table->unsignedInteger('Cliente_idcliente');
            $table->foreign('Cliente_idcliente')->references('idcliente')->on('cliente');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrato');
    }
}
