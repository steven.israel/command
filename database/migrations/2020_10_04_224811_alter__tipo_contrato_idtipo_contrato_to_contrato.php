<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTipoContratoIdtipoContratoToContrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrato', function (Blueprint $table) {
                        //$table->unsignedInteger('TipoContrato_idtipoContrato');
    //NO funciona pero sintaxis correcta
             $table->foreign('TipoContrato_idtipoContrato')->references('idtipoContrato')->on('tipocontrato');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato', function (Blueprint $table) {
            $table->dropForeign('TipoContrato_idtipoContrato');
        });
    }
}
