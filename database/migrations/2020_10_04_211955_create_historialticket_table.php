<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialticketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historialticket', function (Blueprint $table) {
            $table->increments('idHistorialTicket');
            $table->unsignedInteger('Ticket_idticket');
            $table->longText('descripcion');
            $table->unsignedInteger('tipo')->default(1);
            $table->unsignedInteger('usuario');
            $table->unsignedInteger('tipo_usuario')->default(1);
            $table->unsignedInteger('estado_ticket')->default(1);
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historialticket');
    }
}
