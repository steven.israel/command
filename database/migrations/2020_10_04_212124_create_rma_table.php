<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRmaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rma', function (Blueprint $table) {
            $table->increments('codigo_rma');
            $table->date('fecha')->nullable();
            $table->unsignedInteger('idcliente')->nullable();
            $table->longtext('descripcion')->nullable();
            $table->longtext('descripcion_falla')->nullable();
            $table->unsignedInteger('accesorio')->nullable();
            $table->unsignedInteger('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('numero_serie')->nullable();
            $table->string('comentario')->nullable();
            $table->string('describir')->nullable();
            $table->unsignedInteger('tipo_rma')->nullable();
            $table->unsignedInteger('tipo_devolucion')->nullable();
            $table->unsignedInteger('estado_rma')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rma');
    }
}
