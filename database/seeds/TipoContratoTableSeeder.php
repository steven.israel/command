<?php

use App\tipoContrato;
use Illuminate\Database\Seeder;

class TipoContratoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		tipoContrato::create([
			'idtipoContrato' 	=> 1,
			'nombre' 			=> "Sin Contrato",
			'descripcion' 		=> "Cliente que no cuentan con contratos de servicio técnico.",
			'prioridadticket_idprioridadticket' => "1",
			'color' 			=> "#212121"
	 	]);

        tipoContrato::create([
       		'idtipoContrato' 	=> 2,
       		'nombre' 			=> "Bajo",
       		'descripcion' 		=> "Cliente que cuentan con contratos de servicio técnico.",
       		'prioridadticket_idprioridadticket' => "1",
       		'color' 			=> "#212121"
        ]);

        tipoContrato::create([
       		'idtipoContrato' => 3,
       		'nombre' => "Normal",
       		'descripcion' => "Clientes que cuentan con contratos de servicio técnico < USD 1000 mensuales.",
       		'prioridadticket_idprioridadticket' => "2",
       		'color' => "#212121"
        ]);

        tipoContrato::create([
       		'idtipoContrato' => 4,
       		'nombre' => "Alto",
       		'descripcion' => "Clientes con contratos de soporte técnico < USD 1000 mensual.  Clientes que cuenten con sistemas instalados con HA y confirmen falla de la PBX primaria.",
       		'prioridadticket_idprioridadticket' => "3",
       		'color' => "#4651ec"
        ]);
		
		tipoContrato::create([
       		'idtipoContrato' => 5,
       		'nombre' => "Urgente",
       		'descripcion' => "Cliente que tiene prioridad 1, tiempo de gestión 24*7",
       		'prioridadticket_idprioridadticket' => "3",
       		'color' => "#fe0101"
        ]);
    }
}
