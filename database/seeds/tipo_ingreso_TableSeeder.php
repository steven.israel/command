<?php

use App\tipo_ingreso;
use Illuminate\Database\Seeder;

class tipo_ingreso_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        tipo_ingreso::truncate();
         tipo_ingreso::create([
       		'idingreso' => "1",
       		'nombre' => "telefono",
       		'color' => "#212121"
        ]);
         tipo_ingreso::create([
       		'idingreso' => "2",
       		'nombre' => "Correo electronico",
       		'color' => "#43a047"
        ]);

    }
}
