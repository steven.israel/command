<?php

use App\tipocierre;
use Illuminate\Database\Seeder;

class tipocierre_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         tipocierre::create([
       		'idtipoCierre' => "1",
       		'nombre' => "Abierto",
       		'descripcion' => "Los tickets que aun no han sido cerrados."
        ]);
    }
}
