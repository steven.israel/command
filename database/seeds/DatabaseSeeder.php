<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmpresaTableSeeder::class);
        $this->call(PrioridadTicketTableSeeder::class);
        $this->call(TipoContratoTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(tipo_ingreso_TableSeeder::class);
        $this->call(estadoticket_TableSeeder::class);
        $this->call(tipocierre_TableSeeder::class);

       
    }
}
