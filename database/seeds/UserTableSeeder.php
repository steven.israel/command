<?php

use App\usuario;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		usuario::create([
       		'username' => "admin",
       		'password' => bcrypt('12345678'),
       		'email' => "admin@gmail.com",
       		'avatar' => "fav.png",
       		'role' => "1",
       		'status' => "1",
       		'idempresa' => "1",
       		'nombres' => "Admin",
       		'apellidos' => "Sistemas"
		]);
		   
		usuario::create([
			'username' => "moderador",
			'password' => bcrypt('12345678'),
			'email' => "moderador@gmail.com",
			'avatar' => "fav.png",
			'role' => "2",
			'status' => "1",
			'idempresa' => "2",
			'nombres' => "moderador",
			'apellidos' => "Sistemas"
		]);

		usuario::create([
       		'username' => "nivel 1",
       		'password' => bcrypt('12345678'),
       		'email' => "nivel1@gmail.com",
       		'avatar' => "fav.png",
       		'role' => "3",
       		'status' => "2",
       		'idempresa' => "1",
       		'nombres' => "nivel 1",
       		'apellidos' => "Sistemas"
       	]);

		usuario::create([
       		'username' => "nivel 2",
       		'password' => bcrypt('12345678'),
       		'email' => "nsxavi040jp.jp@gmail.com",
       		'avatar' => "fav.png",
       		'role' => "4",
       		'status' => "1",
       		'idempresa' => "1",
       		'nombres' => "Nivel 4",
       		'apellidos' => "Sistemas"
       	]);

		usuario::create([
       		'username' => "nivel 3",
       		'password' => bcrypt('12345678'),
       		'email' => "nivel3@gmail.com",
       		'avatar' => "fav.png",
       		'role' => "5",
       		'status' => "1",
       		'idempresa' => "1",
       		'nombres' => "Nivel 3",
       		'apellidos' => "Sistemas"
       	]);
    }
}
