<?php

use App\prioridad;
use Illuminate\Database\Seeder;

class PrioridadTicketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
      
     //        prioridad::truncate();
			 prioridad::create([

       		'idPrioridadTicket' => "1",
       		'nombre' => "Bajo",
       		'descripcion' => "Clientes que no cuentan con contratos de servicio tecnico..",
       		'tiempoMinimoContacto' => "0",
       		'tiempoMaximoContacto' => "24",
       		'color' => "#808080",
       		'tiempo_notificacion' => "6"
        ]);

			 		 prioridad::create([

       		'idPrioridadTicket' => "2",
       		'nombre' => "Normal",
       		'descripcion' => "Clientes que cuenta con contratos de servicio < $1000 mensuales",
       		'tiempoMinimoContacto' => "0",
       		'tiempoMaximoContacto' => "24",
       		'color' => "#368c2c",
       		'tiempo_notificacion' => "4"
        ]);

			 			 			 prioridad::create([

       		'idPrioridadTicket' => "3",
       		'nombre' => "Alto",
       		'descripcion' => "Clientes con contratos de soporte tecnico < $1000 mensuales. Clientes que cuentan con sistemas isntalados con HA y confirmen falla de la PBX primaria.",
       		'tiempoMinimoContacto' => "0",
       		'tiempoMaximoContacto' => "17",
       		'color' => "#ffbe16",
       		'tiempo_notificacion' => "2"
        ]);
        
 
    
}   
}
