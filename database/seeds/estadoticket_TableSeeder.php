<?php

use App\estadoticket;
use Illuminate\Database\Seeder;

class estadoticket_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           estadoticket::create([
          'idEstadoTicket' => "1",
          'nombre' => "Ingresado",
          'descripcion' => "Refiere a todos los reclamos de clientes que tienen contrato de soporte técnico, los cuales fueron ingresado en SIGAC, se le asignó un nivel de prioridad, un número de identificación y se designa automáticamente a un técnico.",
          'idplantilla' => "2",
          'color' => "#1976d2",
          'tipo_estado' => "1"
        ]);
           estadoticket::create([
          'idEstadoTicket' => "2",
          'nombre' => "En revisión",
          'descripcion' => "Reclamos de clientes sin soporte técnico que fue ingresado en SIGAC, se le asignó un nivel de prioridad baja, un número de identificación y se deriva a un técnico para analizar el nivel de horas que requiere de soporte para su solución. ",
          'idplantilla' => "6",
          'color' => "#f9a825",
          'tipo_estado' => "1"
        ]);
           estadoticket::create([
          'idEstadoTicket' => "3",
          'nombre' => "En proceso de resolución",
          'descripcion' => "Hace referencia a un ticket que fue bloqueado por un técnico para su resolución.",
          'idplantilla' => "4",
          'color' => "#43a047",
          'tipo_estado' => "1"
        ]);
           estadoticket::create([
          'idEstadoTicket' => "5",
          'nombre' => "Cerrado",
          'descripcion' => "Hace referencia a un ticket que ya fue tratado, solucionado y/o resuelto y que luego fue cerrado.",
          'idplantilla' => "18",
          'color' => "#d32f2f",
          'tipo_estado' => "1"
        ]);

                      estadoticket::create([
          'idEstadoTicket' => "6",
          'nombre' => "Re Abierto",
          'descripcion' => "Hace referencia a un ticket que ya fue tratado, solucionado y/o resuelto y que luego fue abierto.",
          'idplantilla' => "18",
          'color' => "#00796b",
          'tipo_estado' => "1"
        ]);
           estadoticket::create([
          'idEstadoTicket' => "7",
          'nombre' => "Cierre Satisfactorio",
          'descripcion' => "El ticket fue resuelto satisfactoriamente, dando
			resolución al pedido de soporte técnico.",
          'idplantilla' => "1",
          'color' => "#008744",
          'tipo_estado' => "2"
        ]);
                      estadoticket::create([
          'idEstadoTicket' => "8",
          'nombre' => "Cierre sin Resolución",
          'descripcion' => "La resolución del ticket no es viable debido a que la
			misma depende del fabricante del producto (sea HW
			o SW).",
          'idplantilla' => "2",
          'color' => "#0057e7",
          'tipo_estado' => "2"
        ]);
           estadoticket::create([
          'idEstadoTicket' => "9",
          'nombre' => "Cierre por Falta de Respuesta",
          'descripcion' => "El ticket no fue resuelto debido a que el cliente no
			remitió la información requerida.",
          'idplantilla' => "2",
          'color' => "#ffa700",
          'tipo_estado' => "2"
        ]);
                      estadoticket::create([
          'idEstadoTicket' => "10",
          'nombre' => "Cierre por Rechazo de Presupuesto",
          'descripcion' => "El cliente no aceptó el costo del servicio.",
          'idplantilla' => "2",
          'color' => "#d62d20",
          'tipo_estado' => "2"
        ]);
           estadoticket::create([
          'idEstadoTicket' => "11",
          'nombre' => "Cierre Problema Externo",
          'descripcion' => "La resolución del ticket trasciende los servicio de
			soporte técnico brindado por Commandline, al no
			estar asociados al producto y/o solución
			comercializada. (Ej. Un firewall mal configurado.)",
          'idplantilla' => "1",
          'color' => "#d62d20",
          'tipo_estado' => "2"
        ]);

    }
}
