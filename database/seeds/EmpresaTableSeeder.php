<?php

use App\empresa;
use Illuminate\Database\Seeder;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()  {
        

        empresa::create([
          'nombre' => "Sin empresa",
          'razon' => "razon",
          'rut' => "ruta",
          'telefono1' => "111111111",
          'telefono2' => "222222222",
          'direccion1' => "empresa",
          'direccion2' => "empresa",
          'email' => "sinempresa@gmail.com",
          'ciudad' => "San sivar",
          'pais' => "el salvador",
          'cp' => "11800",
          'notas' => "prueba de notas",
          'fechaInicio' => "2020-01-01",
          'fechaFin' => "2020-01-20",
          'estado' => "1",
          'idtipocontrato' => "1"
        ]);



        empresa::create([
          'nombre' => "empresa prueba",
          'razon' => "razon",
          'rut' => "ruta",
          'telefono1' => "111111111",
          'telefono2' => "222222222",
          'direccion1' => "empresa",
          'direccion2' => "empresa",
          'email' => "empresaprueba@gmail.com",
          'ciudad' => "San sivar",
          'pais' => "el salvador",
          'cp' => "11800",
          'notas' => "prueba de notas",
          'fechaInicio' => "2020-01-01",
          'fechaFin' => "2020-01-20",
          'estado' => "1",
          'idtipocontrato' => "2"
        ]);



        empresa::create([
          'nombre' => "Empresa 3",
          'razon' => "razon",
          'rut' => "ruta",
          'telefono1' => "111111111",
          'telefono2' => "222222222",
          'direccion1' => "empresa",
          'direccion2' => "empresa",
          'email' => "nike@gmail.com",
          'ciudad' => "San sivar",
          'pais' => "el salvador",
          'cp' => "11800",
          'notas' => "prueba de notas",
          'fechaInicio' => "2020-01-01",
          'fechaFin' => "2020-01-20",
          'estado' => "1",
          'idtipocontrato' => "3"
        ]);
 
}

    }

