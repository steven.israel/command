<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Nos mostrará el formulario de login.
Route::get('/', 'Auth\LoginController@showLogin');

// Validamos los datos de inicio de sesión.
Route::post('/validar', 'Auth\LoginController@validarLogin');

    // Esta será nuestra ruta de bienvenida.
Route::get('dashboard', 'DashboardController@index');
Route::get('/busqueda/{busqueda}', 'DashboardController@busquedaTicket');
Route::get('/resultados', 'DashboardController@busqueda')->name('ticket.busqueda');


    // Esta será nuestra ruta de bienvenida.
Route::post('dashboard', 'DashboardController@indexFecha');
    // Esta ruta nos servirá para cerrar sesión.
Route::get('logout', 'Auth\LoginController@logOut');

    //Esta ruta es para crear cliente
    Route::resource('cliente', 'ClienteController');
    //ruta para desactivar y activar clientes
    Route::get('cliente/desactivar/{id}', 'ClienteController@desactivar');
    Route::get('cliente/activar/{id}', 'ClienteController@activar');

    //Rutas para marcas
    Route::resource('marca', 'MarcaController');
    //ruta para desactivar y activar marca
    Route::get('marca/desactivar/{id}', 'MarcaController@desactivar');
    Route::get('marca/activar/{id}', 'MarcaController@activar');
    Route::get('marca/validateNombre/{nombre}', 'MarcaController@validateNombre');

    //Esta ruta es para crear rma
    Route::resource('rma', 'RmaController');
    Route::get('/archivadosrma', 'RmaController@indexArchivados')->name('archivados');
    //Esta ruta es para crear rma
    Route::get('rma/historial/{id}', 'RmaController@historial');
    //ruta para formulario de entrada
    Route::get('entrada', 'RmaController@createE');
    //ruta para formulario de salida
    Route::get('salida', 'RmaController@createS');

    //Rutas para usuarios
    Route::resource('usuario', 'UsuarioController');
    //ruta para desactivar y activar usuario
    Route::get('usuario/desactivar/{id}', 'UsuarioController@desactivar');
    Route::get('usuario/activar/{id}', 'UsuarioController@activar');
    Route::get('usuario/validateUsername/{username}', 'UsuarioController@validateUsername');
    Route::get('usuario/validateCorreo/{email}', 'UsuarioController@validateCorreo');

    //Rutas para empresas
    Route::resource('empresa', 'EmpresaController');
    //ruta para desactivar y activar empresa
    Route::get('empresa/desactivar/{id}', 'EmpresaController@desactivar');
    Route::get('empresa/activar/{id}', 'EmpresaController@activar');
    Route::get('empresa/validateNombre/{nombre}', 'EmpresaController@validateNombre');


    //Esta ruta es para crear prioridades
    Route::resource('prioridad', 'PrioridadController');
    Route::get('prioridad/validateNombre/{nombre}', 'PrioridadController@validateNombre');

    //Esta ruta es para crear tipos de contrato
    Route::resource('tipoContrato', 'TipoContratoController');
    //ruta para desactivar y activar empresa
    Route::get('tipoContrato/desactivar/{id}', 'TipoContratoController@desactivar');
    Route::get('tipoContrato/activar/{id}', 'TipoContratoController@activar');
    Route::get('tipoContrato/validateNombre/{nombre}', 'TipoContratoController@validateNombre');

    //Esta ruta es para crear contratos
Route::resource('contrato', 'ContratoController');
    //Esta ruta es para crear Ticekts
Route::resource('ticket', 'TicketController');

Route::get('/seleccionarCliente/{id}/{emmpresa}', 'ClienteController@seleccion');
Route::get('/unSeleccionarCliente/{id}/{empresa}', 'ClienteController@unSeleccion');
Route::delete('/eliminar_ticket/{id}', 'TicketController@eliminarTicket')->name('ticket.eliminar');
Route::get('/RecordatorioCorreo', 'MailRememberController@recordarRespuesta');//ruta para correos persistentes de recordatorio de respuesta
    //Ruta para obtener todos los mails ingresados por correo
    Route::get('/mails', 'TicketController@tickets_mails');

    //Ruta para obtener todos los mails ingresados por correo
    Route::get('/todos', 'TicketController@allTickets');
        //Esta ruta es para ver los tickets ingresados validados
    Route::get('/ingresados', 'TicketController@tickets_ingresados');
        //Esta ruta es para ver los tickets ingresados que estan en revision
    Route::get('/revision', 'TicketController@tickets_revision');
        //Esta ruta es para ver los tickets ingresados que estan en revision
    Route::get('/proceso', 'TicketController@tickets_proceso');
        //Esta ruta es para ver los tickets ingresados que estan en revision
    Route::get('/respuesta', 'TicketController@tickets_respuesta');
        //Esta ruta es para ver los tickets ingresados que estan en revision
    Route::get('/cerrado', 'TicketController@tickets_cerrado');
    // tickets re abiertos
    Route::get('/re-abierto', 'TicketController@tickets_reabierto');

    //Esta ruta es para crear Ticekts
Route::post('/estadoTicket', 'TicketController@updateHistorico');
    //Esta ruta es para ver los tickets archivados
Route::get('/ticketArchivados', 'TicketController@archivados');
    //Esta ruta es para ver los tickets ingresados por correo
Route::get('/ticketMail', 'TicketController@indexMails');
    //Ruta para validar ticket desde el correo
Route::get('validar/{id}', 'TicketController@validar');
//valida i el ticketya esta siendo trabajado
Route::get('trabajoTicket/{id}', 'TicketController@insertarTiempoInicio');
    //Ruta para enviar correos
Route::get('mail/send', 'TicketController@send');
    //Ruta para enviar correos
Route::get('mail', 'TicketControllmailer@send1');
    //Ruta para enviar correos
Route::get('send', 'TicketController@enviar');
    // ruta para exportar pdf
Route::get('/customers/pdf','ClienteController@export_pdf');
    // ruta para obtener el listado de notificaciones
Route::get('listaNotificaciones', 'TicketController@notificaciones');

    // ruta para obtener el listado de ticket programados
Route::get('listaProgramados', 'RecordarNotificacionesController@programados');
    // ruta para obtener el listado de ticket programados por frecuencia
Route::get('/ticketProgramado', 'RecordarNotificacionesController@ticketFrecuencia');
    // ruta para obtener el listado de ticket programados
Route::get('listadoAbiertos', 'TicketController@listaAbiertos');
Route::get('/actualizarVistos', 'TicketController@actualizarVisto');

Route::get('notificacionesSistema', 'TicketController@notificacionesSistema');
Route::get('/notificacionVista/{id}', 'TicketController@actualizarNotificacionesVista');
Route::get('/notificacionVistaTecnico/{id}', 'TicketController@actualizarNotificacionesVistaTecnico');

    // ruta para actualizar el tiempo
Route::get('updateTiempo/{id}/{tiempo}', 'TicketController@updateTiempo');

Route::get('select_clientes/{id}', 'TicketController@clientes');
Route::get('select_info/{id}', 'TicketController@cliente_info');



Auth::routes();


