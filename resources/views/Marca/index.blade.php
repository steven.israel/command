@extends('Layout.layout')
@section('content')
<!-- BEGIN BASE-->
<div id="base">
<div class="row">
  <section class="content" id="content">
  @if(Session::has('success'))
        <div class="alert alert-info">
          {{Session::get('success')}}
        </div>
      @endif
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
              <div class="pull-left"><h3>Lista de Marcas</h3></div>
              <div class="pull-right">
                <div class="btn-group">
                  <a href="{{ route('marca.create') }}" class="btn btn-info" >Crear Marca</a>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="">
                <div class="table-responsive">
                  <table id="example" class="display nowrap" width="100%">
                    <thead>
                      <th>Nombres</th>
                      <th>Estado</th>
                      <th class="no-sort">Acciones</th>
                    </thead>
                    <tbody>
                      @if($marcas->count())
                        @foreach($marcas as $marca)
                          <tr>
                            <td id="marca-{{$marca->idmarca}}">{{$marca->nombre}}</td>
                            <td width="50%">
                              @if($marca->estado == 1)
                                Activo
                              @else
                                Inactivo
                              @endif
                            </td>
                            <td>
                              <div class="row">
                                <div class="col-md-4 col-xs-4">
                                  <a class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Editar Prioridad" href="{{action('MarcaController@edit', $marca->idmarca)}}" ><span class="glyphicon glyphicon-pencil"></span></a>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                  @if($marca->estado == 1)
                                    <a onclick="act_inac({{$marca->idmarca}},0)" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Desactivar Marca">
                                      <span class="md md-block"></span>
                                    </a>
                                  @else
                                    <a onclick="act_inac({{$marca->idmarca}},1)" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Activar Marca">
                                      <span class="md md-check"></span>
                                    </a>
                                  @endif
                                </div>
                                @if($marca->is_delete())
                                <div class="col-md-4 col-xs-4">
                                  <form action="{{action('MarcaController@destroy', $marca->idmarca)}}" onclick="borrar(event,this)" method="post">
                                    {{csrf_field()}}
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar Marca" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                  </form>
                                </div>
                                @endif
                              </div>
                            </td>
                          </tr>
                        @endforeach
                      @else
                        <tr>
                          <td colspan="8">No hay registro !!</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script>
  $('#example').DataTable( {
      dom: 'Bfrtip',
      buttons: [],
      "columnDefs": [{ targets: 'no-sort', orderable: false }],
      "language": {
        "search": "Buscar:",
        paginate: {
          next: '&#8594;', // or '→'
          previous: '&#8592;' // or '←'
        }
      },
      "order": [[ 0, "desc" ]]
    } );

  function borrar(e,form){
    e.preventDefault();
    swal({
      title: "Confirmar eliminación de registro?",
      icon: "warning",
      buttons: ["Confirmar", "Cancelar"],
      dangerMode: true,
    }).then((willDelete) => {
      if (!willDelete) {
        form.submit();
      } else {
        return false;
      }
    });
  }

  function act_inac(id,value){
    var title = 'Activar';
    var url   = 'activar';
    if (!value) {
      title = 'Desactivar';
      url   = 'desactivar';
    }
    swal({
      title: 'Está usted seguro de '+title+' la marca "'+ $('#marca-'+id).text() +'"?',
      icon: "warning",
      buttons: ["Confirmar", "Cancelar"],
      dangerMode: true,
    }).then((willDelete) => {
      if (!willDelete) {
        window.location.href = "/marca/"+url+'/'+id;
      } else {
        return false;
      }
    });
  }
</script>
@endsection
