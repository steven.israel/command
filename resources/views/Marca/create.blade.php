@extends('Layout.layout')
@section('content')
<div id="base">
<div class="row">
	<section class="content" id="content">

		<div class="col-md-8 col-md-offset-2">
			<div class="section-header">
	            <ol class="breadcrumb">
	               <li><a href="{{ route('marca.index') }}">Lista de Marcas</a></li>
	              <li class="active">Nueva Marca</li>
	            </ol>
          	</div>
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Nueva Marca</h3>
				</div>
				<div class="panel-body">
					<div class="table-container">
						<form method="POST" action="{{ route('marca.store') }}"  role="form" id="accion_form">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Nombre</label>
										<input type="text" name="nombre" id="nombre" value="{{ old('nombre') }}" class="form-control input-sm" >
										<p style="color: red;" id="text-nombre">{{ $errors->first('nombre') }}</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label for="estado">Estado</label>
										<select id="estado" name="estado" class="form-control">
											<option value="1">Activo</option>
											<option value="2">Inactivo</option>
										</select>
									</div>
								</div>
								<p style="color: red;">{{ $errors->first('estado') }}</p>
							</div>

							<div class="row" align="center">

								<div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
									<input type="submit"  value="Guardar" class="btn btn-success btn-block">
								</div>

							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	$("#nombre").on("keyup", function(){
		if ($(this).val() == "") {
			$('#text-nombre').text('Campo requerido')
		}else{
			$('#text-nombre').text('');
			$.ajax({
				type: 'GET',
				url: '/marca/validateNombre/'+$('#nombre').val(),
				data: $(this).serialize(),
				success: function(data) {
					if (data == 1){
						$('#text-nombre').text('La marca ya existe');
					}else{
						$('#text-nombre').text('');
					}
				}
			});
		}
	});
</script>

<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
@endsection
