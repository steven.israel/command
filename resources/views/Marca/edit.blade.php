@extends('Layout.layout')
@section('content')
<div id="base">
    <div class="row">
        <section class="content" id="content">

            <div class="col-md-8 col-md-offset-2">
                <div class="section-header">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('prioridad.index') }}">Lista de Marcas</a></li>
                        <li class="active">Nueva Marca</li>
                    </ol>
                </div>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Error!</strong> Revise los campos obligatorios.<br>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    {{Session::get('success')}}
                </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Nueva Marca</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-container">
                            <form method="POST" action="{{ route('marca.update',$marca->idmarca) }}" role="form"
                                id="accion_form">
                                {{ csrf_field() }}
                                <input name="_method" type="hidden" value="PATCH">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input type="text" name="nombre" value="{{ $marca->nombre }}"
                                                class="form-control input-sm">
                                            <p style="color: red;">{{ $errors->first('nombre') }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="estado">Estado</label>
                                            <select id="estado" name="estado" class="form-control">
                                                <option value="1" <?php if ($marca->estado == 1): ?> selected
                                                    <?php endif ?>>Activo</option>
                                                <option value="2" <?php if ($marca->estado == 2): ?> selected
                                                    <?php endif ?>>Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <p style="color: red;">{{ $errors->first('estado') }}</p>
                                </div>

                                <div class="row" align="center">

                                    <div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
                                        <input type="submit" value="Guardar" class="btn btn-success btn-block">
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
    @endsection
