@extends('Layout.layout')
@section('content')
<!-- BEGIN BASE-->
<div id="base">
    <div class="row">
        <section class="content" id="content">
            @if(Session::has('success'))
            <div class="alert alert-info">
                {{Session::get('success')}}
            </div>
            @endif
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <h3>Lista de Prioridades</h3>
                                </div>
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <a href="{{ route('prioridad.create') }}" class="btn btn-info">Crear
                                            Prioridad</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-container">
                                    <div class="table-responsive">
                                        <table id="mytable" class="table table-bordred table-striped">
                                            <thead>
                                                <th>Color</th>
                                                <th>Nombres</th>
                                                <th>Descripción</th>
                                                <th>Tiempo Mínimo de Contacto</th>
                                                <th>Tiempo Máximo de Contacto</th>
                                                <th>Tiempo de Notificación</th>
                                            </thead>
                                            <tbody>
                                                @if($prioridades->count())
                                                @foreach($prioridades as $prioridad)
                                                <tr>
                                                    <td><span
                                                            style="width: 50px; height: 20px; background: {{$prioridad->color}}; padding: 2%;">&nbsp;
                                                            &nbsp; &nbsp; &nbsp;</span></td>
                                                    <td>{{$prioridad->nombre}}</td>
                                                    <td width="50%">{{$prioridad->descripcion}}</td>
                                                    <td>{{$prioridad->tiempoMinimoContacto}}</td>
                                                    <td>{{$prioridad->tiempoMaximoContacto}}</td>
                                                    <td>{{$prioridad->tiempo_notificacion}}</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-4 col-xs-4">
                                                                <a class="btn btn-primary btn-xs" data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    data-original-title="Editar Prioridad"
                                                                    href="{{action('PrioridadController@edit', $prioridad->idPrioridadTicket)}}"><span
                                                                        class="glyphicon glyphicon-pencil"></span></a>
                                                            </div>
                                                            <div class="col-md-4 col-xs-4">
                                                                @if($prioridad->is_delete())
                                                                <form
                                                                    action="{{action('PrioridadController@destroy', $prioridad->idPrioridadTicket)}}"
                                                                    onclick="borrar(event,this)" method="post">
                                                                    {{csrf_field()}}
                                                                    <input name="_method" type="hidden" value="DELETE">
                                                                    <button class="btn btn-danger btn-xs"
                                                                        data-toggle="tooltip" data-placement="top"
                                                                        data-original-title="Eliminar Prioridad"
                                                                        type="submit"><span
                                                                            class="glyphicon glyphicon-trash"></span></button>
                                                                </form>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="8">No hay registro !!</td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                {{ $prioridades->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection

@section('script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script>
    function borrar(e,form){
    e.preventDefault();
    swal({
      title: "Confirmar eliminación de registro?",
      icon: "warning",
      buttons: ["Confirmar", "Cancelar"],
      dangerMode: true,
    }).then((willDelete) => {
      if (!willDelete) {
        form.submit();
      } else {
        return false;
      }
    });
  }

  function act_inac(id,value){
    var title = 'Activar';
    var url   = 'activar';
    if (!value) {
      title = 'Desactivar';
      url   = 'desactivar';
    }
    swal({
      title: 'Está usted seguro de '+title+' al usuario "'+ $('#nombre-'+id).text() +'"?',
      icon: "warning",
      buttons: ["Confirmar", "Cancelar"],
      dangerMode: true,
    }).then((willDelete) => {
      if (!willDelete) {
        window.location.href = "/prioridad/"+url+'/'+id;
      } else {
        return false;
      }
    });
  }
</script>
@endsection
