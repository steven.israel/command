@extends('Layout.layout')
@section('content')
<div id="base">
    <div class="row">
        <section class="content" id="content">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-header">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('prioridad.index') }}">Lista de Prioridades</a></li>
                        <li class="active">Actualizar Prioridad</li>
                    </ol>
                </div>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Error!</strong> Revise los campos obligatorios.<br>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    {{Session::get('success')}}
                </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Actualizar Prioridad</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-container">
                            <form method="POST" action="{{ route('prioridad.update',$prioridad->idPrioridadTicket) }}"
                                role="form" id="accion_form">
                                {{ csrf_field() }}
                                <input name="_method" type="hidden" value="PATCH">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input type="text" name="nombre" id="nombre" value="{{$prioridad->nombre}}"
                                                class="form-control input-sm" placeholder="Nombre">
                                            <p style="color: red;">{{ $errors->first('nombre') }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label>Color</label>
                                            <input type="color" name="color" id="color" value="{{$prioridad->color}}"
                                                class="form-control input-sm" placeholder="Color">
                                            <p style="color: red;">{{ $errors->first('color') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <textarea name="descripcion" class="form-control input-sm"
                                        placeholder="Descripcion">{{$prioridad->descripcion}}</textarea>
                                    <p style="color: red;">{{ $errors->first('descripcion') }}</p>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label>Tiempo Mínimo de Contacto <small>(Horas)</small></label>
                                            <input type="numer" name="tiempoMinimoContacto"
                                                value="{{$prioridad->tiempoMinimoContacto}}" id="edicion"
                                                class="form-control input-sm" placeholder="Tiempo Mínimo de Contacto">
                                            <p style="color: red;">{{ $errors->first('tiempoMinimoContacto') }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label>Tiempo Máximo de Contacto <small>(Horas)</small></label>
                                            <input type="number" name="tiempoMaximoContacto"
                                                value="{{$prioridad->tiempoMaximoContacto}}" id="edicion"
                                                class="form-control input-sm" placeholder="Tiempo Máximo de Contacto">
                                            <p style="color: red;">{{ $errors->first('tiempoMaximoContacto') }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label>Tiempo de notificación <small>(Horas)</small> </label>
                                            <input type="number" name="tiempo_notificacion"
                                                value="{{ $prioridad->tiempo_notificacion }}"
                                                class="form-control input-sm">
                                            <p style="color: red;">{{ $errors->first('tiempo_notificacion') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
                                        <input type="submit" value="Guardar" class="btn btn-success btn-block">
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
    @endsection
