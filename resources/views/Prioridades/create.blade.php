@extends('Layout.layout')
@section('content')
<div id="base">
<div class="row">
	<section class="content" id="content">

		<div class="col-md-8 col-md-offset-2">
			<div class="section-header">
	            <ol class="breadcrumb">
	               <li><a href="{{ route('prioridad.index') }}">Lista de Prioridades</a></li>
	              <li class="active">Nueva Prioridad</li>
	            </ol>
          	</div>
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Nueva Prioridad</h3>
				</div>
				<div class="panel-body">
					<div class="table-container">
						<form method="POST" action="{{ route('prioridad.store') }}"  role="form" autocomplete="off" id="accion_form">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Nombre</label>
										<input type="text" name="nombre" id="nombre" value="{{ old('nombre') }}" class="form-control input-sm" >
										<p style="color: red;" id="text-nombre">
										@if($errors->first('nombre'))
											{{ $errors->first('nombre') }}
										@else
											Campo Requerido
										@endif
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Color</label>
										<input type="color" name="color" value="{{ old('color') }}"  class="form-control input-sm" >
										<p style="color: red;">{{ $errors->first('color') }}</p>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Descripción</label>
								<textarea name="descripcion" class="form-control input-sm" placeholder="Descripcion">{{ old('descripcion') }} </textarea>
								<p style="color: red;">
									@if($errors->first('descripcion'))
										{{ $errors->first('descripcion') }}
									@else
										Campo Requerido
									@endif
								</p>
							</div>
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Tiempo Mínimo de Contacto <small>(Horas)</small></label>
										<input type="numer" name="tiempoMinimoContacto" value="{{ old('tiempoMinimoContacto') }}" class="form-control input-sm">
										<p style="color: red;">
											@if($errors->first('tiempoMinimoContacto'))
												{{ $errors->first('tiempoMinimoContacto') }}
											@else
												Campo Requerido
											@endif
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Tiempo Máximo de Contacto <small>(Horas)</small></label>
										<input type="number" name="tiempoMaximoContacto" value="{{ old('tiempoMaximoContacto') }}" class="form-control input-sm">
										<p style="color: red;">
											@if($errors->first('tiempoMaximoContacto'))
												{{ $errors->first('tiempoMaximoContacto') }}
											@else
												Campo Requerido
											@endif
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Tiempo de notificación <small>(Horas)</small> </label>
										<input type="number" name="tiempo_notificacion" value="{{ old('tiempoMaximoContacto') }}" class="form-control input-sm">
										<p style="color: red;">
											@if($errors->first('tiempo_notificacion'))
												{{ $errors->first('tiempo_notificacion') }}
											@else
												Campo Requerido
											@endif
										</p>
									</div>
								</div>
							</div>
							<div class="row" align="center">

								<div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
									<input type="submit"  value="Guardar" class="btn btn-success btn-block">
								</div>

							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>
	</div>
	@endsection

@section('script')
<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script>
	$("#nombre").on("keyup", function(){
		if ($(this).val() == "") {
			$('#text-nombre').text('Campo requerido')
		}else{
			$('#text-nombre').text('');
			$.ajax({
				type: 'GET',
				url: '/prioridad/validateNombre/'+$('#nombre').val(),
				data: $(this).serialize(),
				success: function(data) {
					if (data == 1){
						$('#text-nombre').text('La prioridad ya existe');
					}else{
						$('#text-nombre').text('');
					}
				}
			});
		}
	});
</script>
@endsection
