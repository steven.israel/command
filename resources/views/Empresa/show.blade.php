@extends('Layout.layout')
@section('content')
<!-- BEGIN BASE-->
<div id="base">
<div class="row">
  <section class="content" id="content">
  @if(Session::has('success'))
        <div class="alert alert-info">
          {{Session::get('success')}}
        </div>
      @endif
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
              <div class="pull-left"><h3>Comentarios internos empresa: {{ $empresa->nombre }}</h3>

              </div>
              <div class="pull-right">

              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="table-container">
                <div class="table-responsive">
                  <table id="example" class="display nowrap" style="width: 100%;">
                  <thead>
                      <th>Comentario</th>
                      <th>Usuario</th>
                      <th>Fecha Registro</th>
                  </thead>
                  <tbody>
                        @foreach($empresa->historial as $historial)
                        <tr>
                        <td>{{$historial->comentario}}</td>
                        <td>{{$historial->user->nombres}} {{$historial->user->apellidos}}</td>
                        <td>{{$historial->created_at}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
      </div>
      </div>
    </div>
  </div>
</section>
</div>

@endsection

@section('script')
<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            "paging": true,
            "ordering": false,
            "info":     true,
            buttons: [],
            "language": {
              "search": "Buscar:",
              paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
              }
            },
            "order": [[ 0, "desc" ]]
        });
    });
</script>
@endsection
