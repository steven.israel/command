<!DOCTYPE html>
<html lang="en">

<head>
    <title>Commandline</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->
    @include('../Layout/links')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

</head>

<body class="menubar-hoverable header-fixed menubar-pin ">

    @include('../Layout/navbar');
    <!-- BEGIN BASE-->
    <div id="base">
        <div class="row">
            <section class="content" id="content">
                @if(Session::has('success'))
                <div class="alert alert-info">
                    {{Session::get('success')}}
                </div>
                @endif
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <h3>Lista de empresas</h3>
                                    </div>
                                    @if(auth()->user()->role != 3 )
                                    <div class="pull-right">
                                        <div class="btn-group">
                                            <a href="{{ route('empresa.create') }}" class="btn btn-info">Crear
                                                Empresa</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table id="example" class="display nowrap">
                                            <thead>
                                                <th>ID Cliente</th>
                                                <th>Nombre</th>
                                                <th>Razon Social</th>
                                                <th>Telefonos</th>
                                                <th>Correo</th>
                                                <th>Tipo Contrato</th>
                                                <th># ticket</th>
                                                <th>Fecha Caducidad</th>
                                                <th class="no-sort">Acciones</th>
                                            </thead>
                                            <tbody>
                                                @if($empresas->count())
                                                @foreach($empresas as $empresa)
                                                <tr>
                                                    <td>{{ $empresa->idempresa }}</td>
                                                    
                                                    <td id="empresa-{{ $empresa->idempresa }}">{{$empresa->nombre}}</td>
                                                    <td>{{$empresa->razon}}</td>
                                                    <td>{{$empresa->telefono1}}-{{$empresa->telefono2}}</td>
                                                    <td>{{$empresa->email}}</td>
                                                    <td>{{$empresa->tipoContrato->nombre}}</td>
                                                    <td>{{$empresa->ticket}}</td>
                                                    <td>{{ ($empresa->idtipocontrato == 1) ? 'Sin Definir' : date('d-M-Y',strtotime($empresa->fechaFin)) }}
                                                    </td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3 col-xs-3">
                                                                <a class="btn btn-primary btn-xs" data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    data-original-title="Ver Comentarios"
                                                                    href="{{action('EmpresaController@show', $empresa->idempresa)}}"><span
                                                                        class="glyphicon glyphicon-eye-open"></span></a>
                                                            </div>
                                                            @if(auth()->user()->role != 3 )
                                                            <div class="col-md-3 col-xs-3">
                                                                <a class="btn btn-primary btn-xs" data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    data-original-title="Editar Empresa"
                                                                    href="{{action('EmpresaController@edit', $empresa->idempresa)}}"><span
                                                                        class="glyphicon glyphicon-pencil"></span></a>
                                                            </div>
                                                            @endif
                                                            @if($empresa->is_delete() && auth()->user()->role == 1 )
                                                            <div class="col-md-3 col-xs-3">

                                                                <form
                                                                    action="{{action('EmpresaController@destroy', $empresa->idempresa)}}"
                                                                    onclick="borrar(event,this)" method="post">
                                                                    {{csrf_field()}}
                                                                    <input name="_method" type="hidden" value="DELETE">
                                                                    <button class="btn btn-danger btn-xs"
                                                                        data-toggle="tooltip" data-placement="top"
                                                                        data-original-title="Eliminar Empresa"
                                                                        type="submit"><span
                                                                            class="glyphicon glyphicon-trash"></span></button>
                                                                </form>

                                                            </div>
                                                            @endif
                                                            <div class="col-md-3 col-xs-3">
                                                                @if($empresa->estado == 1)
                                                                <a onclick="act_inac({{$empresa->idempresa}},0)"
                                                                    class="btn btn-primary btn-xs" data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    data-original-title="Desactivar Empresa">
                                                                    <span class="md md-block"></span>
                                                                </a>
                                                                @else
                                                                <a onclick="act_inac({{$empresa->idempresa}},1)"
                                                                    class="btn btn-primary btn-xs" data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    data-original-title="Activar Empresa">
                                                                    <span class="md md-check"></span>
                                                                </a>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="8">No hay registros!!</td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        @include('../Layout/menu')
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
    <script src="{{ asset('js/libs/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/libs/spin.js/spin.min.js')}}"></script>
    <script src="{{ asset('js/libs/autosize/jquery.autosize.min.js')}}"></script>
    <script src="{{ asset('js/libs/moment/moment.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.time.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.resize.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.orderBars.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('js/libs/flot/curvedLines.js')}}"></script>
    <script src="{{ asset('js/libs/jquery-knob/jquery.knob.min.js')}}"></script>
    <script src="{{ asset('js/libs/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{ asset('js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
    <script src="{{ asset('js/libs/d3/d3.min.js')}}"></script>
    <script src="{{ asset('js/libs/d3/d3.v3.js')}}"></script>
    <script src="{{ asset('js/libs/rickshaw/rickshaw.min.js')}}"></script>
    <script src="{{ asset('js/core/source/App.js')}}"></script>
    <script src="{{ asset('js/core/source/AppNavigation.js')}}"></script>
    <script src="{{ asset('js/core/source/AppOffcanvas.js')}}"></script>
    <script src="{{ asset('js/core/source/AppCard.js')}}"></script>
    <script src="{{ asset('js/core/source/AppForm.js')}}"></script>
    <script src="{{ asset('js/core/source/AppNavSearch.js')}}"></script>
    <script src="{{ asset('js/core/source/AppVendor.js')}}"></script>
    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
    $('#example').DataTable( {
      dom: 'Bfrtip',
      buttons: [
      'pdf', 'excel'
      ],
      "columnDefs": [{ targets: 'no-sort', orderable: false }],
      "language": {
        "search": "Buscar:",
        paginate: {
          next: '&#8594;', // or '→'
          previous: '&#8592;' // or '←'
        }
      },
      "order": [[ 0, "asc" ]]
    } );
  } );

        function borrar(e,form){
          e.preventDefault();
          swal({
            title: "Confirmar eliminación de registro?",
            icon: "warning",
            buttons: ["Confirmar", "Cancelar"],
            dangerMode: true,
          }).then((willDelete) => {
            if (!willDelete) {
              form.submit();
            } else {
              return false;
            }
          });
        }

        function act_inac(id,value){
          var title = 'Activar';
          var url   = 'activar';
          if (!value) {
            title = 'Desactivar';
            url   = 'desactivar';
          }
          swal({
            title: 'Está usted seguro de '+title+' la empresa "'+ $('#empresa-'+id).text() +'"?',
            icon: "warning",
            buttons: ["Confirmar", "Cancelar"],
            dangerMode: true,
          }).then((willDelete) => {
            if (!willDelete) {
              window.location.href = "/empresa/"+url+'/'+id;
            } else {
              return false;
            }
          });
        }
    </script>

</body>

</html>
