<!DOCTYPE html>
<html lang="en">

<head>
    <title>Commandline</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->
    @include('../Layout/links')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

</head>

<body class="menubar-hoverable header-fixed menubar-pin ">

    @include('../Layout/navbar');
    <!-- BEGIN BASE-->
    <div id="base">
        <div class="row">
            <section class="content" id="content">
                <div class="col-md-10 col-md-offset-1">
                    <div class="section-header">
                        <ol class="breadcrumb">
                            <li><a href="{{ route('empresa.index') }}">Lista de Empresas</a></li>
                            <li class="active">
                                Actualizar Empresa</li>
                        </ol>
                    </div>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Error!</strong> Revise los campos obligatorios.<br><br>

                    </div>
                    @endif
                    @if(Session::has('success'))
                    <div class="alert alert-info">
                        {{Session::get('success')}}
                    </div>
                    @endif
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="pull-left">
                                <h3>Lista de clientes</h3>
                            </div>

                            <div class="col-md-12">
                                <div class="table-container">
                                    <div class="table-responsive">
                                        <table id="example" class="display nowrap" style="width: 100%;">
                                            <thead>
                                                <th>Nombre</th>
                                                <th>Correo</th>
                                                <th></th>
                                            </thead>
                                            <tbody>
                                                @if($clientes->count())
                                                @foreach($clientes as $cliente)
                                                <tr>
                                                    <td>{{$cliente->nombres}} {{$cliente->apellidos}}</td>
                                                    <td>{{$cliente->email}}</td>
                                                    <td>
                                                        <input type="checkbox" <?php if ($cliente->estado == 1): ?>
                                                            checked <?php endif ?> name="cliente"
                                                            onclick="clienteSelect(<?=$cliente->idcliente?>, this,<?=$empresa->idempresa?>)">
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="8">No hay registro !!</td>
                                                </tr>
                                                @endif
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1">



                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Actualizar Empresa</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-container">
                                <form method="POST" action="{{ route('empresa.update',$empresa->idempresa) }}"
                                    role="form" id="accion_form">
                                    {{ csrf_field() }}
                                    <input name="_method" type="hidden" value="PATCH">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <div class="form-group">
                                                <label for="nombre">Nombre</label>
                                                <input type="text" name="nombre" id="nombre"
                                                    value="{{$empresa->nombre}}" class="form-control input-sm"
                                                    placeholder="Nombre">
                                                <p style="color: red;">{{ $errors->first('nombre') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <div class="form-group">
                                                <label for="nombre">Codigo</label>
                                                <input type="text" name="codigo" id="codigo"
                                                    value="{{$empresa->codigo}}" class="form-control input-sm"
                                                    placeholder="Nombre">
                                                {{-- <p style="color: red;">{{ $errors->first('nombre') }}</p> --}}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <div class="form-group">
                                                <label for="rut">Número de identificación</label>
                                                <input type="text" name="rut" id="rut" value="{{$empresa->rut}}"
                                                    class="form-control input-sm"
                                                    placeholder="Numero de identificación">
                                                <p style="color: red;">{{ $errors->first('rut') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label for="email">Correo Electrónico</label>
                                                <input type="email" name="email" id="email" value="{{$empresa->email}}"
                                                    class="form-control input-sm" placeholder="Correo">
                                                <p style="color: red;">{{ $errors->first('email') }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label for="telefono1">Telefono 1</label>
                                                <input type="text" name="telefono1" id="telefono1"
                                                    value="{{$empresa->telefono1}}" class="form-control input-sm"
                                                    placeholder="Telefono 1">
                                                <p style="color: red;">{{ $errors->first('telefono1') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label for="telefono2">Telefono 2</label>
                                                <input type="text" name="telefono2" id="telefono2"
                                                    value="{{$empresa->telefono2}}" class="form-control input-sm"
                                                    placeholder="Telefono 2">
                                                <p style="color: red;">{{ $errors->first('telefono2') }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <div class="form-group">
                                                <label for="pais">Pais</label>
                                                <input type="text" name="pais" id="pais" value="{{$empresa->pais}}"
                                                    class="form-control input-sm" placeholder="País">
                                                <p style="color: red;">{{ $errors->first('pais') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <div class="form-group">
                                                <label for="ciudad">Ciudad</label>
                                                <input type="text" name="ciudad" id="ciudad"
                                                    value="{{$empresa->ciudad}}" class="form-control input-sm"
                                                    placeholder="Ciudad">
                                                <p style="color: red;">{{ $errors->first('ciudad') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <div class="form-group">
                                                <label for="cp">Codigo Postal</label>
                                                <input type="text" name="cp" id="cp" value="{{$empresa->cp}}"
                                                    class="form-control input-sm" placeholder="Codigo postal">
                                                <p style="color: red;">{{ $errors->first('cp') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">

                                                <label for="idtipocontrato">Tipo de contrato</label>
                                                <select id="idtipocontrato" name="idtipocontrato" class="form-control">
                                                    @if($tipoContratos->count())
                                                    @foreach($tipoContratos as $list)
                                                    @if($empresa->idtipocontrato == $list->idtipoContrato)
                                                    <option value="{{ $list->idtipoContrato}}" selected>
                                                        {{ $list->nombre }}</option>
                                                    @else
                                                    <option value="{{ $list->idtipoContrato}}">{{ $list->nombre }}
                                                    </option>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </select>
                                                <p style="color: red;">{{ $errors->first('idtipocontrato') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label for="estado">Número de Tickets</label>
                                                <input type="text" name="ticket" id="ticket"
                                                    value="{{ $empresa->ticket }}" min="0" class="form-control">
                                                <p style="color: red;">{{ $errors->first('estado') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6" id="fechaInicio"
                                            style="display: {{ ($empresa->idtipocontrato == 1) ? 'none' : 'block' }}">
                                            <div class="form-group">
                                                <label>Fecha de Inicio</label>
                                                <input type="date" name="fechaInicio" value="{{$empresa->fechaInicio}}"
                                                    class="form-control input-sm">
                                                <p style="color: red;">{{ $errors->first('fechaInicio') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6" id="fechaFin"
                                            style="display: {{ ($empresa->idtipocontrato == 1) ? 'none' : 'block' }}">
                                            <div class="form-group">
                                                <label>Fecha de Finalización</label>
                                                <input type="date" name="fechaFin" value="{{$empresa->fechaFin}}"
                                                    class="form-control input-sm">
                                                <p style="color: red;">{{ $errors->first('fechaFin') }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="razon">Razon Social</label>
                                        <textarea name="razon" class="form-control input-sm"
                                            placeholder="Razón Social">{{$empresa->razon}}</textarea>
                                        <p style="color: red;">{{ $errors->first('razon') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="direccion1">Dirección 1</label>
                                        <textarea name="direccion1" class="form-control input-sm"
                                            placeholder="dirección 1">{{$empresa->direccion1}}</textarea>
                                        <p style="color: red;">{{ $errors->first('direccion1') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="direccion2">Dirección 2</label>
                                        <textarea name="direccion2" class="form-control input-sm"
                                            placeholder="Dirección 2">{{$empresa->direccion2}}</textarea>
                                        <p style="color: red;">{{ $errors->first('direccion2') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="notas">Notas u Observaciones</label>
                                        <textarea name="notas" class="form-control input-sm"
                                            placeholder="Notas u Observaciones">{{$empresa->notas}}</textarea>
                                        <p style="color: red;">{{ $errors->first('notas') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="comentario">Comentario Interno</label>
                                        <textarea name="comentario" class="form-control input-sm"
                                            placeholder="Comentarios"></textarea>
                                        <p style="color: red;">{{ $errors->first('comentario') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="estado">Estado</label>
                                        <select id="estado" name="estado" class="form-control">
                                            <option value="0">Seleccionar Estado</option>
                                            <option value="1" <?php if ($empresa->estado == 1): ?>selected
                                                <?php endif ?>>Activo</option>
                                            <option value="2" <?php if ($empresa->estado == 2): ?>selected
                                                <?php endif ?>>Inactivo</option>
                                        </select>
                                        <p style="color: red;">{{ $errors->first('estado') }}</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-3 col-md-offset-3">
                                            <input type="submit" value="Actualizar" class="btn btn-success btn-block">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
        @include('../Layout/menu')
    </div>


    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
    <script src="{{ asset('js/libs/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/libs/spin.js/spin.min.js')}}"></script>
    <script src="{{ asset('js/libs/autosize/jquery.autosize.min.js')}}"></script>
    <script src="{{ asset('js/libs/moment/moment.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.time.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.resize.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.orderBars.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('js/libs/flot/curvedLines.js')}}"></script>
    <script src="{{ asset('js/libs/jquery-knob/jquery.knob.min.js')}}"></script>
    <script src="{{ asset('js/libs/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{ asset('js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
    <script src="{{ asset('js/libs/d3/d3.min.js')}}"></script>
    <script src="{{ asset('js/libs/d3/d3.v3.js')}}"></script>
    <script src="{{ asset('js/libs/rickshaw/rickshaw.min.js')}}"></script>
    <script src="{{ asset('js/core/source/App.js')}}"></script>
    <script src="{{ asset('js/core/source/AppNavigation.js')}}"></script>
    <script src="{{ asset('js/core/source/AppOffcanvas.js')}}"></script>
    <script src="{{ asset('js/core/source/AppCard.js')}}"></script>
    <script src="{{ asset('js/core/source/AppForm.js')}}"></script>
    <script src="{{ asset('js/core/source/AppNavSearch.js')}}"></script>
    <script src="{{ asset('js/core/source/AppVendor.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
			$('#example').DataTable( {
				dom: 'Bfrtip',
				buttons: [
				'pdf', 'excel'
				],
				"language": {
					"search": "Buscar:",
					paginate: {
	          			next: '&#8594;', // or '→'
	          			previous: '&#8592;' // or '←'
	          		}
	          	},
	          	"order": [[ 0, "desc" ]]
	          });
	});

		function clienteSelect(id, input, empresa){
			//alert(id);

			//console.log(input.checked);

			//console.log(id + ' ' + empresa + ' ');

			if (!input.checked) {
				$.ajax({
		      		type: 'GET',
		      		url: '{{ url('/') }}/seleccionarCliente/'+id+'/'+empresa,
		      		data: $(this).serialize(),
		      		success: function(data) {
		      	    if (data != false){

		                }
	                }
	            });
			}else{
				$.ajax({
		      		type: 'GET',
		      		url: '{{ url('/') }}/unSeleccionarCliente/'+id+'/'+empresa,
		      		data: $(this).serialize(),
		      		success: function(data) {
		      			if (data != false){

		                }
	                }
	            });
			}



		}

	  $("#idtipocontrato").on("change", function(){
		console.log($(this).val());
		if ($(this).val() == 1) {
			$('#fechaInicio').css("display","none");
			$('#fechaFin').css("display","none");
		}else{
			$('#fechaInicio').css("display","block");
			$('#fechaFin').css("display","block");
		}
	});
    </script>

</body>

</html>
