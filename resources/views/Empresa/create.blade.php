@extends('Layout.layout')

@section('content')
<div id="base">
	<div class="row">
		<section class="content" id="content">
			<div class="col-md-8 col-md-offset-2">
			<div class="section-header">
					<ol class="breadcrumb">
					<li><a href="{{ route('empresa.index') }}">Lista de Empresas</a></li>
					<li class="active">Nueva Empresa</li>
					</ol>
				</div>
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Error!</strong> Revise los campos obligatorios.<br>
				</div>
				@endif
				@if(Session::has('success'))
				<div class="alert alert-info">
					{{Session::get('success')}}
				</div>
				@endif

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Nueva Empresa</h3>
					</div>
					<div class="panel-body">
						<div class="table-container">
							<form method="POST" action="{{ route('empresa.store') }}"  role="form" autocomplete="off" id="accion_form">
								{{ csrf_field() }}
								<div class="row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<div class="form-group">
											<label>Nombre</label>
											<input type="text" name="nombre" id="nombre" value="{{ old('nombre') }}" class="form-control input-sm">
											<p style="color: red;" id="text-nombre">
												@if($errors->first('nombre'))
													{{ $errors->first('nombre') }}
												@else
													Campo requerido
												@endif
											</p>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-4">
										<div class="form-group">
											<label>Codigo</label>
											<input placeholder="XX-123456" type="text" name="codigo" value="{{ old('codigo') }}" class="form-control input-sm">
											{{-- <p style="color: red;">
												@if($errors->first('rut'))
													{{ $errors->first('rut') }}
												@else
													Campo requerido
												@endif
											</p> --}}
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-4">
										<div class="form-group">
											<label>Número de Identificación</label>
											<input type="text" name="rut" value="{{ old('rut') }}" class="form-control input-sm">
											<p style="color: red;">
												@if($errors->first('rut'))
													{{ $errors->first('rut') }}
												@else
													Campo requerido
												@endif
											</p>
										</div>
									</div>

								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label>Correo Electrónico</label>
											<input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control input-sm">
											<p style="color: red;">
												@if($errors->first('email'))
													{{ $errors->first('email') }}
												@else
													Campo requerido	
												@endif
											</p>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label>Telefono 1</label>
											<input type="text" name="telefono1" value="{{ old('telefono1') }}" class="form-control input-sm">
											<p style="color: red;">
												@if($errors->first('telefono1'))
													{{ $errors->first('telefono1') }}
												@else
													Campo requerido
												@endif
											</p>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label>Telefono 2</label>
											<input type="text" name="telefono2" value="{{ old('telefono2') }}" class="form-control input-sm">
											<p style="color: red;">
												{{ $errors->first('telefono2') }}
											</p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label>Pais</label>
											<input type="text" name="pais" value="{{ old('pais') }}" class="form-control input-sm">
											<p style="color: red;">
												@if($errors->first('pais'))
													{{ $errors->first('pais') }}
												@else
													Campo requerido
												@endif
											</p>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label>Ciudad</label>
											<input type="text" name="ciudad" value="{{ old('ciudad') }}" class="form-control input-sm">
											<p style="color: red;">
												@if($errors->first('ciudad'))
													{{ $errors->first('ciudad') }}
												@else
													Campo requerido
												@endif
											</p>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label>Codigo Postal</label>
											<input type="text" name="cp" value="{{ old('cp') }}" class="form-control input-sm">
											<p style="color: red;">
												@if($errors->first('cp'))
													{{ $errors->first('cp') }}
												@else
													Campo requerido
												@endif
											</p>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label for="idtipocontrato">Tipo de contrato</label>
											<select id="idtipocontrato" name="idtipocontrato" class="form-control">
												<option value="0">Seleccionar Tipo de Contrato</option>
												@if($tipoContratos->count())
													@foreach($tipoContratos as $list)
													<option value="{{ $list->idtipoContrato}}">{{ $list->nombre }}</option>
													@endforeach
												@endif
											</select>
										</div>
										<p style="color: red;">
										@if($errors->first('idtipocontrato'))
											{{ $errors->first('idtipocontrato') }}
										@else
											Campo requerido
										@endif
									</p>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label for="estado">Numero de Tickets</label>
											<input type="text" name="ticket" id="ticket" value="{{ old('ticket') }}" min="0" required class="form-control">
											<p style="color: red;">{{ $errors->first('estado') }}</p>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6" id="fechaInicio" style="display: none">
										<div class="form-group">
											<label>Fecha de Inicio</label>
											<input type="date" name="fechaInicio" id="fechaInicio" value="{{ old('fechaInicio',date('Y-m-d')) }}" class="form-control input-sm">
											@if($errors->first('fechaInicio'))
												<p style="color: red;">{{ $errors->first('fechaInicio') }}</p>
											@else
												<p style="color: red;">Campo requerido</p>
											@endif
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6" id="fechaFin" style="display: none">
										<div class="form-group">
											<label>Fecha de Finalización</label>
											<input type="date" name="fechaFin" id="fechaFin" value="{{ old('fechaFin',date('Y-m-d')) }}" class="form-control input-sm">
											@if($errors->first('fechaFin'))
												<p style="color: red;">{{ $errors->first('fechaFin') }}</p>
											@else
												<p style="color: red;">Campo requerido</p>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Razon Social</label>
									<textarea name="razon" class="form-control input-sm">{{ old('razon') }}</textarea>
									<p style="color: red;">
										@if($errors->first('razon'))
											{{ $errors->first('razon') }}
										@else
											Campo requerido
										@endif
									</p>
								</div>
								<div class="form-group">
									<label>Dirección 1</label>
									<textarea name="direccion1" class="form-control input-sm">{{ old('direccion1') }}</textarea>
									<p style="color: red;">
										@if($errors->first('direccion1'))
											{{ $errors->first('direccion1') }}
										@else
											Campo requerido
										@endif
									</p>
								</div>
								<div class="form-group">
									<label>Dirección 2</label>
									<textarea name="direccion2" class="form-control input-sm">{{ old('direccion2') }}</textarea>
									<p style="color: red;">
										{{ $errors->first('direccion2') }}
									</p>
								</div>
								<div class="form-group">
									<label>Comentario</label>
									<textarea name="notas" class="form-control input-sm">{{ old('notas') }}</textarea>
									<p style="color: red;">
										{{ $errors->first('notas') }}
									</p>
								</div>
								<div class="form-group">
									<label for="estado">Estado</label>
									<select id="estado" name="estado" class="form-control">
										<option value="0">Seleccionar Estado</option>
										<option value="1">Activo</option>
										<option value="2">Inactivo</option>
									</select>
									<p style="color: red;">{{ $errors->first('estado') }}</p>
								</div>
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
										<input type="submit"  value="Guardar" class="btn btn-success btn-block">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@endsection

@section('script')

<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script>
	$("#nombre").on("input", function(){
		if ($(this).val() == "") {
			$('#text-nombre').text('Campo requerido')
		}else{
			$('#text-nombre').text('');
			$.ajax({
				type: 'GET',
				url: '/empresa/validateNombre/'+$('#nombre').val(),
				data: $(this).serialize(),
				success: function(data) {
					if (data == 1){
						$('#text-nombre').text('La empresa ya existe');
					}else{
					}
				}
			});
		}
	});

	$("#idtipocontrato").on("change", function(){
		console.log($(this).val());
		if ($(this).val() == 1) {
			$('#fechaInicio').css("display","none");
			$('#fechaFin').css("display","none");
		}else{
			$('#fechaInicio').css("display","block");
			$('#fechaFin').css("display","block");
		}
	});
</script>
@endsection
