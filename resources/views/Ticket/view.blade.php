<!DOCTYPE html>
<html lang="en">

<head>
    <title>Commandline</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <link rel="icon" type="image/png" href="/images/fav.png" />
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet'
        type='text/css' />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/bootstrap.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/materialadmin.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/font-awesome.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/material-design-iconic-font.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/libs/summernote/summernote.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/cronometro.css') }}" />
    <!-- END STYLESHEETS -->

    <style type="text/css">
        #cierre {
            display: none;
        }
    </style>

</head>

<body class=" header-fixed menubar-pin">


    @include('Layout/navbar');

    <!-- BEGIN BASE-->
    <div id="base">
        <!-- BEGIN CONTENT-->
        <div id="content">

            <section>

                <div class="section-header">
                    <ol class="breadcrumb">
                        <li class="active">Ticket</li>
                    </ol>
                </div>
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))

                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                            data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                    @endforeach
                </div>
                <div class="section-body">
                    <div class="card">

                        <!-- BEGIN CONTACT DETAILS HEADER -->
                        <div class="card-head style-primary">
                            <div class="tools pull-left" style="margin-left: 2%">
                                <span> Ticket: {{$tickets->numeroDeCaso}} -
                                    <button type="button" class="btn"
                                        style="background: {{$tickets->estado->color}}; color:#fff; font-weight: bold; ">{{$tickets->estado->nombre}}</button>
                                </span>
                            </div>
                            <div class="tools">
                                <a class="btn btn-flat hidden-xs" style="background: {{$tickets->ingreso->color}}"
                                    href="#">
                                    <span class="">Ingresado Por:</span> {{$tickets->ingreso->nombre}}
                                </a>
                            </div>
                            <!--end .tools -->
                        </div>
                        <!--end .card-head -->
                        <!-- END CONTACT DETAILS HEADER -->

                        <!-- BEGIN CONTACT DETAILS -->
                        <form action="/estadoTicket" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="card-tiles">
                                <div class="hbox-md col-md-12">
                                    <div class="hbox-column col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card card-outlined style-primary">
                                                    {{-- <div class="card-head">


                                                        @if ($tickets->conteoInicio == 0)
                                                        <header>
                                                            <button type="button"
                                                                class="btn ink-reaction btn-floating-action btn-primary disabled"><i
                                                                    class="md md-timer"></i></button>
                                                            <div class="reloj" id="Horas">00</div>
                                                            <div class="reloj" id="Minutos">:00</div>
                                                            <div class="reloj" id="Segundos">:00</div>
                                                            <div class="reloj" id="Centesimas">:00</div>
                                                            <button type="button" id="inicio"
                                                                class="btn ink-reaction btn-floating-action btn-success"
                                                                onclick="start({{$tickets->conteoInicio}});"><i
                                                                    class="md md-play-arrow"></i></button>
                                                            <button type="button" id="parar"
                                                                class="btn ink-reaction btn-floating-action btn-success"
                                                                onclick="stop();" disabled><i
                                                                    class="md md-stop"></i></button>
                                                            <input type="hidden" id="tiempo" name="tiempo" value="">
                                                            <input type="hidden" id="estadoTrabajo" name="estadoTrabajo"
                                                                value="{{$tickets->conteoInicio}}">
                                                        </header>
                                                        @else


                                                        <header>
                                                            <button type="button"
                                                                class="btn ink-reaction btn-floating-action btn-primary disabled"><i
                                                                    class="md md-timer"></i></button>
                                                            <div class="reloj" id="Horas">00</div>
                                                            <div class="reloj" id="Minutos">:00</div>
                                                            <div class="reloj" id="Segundos">:00</div>
                                                            <div class="reloj" id="Centesimas">:00</div>
                                                            <button type="button" id="inicio"
                                                                class="btn ink-reaction btn-floating-action btn-success"
                                                                onclick="start({{$tickets->conteoInicio}});"><i
                                                                    class="md md-play-arrow"></i></button>
                                                            <button type="button" id="parar"
                                                                class="btn ink-reaction btn-floating-action btn-success"
                                                                onclick="stop();" disabled><i
                                                                    class="md md-stop"></i></button>
                                                            <input type="hidden" id="tiempo" name="tiempo" value="">
                                                            <input type="hidden" id="estadoTrabajo" name="estadoTrabajo"
                                                                value="{{$tickets->conteoInicio}}">
                                                        </header>


                                                        @endif



                                                    </div> --}}
                                                    <!--end .card-head -->
                                                </div>
                                                <!--end .card -->
                                            </div>
                                            <!-- BEGIN CONTACTS MAIN CONTENT -->
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <ul class="nav nav-tabs" data-toggle="tabs">
                                                    <li class="active"><a href="#activity">Actividad</a></li>
                                                    <li><a href="#coment">Comentarios Internos</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <!-- BEGIN CONTACTS ACTIVITY -->
                                                    <div class="tab-pane active" id="activity">
                                                        <div
                                                            style="height: 600px; overflow-y: scroll; overflow-x: hidden; width: 100%">
                                                            <hr class="no-margin" />
                                                            <ul class="timeline collapse-md">
                                                                @if($historico->count())
                                                                @foreach($historico as $list)
                                                                @if($list->tipo == 8)
                                                                <li>
                                                                    <div class="timeline-circ circ-xl style-accent">
                                                                        <span class="md md-comment"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-info">
                                                                            <div class="card-body small-padding"
                                                                                style="height: 600px; overflow-y: scroll; overflow-x: hidden; width: 100%">
                                                                                <p>
                                                                                    <span
                                                                                        class="text-medium">{{$list->user->nombres}}
                                                                                        {{$list->user->apellidos}} El
                                                                                        cliente ingreso el ticket
                                                                                        <a class="text"
                                                                                            href="#">"{{$list->ticket->titulo}}"</a>
                                                                                    </span><br />
                                                                                    <span class="opacity-50">
                                                                                        <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                    </span>
                                                                                </p>
                                                                                <em>
                                                                                    <?php echo $list->descripcion ?>
                                                                                </em>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 9)
                                                                <li>
                                                                    <div class="timeline-circ circ-xl style-accent">
                                                                        <span class="md md-comment"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-info">
                                                                            <div class="card-body small-padding">
                                                                                <p>
                                                                                    <span class="text-medium">El cliente
                                                                                        Respondio el ticket
                                                                                        <a class="text"
                                                                                            href="#">"{{$list->ticket->titulo}}"</a>
                                                                                    </span><br />
                                                                                    <span class="opacity-50">
                                                                                        <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                    </span>
                                                                                </p>
                                                                                <em>
                                                                                    <?php echo $list->descripcion ?>
                                                                                </em>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 1)
                                                                <li class="timeline-inverted">
                                                                    <div class="timeline-circ circ-xl"><span
                                                                            class="md md-repeat"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-default-light">
                                                                            <div class="card-body small-padding">
                                                                                <img class="img-circle img-responsive pull-left width-1"
                                                                                    src="#" alt="" />
                                                                                <span
                                                                                    class="text-medium">{{$list->user->nombres}}
                                                                                    {{$list->user->apellidos}} Actualizo
                                                                                    el estado del ticket a <a
                                                                                        class="text-primary"
                                                                                        href="#">{{$list->estado->nombre}}</a>
                                                                                    <span
                                                                                        class="text-primary"></span></span><br />
                                                                                <span class="opacity-50">
                                                                                    <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 2)
                                                                @if($list->interno == 0 || $list->ticket->comercial == auth()->user()->id || auth()->user()->role == 1 || $list->ticket->tecnico == auth()->user()->id )
                                                                <li>
                                                                    <div class="timeline-circ circ-xl style-accent">
                                                                        <span class="md md-comment"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-@php if($list->interno != 0){echo'danger';}else{echo'info';} @endphp">
                                                                            <div class="card-body small-padding">
                                                                                <p>
                                                                                    <span 
                                                                                        class="text-medium">@php if($list->interno != 0){echo'(Mensaje Interno) ';}@endphp {{$list->user->nombres}}
                                                                                        {{$list->user->apellidos}} Dio
                                                                                        seguimiento al Ticket
                                                                                        <a class="text"
                                                                                            href="#">"{{$list->ticket->titulo}}"</a>
                                                                                            </span><br />
                                                                                    <span class="opacity-50">
                                                                                        <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                    </span>
                                                                                </p>
                                                                                <em>
                                                                                    <?php echo $list->descripcion ?>
                                                                                </em>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                @endif

                                                                @elseif($list->tipo == 3)
                                                                <li class="timeline-inverted">
                                                                    <div class="timeline-circ circ-xl"><span
                                                                            class="md md-trending-up"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-warning">
                                                                            <div class="card-body small-padding">
                                                                                <img class="img-circle img-responsive pull-left width-1"
                                                                                    src="#" alt="" />
                                                                                <span
                                                                                    class="text-medium">{{$list->user->nombres}}
                                                                                    {{$list->user->apellidos}} Actualizo
                                                                                    la Prioridad del ticket<a
                                                                                        class="text-primary"
                                                                                        href="#"></a> <span
                                                                                        class="text-primary"></span></span><br />
                                                                                <span class="opacity-50">
                                                                                    <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 4)
                                                                <li class="timeline-inverted">
                                                                    <div class="timeline-circ circ-xl"><span
                                                                            class="md md-person"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-success">
                                                                            <div class="card-body small-padding">
                                                                                <img class="img-circle img-responsive pull-left width-1"
                                                                                    src="#" alt="" />
                                                                                <span
                                                                                    class="text-medium">{{$list->creador->nombres}} {{$list->creador->apellidos}} Asigno el
                                                                                    ticket a <a class="text"
                                                                                        href="#">{{$tickets->tecnicos->nombres}}
                                                                                        {{$tickets->tecnicos->apellidos}}</a>
                                                                                    <span
                                                                                        class="text-primary"></span></span><br />
                                                                                <span class="opacity-50">
                                                                                    <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 20)
                                                                <li class="timeline-inverted">
                                                                    <div class="timeline-circ circ-xl"><span
                                                                            class="md md-person"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-default">
                                                                            <div class="card-body small-padding">
                                                                                <img class="img-circle img-responsive pull-left width-1"
                                                                                    src="#" alt="" />
                                                                                <span
                                                                                    class="text-medium">{{$list->creador->nombres}} {{$list->creador->apellidos}} Asigno el
                                                                                    ticket al Ejecutivo <a class="text"
                                                                                        href="#">{{$tickets->ejecutivo->nombres}}
                                                                                        {{$tickets->ejecutivo->apellidos}}</a>
                                                                                    <span
                                                                                        class="text-primary"></span></span><br />
                                                                                <span class="opacity-50">
                                                                                    <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 6)
                                                                <li class="timeline-inverted">
                                                                    <div class="timeline-circ circ-xl"><span
                                                                            class="md md-lock-outline"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-primary">
                                                                            <div class="card-body small-padding">
                                                                                <img class="img-circle img-responsive pull-left width-1"
                                                                                    src="#" alt="" />
                                                                                <span
                                                                                    class="text-medium">{{$list->user->nombres}}
                                                                                    {{$list->user->apellidos}} Cerro el
                                                                                    caso: <a class="text"
                                                                                        href="#">{{$list->estado->nombre}}</a>
                                                                                    <span
                                                                                        class="text-primary"></span></span><br />
                                                                                <span class="opacity-50">
                                                                                    <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                @elseif($list->tipo == 7)
                                                                <li class="timeline-inverted">
                                                                    <div class="timeline-circ circ-xl"><span
                                                                            class="md md-lock-open"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-success">
                                                                            <div class="card-body small-padding">
                                                                                <img class="img-circle img-responsive pull-left width-1"
                                                                                    src="#" alt="" />
                                                                                <span
                                                                                    class="text-medium">{{$list->user->nombres}}
                                                                                    {{$list->user->apellidos}} Re
                                                                                    Aperturo El caso <span
                                                                                        class="text-primary"></span></span><br />
                                                                                <span class="opacity-50">
                                                                                    <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 11)
                                                                <li>
                                                                    <div class="timeline-circ circ-xl style-accent">
                                                                        <span class="md md-comment"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-info">
                                                                            <div class="card-body small-padding"
                                                                                align="center">
                                                                                <p>
                                                                                    <span
                                                                                        class="text-medium">{{$list->user->nombres}}
                                                                                        {{$list->user->apellidos}}
                                                                                        Adjunto un archivo multimedia
                                                                                        <a class="text" href="#"></a>
                                                                                    </span><br />
                                                                                    <span class="opacity-50">
                                                                                        <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                    </span>
                                                                                </p>
                                                                                <em>
                                                                                    <img class="img-responsive width-5"
                                                                                        src="/files/{{$list->descripcion}}"
                                                                                        alt="" />

                                                                                        <a target="_blank" href="https://command.latmobile.com/files/{{$list->descripcion}}">Ver Archivo adjunto</a>
                                                                                </em>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 12)
                                                                <li>
                                                                    <div class="timeline-circ circ-xl style-accent">
                                                                        <span class="md md-comment"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-info">
                                                                            <div class="card-body small-padding"
                                                                                align="center">
                                                                                <p>
                                                                                    <span
                                                                                        class="text-medium">{{$list->user->nombres}}
                                                                                        {{$list->user->apellidos}}
                                                                                        Adjunto un archivo
                                                                                        <a class="text" href="#"></a>
                                                                                    </span><br />
                                                                                    <span class="opacity-50">
                                                                                        <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                    </span>
                                                                                </p>
                                                                                <em>
                                                                                    <a href="/files/{{$list->descripcion}}"
                                                                                        target="_blank">Ver Archivo</a>
                                                                                </em>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 13)
                                                                <li>
                                                                    <div class="timeline-circ circ-xl style-accent">
                                                                        <span class="md md-comment"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-warning">
                                                                            <div class="card-body small-padding"
                                                                                align="center">
                                                                                <p>
                                                                                    <span
                                                                                        class="text-medium">{{$tickets->cliente->nombres}}
                                                                                        {{$tickets->cliente->apellidos}}
                                                                                        El cliente Adjunto una imagen
                                                                                        <a class="text" href="#"></a>
                                                                                    </span><br />
                                                                                    <span class="opacity-50">
                                                                                        <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                    </span>
                                                                                </p>
                                                                                <em>
                                                                                    <img class="img-responsive width-5"
                                                                                        src="/attach/{{$list->descripcion}}"
                                                                                        alt="" />
                                                                                </em>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @elseif($list->tipo == 14)
                                                                <li>
                                                                    <div class="timeline-circ circ-xl style-accent">
                                                                        <span class="md md-comment"></span></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-warning">
                                                                            <div class="card-body small-padding"
                                                                                align="center">
                                                                                <p>
                                                                                    <span
                                                                                        class="text-medium">{{$tickets->cliente->nombres}}
                                                                                        {{$tickets->cliente->apellidos}}
                                                                                        El cliente Adjunto un archivo
                                                                                        <a class="text" href="#"></a>
                                                                                    </span><br />
                                                                                    <span class="opacity-50">
                                                                                        <?php echo date("F j, Y H:i:s", strtotime($list->created_at))?>
                                                                                    </span>
                                                                                </p>
                                                                                <em>
                                                                                    <a href="/attach/{{$list->descripcion}}"
                                                                                        target="_blank">Ver Archivo</a>
                                                                                </em>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                                                @endif

                                                                @endforeach
                                                                @endif
                                                            </ul>
                                                            <!--end .timeline -->
                                                        </div>
                                                        <br><br>
                                                        <div id="span" class="col-md-12">
                                                            <span style="color: red;"></span>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="Check1">
                                                                <label  class="custom-control-label" for="Check1">Agregar contactos</label>
                                                              </div>
                                                            <input class="name form-control hidden" name="correos" type="text" id="autoUpdate"  placeholder="Direcciones de correo"/></label>
                                                            <p ><span class="emsg hidden text-danger " style="
                                                                color: red;">Ingrese direcciones validas separadas por coma (,)</span></p>
                                                        </div>




                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <input type="hidden" name="idticket"
                                                                    value="{{$tickets->idticket}}">
                                                                <textarea name="respuesta"
                                                                    style="border: 1px solid #212121"
                                                                    class="form-control input-sm"
                                                                    placeholder="Respuesta"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="upload-btn-wrapper"
                                                                style="position: relative; overflow: hidden; display: inline-block;">
                                                                <button class="btn" id="lbl1"
                                                                    style="border: 2px solid gray; color: gray; background-color: white; padding: 6px 18px; border-radius: 8px; font-size: 10px; font-weight: bold;">Adjuntar
                                                                    Archivo</button>
                                                                <input type="file" id="file" onchange="cambio(1)"
                                                                    name="file"
                                                                    style="font-size: 100px; position: absolute; left: 0; top: 0; opacity: 0;" />

                                                            </div>
                                                            <span id="file_name"></span>
                                                            
                                                        </div>
                                                        <div class="col-md-3">

                                                            <button type="submit"
                                                                class="btn btn-block ink-reaction btn-info">Responder</button>
                                                                
                                                        </div>
                                                        <div class="row col-12">
                                                        <div class="col-md-5" >
                                                            <div class="form-check">
                                                                <input class="form-check-input" value="todos" type="radio" name="action" id="todos" checked>
                                                                <label class="form-check-label" for="todos">
                                                                  Respuesta a cliente
                                                                </label>
                                                              </div>

                                                              <div class="form-check">
                                                                <input class="form-check-input" value="interno" type="radio" name="action" id="interno" @php if($tickets->comercial == 0){echo'disabled';}@endphp >
                                                                <label class="form-check-label" for="interno"  >
                                                                  Respuesta interna @php if($tickets->comercial == 0){echo'(Para respuesta interna asigne un ejecutivo)';}@endphp
                                                                </label>
                                                              </div>
                                                        </div>

                                                    </div>



                                                        
                                                    </div>
                                                    <!--end #activity -->
                                                    <!-- END CONTACTS ACTIVITY -->
                                                    <!-- BEGIN CONTACTS ACTIVITY -->
                                                    <div class="tab-pane" id="coment">

                                                        <div
                                                            style="height: 600px; overflow-y: scroll; overflow-x: hidden; width: 100%">
                                                            <ul
                                                                class="timeline collapse-lg timeline-hairline no-shadow">
                                                                <li class="timeline-inverted">
                                                                    <div class="timeline-circ style-accent"></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-default-bright">
                                                                            <div class="card-body small-padding">
                                                                                <small
                                                                                    class="text-uppercase text-success pull-right">
                                                                                    <?php echo date("F j, Y H:i:s", strtotime($tickets->created_at))?>
                                                                                </small>
                                                                                <p>
                                                                                    <span
                                                                                        class="text-lg text-medium">{{$tickets->usuario->nombres}}
                                                                                        {{$tickets->usuario->apellidos}}</span><br>
                                                                                </p>
                                                                                <p>
                                                                                    {{$tickets->comentario}}
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @if($historico->count())
                                                                @foreach($historico as $list)
                                                                @if($list->tipo == 5)
                                                                <li>
                                                                    <div class="timeline-circ style-accent"></div>
                                                                    <div class="timeline-entry">
                                                                        <div class="card style-default-bright">
                                                                            <div class="card-body small-padding">
                                                                                <small
                                                                                    class="text-uppercase text-primary pull-right"><?php echo date("F j, Y H:i:s", strtotime($list->created_at))?></small>
                                                                                <p>
                                                                                    <span
                                                                                        class="text-lg text-medium">{{$list->user->nombres}}
                                                                                        {{$list->user->apellidos}}</span><br>
                                                                                </p>
                                                                                <p>
                                                                                    {{$list->descripcion}}
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @endif
                                                                @endforeach
                                                                @endif

                                                            </ul>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <input type="hidden" name="idticket" id="idticket"
                                                                    value="{{$tickets->idticket}}">
                                                                <textarea name="comentario"
                                                                    style="border: 1px solid #212121"
                                                                    class="form-control input-sm"
                                                                    placeholder="Comentario"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button type="submit"
                                                                class="btn btn-block ink-reaction btn-info">Comentar</button>
                                                        </div>
                                                    </div>
                                                    <!--end #activity -->
                                                    <!-- END CONTACTS ACTIVITY -->

                                                </div>
                                                <!--end .tab-content -->
                                            </div>
                                            <!--end .col -->
                                            <!-- END CONTACTS MAIN CONTENT -->

                                        </div>
                                        <!--end .row -->
                                    </div>
                                    <!--end .hbox-column -->

                                    <!-- BEGIN CONTACTS COMMON DETAILS -->
                                    <div class="hbox-column col-md-3 style-default-light">
                                        <div class="" align="center">
                                            <div class=" clearfix">
                                                <h3>Cliente</h3>
                                                <img class="img-circle size-2"
                                                    src="/images/{{ $tickets->cliente->avatar }}" width="30" alt="" />
                                            </div>
                                            <h1 class="text-light no-margin">{{ $tickets->cliente->nombres }}
                                                {{ $tickets->cliente->apellidos }}</h1>
                                        </div>
                                        <!--end .margin-bottom-xxl -->
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal dl-icon">
                                                    <dt><span class=" fa-lg opacity-50"></span></dt>
                                                    <dd style="margin-left: 0;">
                                                        <span class="opacity-50">
                                                            <h4><span>Tiempo Trabajado:</span> <span
                                                                    style="float: right;">{{$tiempo_t}}</span></h4>
                                                        </span>
                                                        <span class="opacity-50">
                                                            <h4><span>Empresa:</span> <span style="float: right;">
                                                                    {{$tickets->cliente->empresa->nombre}}</span> </h4>
                                                        </span>
                                                        <span class="opacity-50">
                                                            <h4><span>Contrato:</span> <span style="float: right;">
                                                                    {{$tickets->cliente->empresa->tipoContrato->nombre}}</span>
                                                            </h4>
                                                        </span>
                                                        <span class="opacity-50">
                                                            <h4>
                                                                <span>Prioridad:</span>
                                                                <span style="float: right;">
                                                                    <button type="button" class="btn btn-xs"
                                                                        style="background: {{$tickets->prioridad->color}}; color:#fff; font-weight: bold; ">{{$tickets->prioridad->nombre}}</button>

                                                                </span>
                                                            </h4>
                                                        </span>
                                                        <span class="opacity-50">
                                                            <h4><span>Fecha de inicio de contrato:</span>
                                                                @if ($tickets->cliente->empresa->nombre == "Sin empresa")
                                                                    <span style="float: right;">
                                                                        --------------
                                                                    </span>                                                                    
                                                                @else
                                                                    <span style="float: right;">
                                                                        <?php echo date("F j, Y", strtotime($tickets->cliente->empresa->fechaInicio))?>
                                                                    </span>                                                                    
                                                                @endif

                                                            </h4>
                                                        </span>
                                                        <span class="opacity-50">
                                                            <h4>
                                                                <span>Fecha de Vencimiento de contrato:</span> 
                                                                @if ($tickets->cliente->empresa->nombre == "Sin empresa")
                                                                    <span style="float: right;">
                                                                        --------------
                                                                    </span>                                                                    
                                                                @else
                                                                    <span style="float: right;">
                                                                        <?php echo date("F j, Y", strtotime($tickets->cliente->empresa->fechaFin))?>
                                                                    </span>                                                                    
                                                                @endif                                                                
                                                            </h4>
                                                        </span>
                                                        <span class="opacity-50">
                                                            <h4><span>Estado Contrato:</span>
                                                                <span style="float: right;">
                                                                    <?php
                                    if (date('Y-m-d') <= date("Y-m-d", strtotime($tickets->Cliente->empresa->fechaFin))) {
                                      ?>
                                                                    <button type="button" class="btn btn-xs btn-success"
                                                                        style="font-weight: bold;">Vigente</button>
                                                                    <?php
                                    }else if( $tickets->cliente->empresa->nombre == "Sin empresa"){
                                      ?>
                                                                    <button type="button" class="btn btn-xs btn-danger"
                                                                        style="font-weight: bold;">Sin contrato</button>
                                                                    <?php
                                    }
                                    else{
                                    ?>
                                                                <button type="button" class="btn btn-xs btn-danger"
                                                                    style="font-weight: bold;">Caducado</button>
                                                                <?php
                                }
                              ?>
                                                                </span>
                                                            </h4>
                                                        </span>
                                                    </dd>
                                                </dl>
                                                <!--end .dl-horizontal -->
                                                <dl class="dl-horizontal dl-icon">
                                                    <div class="card">
                                                        <div class="card-head card-head-xs style-primary">
                                                            <header>Configuración de Ticket</header>
                                                        </div>
                                                        <!--end .card-head -->
                                                        <div class="card-body">
                                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                                <div class="form-group">
                                                                    <label for="prioridad">Horas trabajadas</label>
                                                                    <input type="number" name="horas" id="horas" class="form-control" min="0" value="{{$tickets->horas}}" >
                                                                </div>

                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                                <div class="form-group">
                                                                    <label for="prioridad">Prioridades</label>
                                                                    <input type="hidden" name="prioridad_actual"
                                                                        value="{{$tickets->PrioridadTicket_idPrioridadTicket}}">
                                                                    <select id="prioridad" name="prioridad"
                                                                        class="form-control">
                                                                        @if($prioridades->count())
                                                                        @foreach($prioridades as $list)
                                                                        @if($list->idPrioridadTicket ==
                                                                        $tickets->PrioridadTicket_idPrioridadTicket)
                                                                        <option value="{{ $list->idPrioridadTicket}}"
                                                                            selected>{{ $list->nombre }}</option>
                                                                        @else
                                                                        <option value="{{ $list->idPrioridadTicket}}">
                                                                            {{ $list->nombre }}</option>
                                                                        @endif
                                                                        @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>

                                                            </div>
                                                            <!--end .card-body -->
                                                            @if(auth()->user()->role != 5)
                                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                                <div class="form-group">
                                                                    <label for="tecnico">Contacto</label>
                                                                    <input type="hidden" name="tecnico_actual"
                                                                        value="{{$tickets->tecnico}}">
                                                                    <select id="tecnico" name="tecnico"
                                                                        class="form-control">
                                                                        @if($tecnicos->count())
                                                                        @foreach($tecnicos as $list)
                                                                        @if($list->id == $tickets->tecnico)
                                                                        <option value="{{ $list->id}}" selected>
                                                                            {{ $list->nombres }} {{ $list->apellidos }}
                                                                        </option>
                                                                        @else
                                                                        <option value="{{ $list->id}}">
                                                                            {{ $list->nombres }} {{ $list->apellidos }}
                                                                        </option>
                                                                        @endif
                                                                        @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                                <div class="form-group">
                                                                    <label for="tecnico">Ejecutivo Comercial</label>
                                                                    <input type="hidden" name="ejecutivo_actual"
                                                                        value="{{$tickets->comercial}}">
                                                                    <select id="tecnico" name="ejecutivo"
                                                                        class="form-control">
                                                                        @if($ejecutivos->count())
                                                                            @if($tickets->comercial ==0)
                                                                            <option selected value="0">
                                                                                Sin Ejecutivo asignado
                                                                            </option>
                                                                            @endif
                                                                        @foreach($ejecutivos as $list)
                                                                        @if($list->id == $tickets->comercial)
                                                                        <option value="{{ $list->id}}" selected>
                                                                            {{ $list->nombres }} {{ $list->apellidos }}
                                                                        </option>
                                                                        @else
                                                                        <option value="{{ $list->id}}">
                                                                            {{ $list->nombres }} {{ $list->apellidos }}
                                                                        </option>
                                                                        @endif
                                                                        @endforeach
                                                                        @endif
                                                                        
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            @endif
                                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                                <div class="form-group">
                                                                    <label for="idestado">Estados</label>
                                                                    <input type="hidden" name="estado_actual"
                                                                        value="{{$tickets->EstadoTicket_idEstadoTicket}}">
                                                                    <select id="idestado" onclick="cierre(this.value)"
                                                                        name="idestado" class="form-control">
                                                                        @if($estados->count())
                                                                        @foreach($estados as $list)
                                                                        @if($list->idEstadoTicket ==
                                                                        $tickets->EstadoTicket_idEstadoTicket)
                                                                        <option value="{{ $list->idEstadoTicket}}"
                                                                            selected>{{ $list->nombre }}</option>
                                                                        @else
                                                                        <option value="{{ $list->idEstadoTicket}}">
                                                                            {{ $list->nombre }}</option>
                                                                        @endif
                                                                        @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12" id="cierre">
                                                                <div class="form-group">
                                                                    <label for="idestado">Tipos de cierre</label>
                                                                    <select id="idcierre" name="idcierre"
                                                                        class="form-control">
                                                                        @if($cierres->count())
                                                                        @foreach($cierres as $list)
                                                                        @if($list->idEstadoTicket ==
                                                                        $tickets->EstadoTicket_idEstadoTicket)
                                                                        <option value="{{ $list->idEstadoTicket}}"
                                                                            selected>{{ $list->nombre }}</option>
                                                                        @else
                                                                        <option value="{{ $list->idEstadoTicket}}">
                                                                            {{ $list->nombre }}</option>
                                                                        @endif
                                                                        @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <button type="submit"
                                                                    class="btn btn-block ink-reaction btn-info">Actualizar</button>
                                                            </div>
                                                        </div>
                                                        <!--end .card -->
                                                </dl>
                                                <!--end .dl-horizontal -->
                                            </div>
                                            <!--end .col -->
                                        </div>
                                        <!--end .row -->
                                    </div>
                                    <!--end .hbox-column -->
                                    <!-- END CONTACTS COMMON DETAILS -->

                                </div>
                                <!--end .hbox-md -->
                        </form>
                    </div>
                    <!--end .card-tiles -->
                    <!-- END CONTACT DETAILS -->

                </div>
                <!--end .card -->
        </div>
        <!--end .section-body -->
        </section>
    </div>
    <!--end #content-->
    <!-- END CONTENT -->

    @include('Layout/menu')
    <!-- BEGIN OFFCANVAS RIGHT -->
    <div class="offcanvas">
    </div>
    <!--end .offcanvas-->
    <!-- END OFFCANVAS RIGHT -->

    </div>
    <!--end #base-->
    <!-- END BASE -->

    <!-- BEGIN JAVASCRIPT -->
    <script src="{{ asset('js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ asset('js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ asset('js/libs/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/libs/spin.js/spin.min.js') }}"></script>
    <script src="{{ asset('js/libs/autosize/jquery.autosize.min.js') }}"></script>
    <script src="{{ asset('js/libs/gmaps/gmaps.js') }}"></script>
    <script src="{{ asset('js/libs/inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('js/libs/moment/moment.min.js') }}"></script>
    <script src="{{ asset('js/libs/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/libs/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('js/libs/bootstrap-rating/bootstrap-rating-input.min.js') }}"></script>
    <script src="{{ asset('js/libs/nanoscroller/jquery.nanoscroller.min.js') }}"></script>
    <script src="{{ asset('js/libs/microtemplating/microtemplating.min.js') }}"></script>
    <script src="{{ asset('js/libs/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('js/core/source/App.js') }}"></script>
    <script src="{{ asset('js/core/source/AppNavigation.js') }}"></script>
    <script src="{{ asset('js/core/source/AppOffcanvas.js') }}"></script>
    <script src="{{ asset('js/core/source/AppCard.js') }}"></script>
    <script src="{{ asset('js/core/source/AppForm.js') }}"></script>
    <script src="{{ asset('js/core/source/AppNavSearch.js') }}"></script>
    <script src="{{ asset('js/core/source/AppVendor.js') }}"></script>
    <script src="{{ asset('js/core/demo/Demo.js') }}"></script>
    <script src="{{ asset('js/core/demo/DemoPageContacts.js') }}"></script>

    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
    <!-- END JAVASCRIPT -->

    <script type="text/javascript">
            var $regexname='(([a-zA-Z0-9\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*,\s*|\s*$))';
            $("form").submit(function(e){

            if( $('#Check1').prop('checked') ){

                if($('.name').val() == ""){
                e.preventDefault(e);
                swal({
                title: "Agrege correos en el campo",
                icon: "warning",
                buttons: ["Cerrar"],
                dangerMode: true,
            })

                $('.name').focus();

                }

            if(!$('.name').val().match($regexname) && $('.name').val() != ""){
                e.preventDefault(e);
                swal({
                title: "Los correos agregados tienen un formato invalido, por favor ingreselos correctamente",
                icon: "warning",
                buttons: ["Cerrar"],
                dangerMode: true,
            })

            }


            }


            });

        $(document).ready(function(){

            $('.name').on('keypress keydown keyup',function(){
                     if (!$(this).val().match($regexname) && $(this).val() != "") {
                      // there is a mismatch, hence show the error message
                         $('.emsg').removeClass('hidden');
                         $('.emsg').show();
                         var flag = true;
                     }
                   else{
                        // else, do not display message
                        $('.emsg').addClass('hidden');
                        flag = false;
                       }

                       if($('.name').val() == ""){
                    flag = false;
                 }
                 });
                 if($('.name').val() == ""){
                    flag = false;

                 }


            $('#Check1').change(function(){
                console.log($('#Check1').prop('checked'));

        if(this.checked){
            $('#autoUpdate').fadeIn('fast');
            $('#autoUpdate').removeClass("hidden");
        }
        else{

            $('#autoUpdate').fadeOut('fast');
            $('#autoUpdate').addClass("hidden");

        }
            
        });

                 



        });

    </script>


    <script type="text/javascript">
        var statusP = $("#estadoTrabajo").val();
    var id = $('#idticket').val();
    var centesimas = 0;
    var segundos = 0;
    var minutos = 0;
    var horas = 0;



    sessionStorage.setItem("idticket"+id,id);




    if(sessionStorage.getItem("centesimas"+id) && sessionStorage.getItem("idticket"+id) == id){

      var centesimas = sessionStorage.getItem("centesimas"+id);


    }

    if(sessionStorage.getItem("segundos"+id) && sessionStorage.getItem("idticket"+id) == id){

      var segundos = sessionStorage.getItem("segundos"+id);


    }
    if(sessionStorage.getItem("minutos"+id) && sessionStorage.getItem("idticket"+id) == id){

      var minutos = sessionStorage.getItem("minutos"+id);


    }
    if(sessionStorage.getItem("horas"+id) && sessionStorage.getItem("idticket"+id) == id){

      var horas = sessionStorage.getItem("horas"+id);


    }

    function cambio(tipo){
              if (tipo == 1) {
                  $('.lb1').css({"background-color":"black"});
                  var file = $('#file')[0].files[0];
                  $('#file_name').text(file.name);
              }else{
                $('.lb2').css({"background-color":"black"});
                  var file = $('#pdf')[0].files[0];
                  $('#pdf_name').text(file.name);
              }
          }

    <?php
                if ( !empty($chart_area) ) {
                    ?>
                        Morris.Line({
                            element: 'chart_area',
                            data: [<?php echo $chart_area;?>],
                            xkey: 'titulo',
                            ykeys: ['abiertos', 'cerrados'],
                            labels: ['Abiertos', 'Cerrados'],
                            pointSize: 3,
                            hideHover: 'auto',
                            pointFillColors: ['#4DB6AC','#FFC107','#c62828','#212121'],
                            pointStrokeColors: ['#4DB6AC','#FFC107','#c62828','#212121'],
                            lineColors: ['#4DB6AC','#FFC107','#c62828','#212121'],
                            fillOpacity: 0.8,
                            parseTime: false,
                            resize: true,
                            redraw: true
                        });
                    <?php
                }
            ?>

    function cierre(id){
      if (id == 5) {
        $('#cierre').css({"display":"inline-block"});
        $("#span span").text("*Ingresar el mensaje de resolución");
      }else{
        $('#cierre').css({"display":"none"});
        $("#span span").text("");
      }
    }





    function start(statusW) {




      control = setInterval(cronometro,10);
      document.getElementById("inicio").disabled = true;
      document.getElementById("parar").disabled = false;
      //$("#tiempo").val(horas +':'+minutos+':'+segundos);
      //var id = $('#idticket').val();

     if(statusW==0){
      $.ajax({
                type: 'GET',
                url: '{{ url('/') }}/trabajoTicket/'+id+'/',
                    data: $(this).serialize(),
                    success: function(data) {
                        //control del response
                        console.log(data);
                    }
                });

      }



    }


    function stop() {
      clearInterval(control);
      document.getElementById("inicio").disabled = false;
      document.getElementById("parar").disabled = true;

      $("#tiempo").val(horas +':'+minutos+':'+segundos);

      var id = $('#idticket').val();
      var tiempo = $('#tiempo').val();

      sessionStorage.setItem("centesimas"+id, 0);
      sessionStorage.setItem("minutos"+id, 0);
      sessionStorage.setItem("segundos"+id, 0);
      sessionStorage.setItem("horas"+id, 0);











       $.ajax({
                type: 'GET',
                url: '{{ url('/') }}/updateTiempo/'+id+'/'+tiempo,
                    data: $(this).serialize(),
                    success: function(data) {
                        //control del response
                        console.log(data);
                        $('#tiempo').val(0);
                    }
                });

    }



    function reinicio() {
      clearInterval(control);
      centesimas = 0;
      segundos = 0;
      minutos = 0;
      horas = 0;
      Centesimas.innerHTML = ":00";
      Segundos.innerHTML = ":00";
      Minutos.innerHTML = ":00";
      Horas.innerHTML = "00";
      document.getElementById("inicio").disabled = false;
      document.getElementById("parar").disabled = true;
    }




    function cronometro () {
      if (centesimas < 99) {
        centesimas++;
        if (centesimas < 10) { centesimas = "0"+centesimas }
        Centesimas.innerHTML = ":"+centesimas;
        sessionStorage.setItem("centesimas"+id, centesimas);
        if(minutos==0){
          sessionStorage.setItem("minutos"+id, 0);
        }
        if(horas==0){
          sessionStorage.setItem("horas"+id, 0);
        }
      }
      if (centesimas == 99) {
        centesimas = -1;
      }
      if (centesimas == 0) {
        segundos ++;
        if (segundos < 10) { segundos = "0"+segundos }
          Segundos.innerHTML = ":"+segundos;
          sessionStorage.setItem("segundos"+id, segundos);
      }
      if (segundos == 59) {
        segundos = -1;
      }
      if ( (centesimas == 0)&&(segundos == 0) ) {
        minutos++;
        if (minutos < 10) { minutos = "0"+minutos }
          Minutos.innerHTML = ":"+minutos;

          sessionStorage.setItem("minutos"+id, minutos);
      }
      if (minutos == 59) {
        minutos = -1;
      }
      if ( (centesimas == 0)&&(segundos == 0)&&(minutos == 0) ) {
        horas ++;
        if (horas < 10) { horas = "0"+horas }
          Horas.innerHTML = horas;
          sessionStorage.setItem("horas"+id, horas);
      }
    }


    window.onload = function() {

      if(statusP == 1 && sessionStorage.getItem("idticket"+id) == id){


        Centesimas.innerHTML= ":" + sessionStorage.getItem("centesimas"+id);
        Segundos.innerHTML =":" +sessionStorage.getItem("segundos"+id);
        Minutos.innerHTML =":" +sessionStorage.getItem("minutos"+id);
        Horas.innerHTML =sessionStorage.getItem("horas"+id);

      }

    };


    </script>

</body>

</html>
