<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tickets</title>

  <!-- BEGIN META -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" content="your,keywords">
  <meta name="description" content="Short explanation about this website">
  <!-- END META -->
  @include('../Layout/links')

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

  <style type="text/css">
  #c_cliente, #info{
    display: none;
  }
</style>

</head>
<body class="menubar-hoverable header-fixed menubar-pin ">

  @include('../Layout/navbar');

  <!-- start informe -->

  <div id="base">

      <!-- BEGIN CONTENT-->
      <div id="content">
        <section>
          <div class="section-header">
            <ol class="breadcrumb">
             <li><a href="{{ route('ticket.index') }}">Lista de Tickets</a></li>
                <li class="active">Nuevo Ticket</li>
            </ol>
          </div>
          @if (count($errors) > 0)
                        <div class="alert alert-danger">
                         <strong>Error!</strong> Revise los campos obligatorios.<br>
                       </div>
                       @endif
                       @if(Session::has('success'))
                       <div class="alert alert-info">
                         {{Session::get('success')}}
                       </div>
                       @endif
          <div class="section-body">
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-tiles style-default-light">

                  <!-- BEGIN BLOG POST HEADER -->
                  <div class="row style-primary">
                    <div class="col-sm-9">
                      <div class="card-body style-default-dark">
                        <h2>Ticket creado por: {{auth()->user()->nombres}} {{auth()->user()->apellidos}}</h2>
                      </div>
                    </div><!--end .col -->
                    <div class="col-sm-3">
                      <div class="card-body">
                        <div class="hidden-xs">
                          <h3 class="text-light"><?php echo date("F j")?></h3>
                        </div>
                      </div>
                    </div><!--end .col -->
                  </div><!--end .row -->
                  <!-- END BLOG POST HEADER -->

                  <div class="row">
                    <form method="POST" action="{{ route('ticket.store') }}"  role="form"  enctype="multipart/form-data" id="accion_form">
                      {{ csrf_field() }}
                      <!-- BEGIN BLOG POST TEXT -->
                      <div class="col-md-9">
                        <article class="style-default-bright">
                          <div class="card-body">

                            <div>
                              <div class="panel panel-default">
                                 <div class="panel-body">
                                  <div class="table-container">

                                  <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                      <div class="form-group">
                                       <input type="text" id="numeroDeCaso" class="form-control input-sm" value="{{$numero}}" disabled>
                                       <input type="hidden" name="numeroDeCaso" id="numeroDeCaso" class="form-control input-sm" value="{{$numero}}" >
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                      <div class="form-group">
                                        <input type="text" name="titulo" id="titulo" class="form-control input-sm" value="{{ old('titulo') }}" placeholder="Problema">
                                        <p style="color: red;">{{ $errors->first('titulo') }}</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                        <label for="idempresa">Empresas</label>
                                          <select id="idempresa" onchange="select()" name="idempresa" class="form-control">
                                            @if($empresas->count())
                                            @foreach($empresas as $list)
                                              @if($list->idempresa == old('idempresa'))
                                                <option value="{{ $list->idempresa}}" selected>{{ $list->nombre }}</option>
                                              @else
                                                <option value="{{ $list->idempresa}}">{{ $list->nombre }}</option>
                                              @endif
                                            @endforeach
                                            @endif
                                          </select>
                                      </div>
                                      <p style="color: red;">{{ $errors->first('idempresa') }}</p>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                        <label for="clientes">Clientes</label>
                                          <select id="clientes" onchange="info_cli(this.value)" name="Cliente_idcliente" class="form-control">
                                            <option value="">&nbsp;</option>
                                          </select>
                                          <p style="color: red;">{{ $errors->first('Cliente_idcliente') }}</p>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                      <div class="form-group">
                                        <label for="idingreso">Tipo de Ingreso</label>
                                        <select id="idingreso" name="idingreso" class="form-control">
                                          @if($ingresos->count())
                                          @foreach($ingresos as $list)
                                            @if($list->idingreso == old('idingreso'))
                                              <option value="{{ $list->idingreso}}" selected>{{ $list->nombre }}</option>
                                            @else
                                              <option value="{{ $list->idingreso}}">{{ $list->nombre }}</option>
                                            @endif
                                          @endforeach
                                          @endif
                                        </select>
                                      </div>
                                      <p style="color: red;">{{ $errors->first('idingreso') }}</p>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <textarea name="descripcion" class="form-control input-sm" placeholder="Descripcion">{{old('descripcion')}}</textarea>
                                    <p style="color: red;">{{ $errors->first('descripcion') }}</p>
                                  </div>
                                  <div class="form-group">
                                    <textarea name="comentario" class="form-control input-sm" placeholder="Comentario Interno">{{old('comentario')}}</textarea>
                                    <p style="color: red;">{{ $errors->first('comentario') }}</p>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12">
                                      <div class="upload-btn-wrapper" style="position: relative; overflow: hidden; display: inline-block;">
                                        <button class="btn" id="lbl1" style="border: 2px solid gray; color: gray; background-color: white; padding: 6px 18px; border-radius: 8px; font-size: 10px; font-weight: bold;">Adjuntar archivo</button>
                                        <input type="file" id="file" onchange="cambioFile(1)" name="file" style="font-size: 100px; position: absolute; left: 0; top: 0; opacity: 0;" />
                                        <p style="color: red;">{{ $errors->first('file') }}</p>
                                      </div>
                                      <span id="pdf_name"></span>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                            </div>
                          </div><!--end .card-body -->
                        </article>
                      </div><!--end .col -->
                      <!-- END BLOG POST TEXT -->

                      <!-- BEGIN BLOG POST MENUBAR -->
                      <div class="col-md-3">
                        <br>
                        <div class="well col-md-12">
                          <input type="radio" name="programar" value="1"> Programar Ticket <br>
                          <label>Fecha Programación</label><br>
                          <input type="date" name="fechai" value="<?=date('Y-m-d')?>" min="<?=date('Y-m-d')?>">
                          <p style="color: red;">{{ $errors->first('fechai') }}</p>
                        </div>
                        <div id="" class="well col-md-12">
                          <input type="radio" name="programar" value="2"> Programar frecuencia del ticket <br>

                          <label>Desde</label>
                          <input type="date" onchange="handler(event)" value="<?=date('Y-m-d')?>" min="<?=date('Y-m-d')?>" class="form-control" name="FecInicio" id="FecInicio">
                          <br><label>Hasta</label>
                          <input type="date" onchange="agregarOpciones(event)" value="<?=date('Y-m-d',strtotime("+1 day"))?>" min="<?=date('Y-m-d')?>" class="form-control " disabled name="FecFin" id="FecFin">
                          <br><label>Frecuencia</label><br>
                          <div id="opciones">
                            <select name="tiempo" id="myselect" class="form-control">
                              <option value="1"> Dia </option>
                            </select>
                            <p style="color: red;">{{ $errors->first('fechai') }}</p>
                          </div>
                        </div>
                        <div class="well col-xs-12 col-sm-12 col-md-12" id="info">

                          <div id="informacion" class="col-xs-12 col-sm-12 col-md-12">
                          </div>
                          <hr>
                          <div class="col-xs-12 col-sm-12 col-md-12">
                            <h3 class="text-light">Cambiar Prioridad <input type="checkbox" name="cambio" value="1"></h3>
                            <ul class="nav nav-pills nav-stacked nav-transparent">
                              <li>
                                <div class="form-group">
                                  <select id="prioridad" name="prioridad" class="form-control">
                                    @if($prioridades->count())
                                    @foreach($prioridades as $list)
                                    <option value="{{ $list->idPrioridadTicket}}">{{ $list->nombre }}</option>
                                    @endforeach
                                    @endif
                                  </select>
                                </div>
                              </li>
                            </ul>
                          </div>

                          <!-- <h3 class="text-light">Categories</h3>
                          <ul class="nav nav-pills nav-stacked nav-transparent">
                            <li><a href="#"><span class="badge pull-right">42</span>Design</a></li>
                            <li><a href="#"><span class="badge pull-right">32</span>Technology</a></li>
                            <li><a href="#"><span class="badge pull-right">12</span>Video</a></li>
                            <li><a href="#"><span class="badge pull-right">5</span>Music</a></li>
                            <li><a href="#"><span class="badge pull-right">28</span>Uncategorized</a></li>
                          </ul>
                          <h3 class="text-light">Tags</h3>
                          <div class="list-tags">
                            <a class="btn btn-xs btn-primary">Wordpress</a>
                            <a class="btn btn-xs btn-primary">Technology</a>
                            <a class="btn btn-xs btn-primary">HTML5</a>
                            <a class="btn btn-xs btn-primary">Illustrator</a>
                            <a class="btn btn-xs btn-primary">Music</a>
                            <a class="btn btn-xs btn-primary">CSS3</a>
                            <a class="btn btn-xs btn-primary">Video</a>
                            <a class="btn btn-xs btn-primary">Photoshop</a>
                            <a class="btn btn-xs btn-primary">jQuery</a>
                          </div> -->
                        </div><!--end .card-body -->
                        <div class="col-sm-12">
                          <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                        </div>
                      </div><!--end .col -->
                      <!-- END BLOG POST MENUBAR -->
                    </form>
                  </div><!--end .row -->
                </div><!--end .card -->
              </div><!--end .col -->
            </div><!--end .row -->
          </div><!--end .section-body -->
        </section>
      </div><!--end #content-->
      <!-- END CONTENT -->

  </div>

@include('../Layout/menu')


<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="{{ asset('js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{ asset('js/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/libs/spin.js/spin.min.js')}}"></script>
<script src="{{ asset('js/libs/autosize/jquery.autosize.min.js')}}"></script>
<script src="{{ asset('js/libs/moment/moment.min.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.min.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.time.min.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.orderBars.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.pie.js')}}"></script>
<script src="{{ asset('js/libs/flot/curvedLines.js')}}"></script>
<script src="{{ asset('js/libs/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{ asset('js/libs/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{ asset('js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
<script src="{{ asset('js/libs/d3/d3.min.js')}}"></script>
<script src="{{ asset('js/libs/d3/d3.v3.js')}}"></script>
<script src="{{ asset('js/libs/rickshaw/rickshaw.min.js')}}"></script>
<script src="{{ asset('js/core/source/App.js')}}"></script>
<script src="{{ asset('js/core/source/AppNavigation.js')}}"></script>
<script src="{{ asset('js/core/source/AppOffcanvas.js')}}"></script>
<script src="{{ asset('js/core/source/AppCard.js')}}"></script>
<script src="{{ asset('js/core/source/AppForm.js')}}"></script>
<script src="{{ asset('js/core/source/AppNavSearch.js')}}"></script>
<script src="{{ asset('js/core/source/AppVendor.js')}}"></script>
<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>


<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable( {
			dom: 'Bfrtip',
			buttons: [
			'pdf', 'print'
			],
			"language": {
				"search": "Buscar:",
				paginate: {
          next: '&#8594;', // or '→'
          previous: '&#8592;' // or '←'
        }
      }
    } );
	} );




  function handler(e){

    var dateControl2 = document.getElementById("FecFin");
    var fecha = new Date($('#FecInicio').val());
    var nd = fecha.setDate(fecha.getDate() + 2);
    resta = fecha.getMilliseconds();
    var resultado = nd - resta;
    nd = new Date(nd);
    var Year = nd.getFullYear();
    var Month = nd.getMonth()+1;
    var Day = nd.getDate();
    if(Month>=10){
      mes = "-"
    }else{
      mes = "-0"
    }

    if(Day>=10){
      dia = "-"
    }else{
      dia = "-0"
    }
    inicio = Date.parse();
    fin = Date.parse(Year.toString()+mes+Month.toString()+dia+Day.toString());
    dateControl2.value = Year.toString()+mes+Month.toString()+dia+ Day.toString();
    dateControl2.disabled = false;
    dateControl2.min = Year.toString()+mes+Month.toString()+dia+ Day.toString();

    var d_nested = document.getElementById("myselect");
    var throwawayNode = d.removeChild(d_nested);

    var node = document.createElement("select");

    node.setAttribute("id","myselect");
    node.setAttribute("name","tiempo");
    node.setAttribute("class","form-control");

    
    $('#myselect').append($('<option>', {
        value: 1,
        text: 'Dia'
    }));

  }

  function agregarOpciones(){
    var dateControl1 = document.getElementById("FecInicio");
    var dateControl2 = document.getElementById("FecFin");

    date1 = new Date(dateControl1.value);
    date2 = new Date(dateControl2.value);
    resultado = date2 - date1;
    console.log(resultado);

    var d = document.getElementById("opciones");
    var d_nested = document.getElementById("myselect");
    var throwawayNode = d.removeChild(d_nested);

    var node = document.createElement("select");

    node.setAttribute("id","myselect");
    node.setAttribute("name","tiempo");
    node.setAttribute("class","form-control");


    document.getElementById("opciones").appendChild(node);

    $('#myselect').append($('<option>', {
        value: 1,
        text: 'Dia'
    }));

    if(resultado>=604800000){
      $('#myselect').append($('<option>', {
          value: 2,
          text: 'Semana'
      }));
    }

    if(resultado>=1296000000){

      $('#myselect').append($('<option>', {
          value: 3,
          text: 'Quincena'
      }));
    }

    if(resultado>=2678400000){

      $('#myselect').append($('<option>', {
        value: 4,
        text: 'Mensualmente'
      }));
    }

    if(resultado>=5184000000){

      $('#myselect').append($('<option>', {
        value: 5,
        text: 'Semestre'
      }));
    }
    if(resultado>=31536000000){
      $('#myselect').append($('<option>', {
        value: 6,
        text: 'Anualmente'
      }));
    }



  }

  function cambioFile(tipo){
              if (tipo == 1) {
                  $('.lb1').css({"background-color":"black"});
                  var file = $('#file')[0].files[0];
                  $('#file_name').text(file.name);
              }else{
                $('.lb2').css({"background-color":"black"});
                  var file = $('#pdf')[0].files[0];
                  $('#pdf_name').text(file.name);
              }
          }


        select();
        function select(){
      	//console.log("notificacion");
      	var id = document.getElementById("idempresa");
        $.ajax({
          type: 'GET',
          url: '{{ url('/') }}/select_clientes/'+ id.value,
          data: $(this).serialize(),
          success: function(data) {
           $('#clientes').empty();
           if (data != ""){
            $('#c_cliente').css({"display":"none"});
            $('#info').css({"display":"none"});
                        //console.log(data);
                        $('#clientes').append('<option value="0"></option>');
                        data.forEach(function(element){
                          $('#clientes').append('<option value="'+ element['idcliente'] +'">'+ element['nombres'] +' '+element['apellidos'] +'</option>');
                        });
                      }else{
                       $('#clientes').append('<option value="0">Sin Resultados</option>');
                       $('#c_cliente').css({"display":"inline-block"});
                       $('#info').css({"display":"none"});
                     }
                   }
                 });
      }

      function info_cli(id){
       $.ajax({
        type: 'GET',
        url: '{{ url('/') }}/select_info/'+ id,
        data: $(this).serialize(),
        success: function(data) {
         if (data != ""){
          $('#info').css({"display":"inline-block"});
                        //console.log(data);
                        $('#informacion').empty();
                        data.forEach(function(element){
                            //console.log(element);
                            $("#informacion").append('<h3 class="text-light">Información cliente</h3>'+
                            '<ul class="nav nav-pills nav-stacked nav-transparent">'+
                            '<li><a href="#"><span class="badge pull-right">'+element['nombres']+'</span>Nombre:</a></li>'+
                            '<li><a href="#"><span class="badge pull-right">'+element['prioridad']+'</span>Prioridad:</a></li>'+
                          '</ul>');
                          });
                      }else{
                       $('#info').css({"display":"none"});
                     }
                   }
                 });
     }


  </script>

</body>
</html>
