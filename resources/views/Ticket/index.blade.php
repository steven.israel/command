<!DOCTYPE html>
<html lang="en">

<head>
    <title>Commandline</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->
    @include('../Layout/links')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

</head>

<body class="menubar-hoverable header-fixed menubar-pin ">

    @include('../Layout/navbar');

    <!-- start informe -->

    <div id="base">

        <!-- BEGIN OFFCANVAS LEFT -->
        <div class="offcanvas">
        </div>
        <!--end .offcanvas-->
        <!-- END OFFCANVAS LEFT -->

        <div id="content">
            <section>
                <div class="section-header">
                    <ol class="breadcrumb">
                        <li class="active">Tickets</li>
                    </ol>
                </div>
                <div class="section-body">
                    <div class="row">
                        <div class="col-lg-12">

                            @if(Session::has('danger'))
                            <div class="alert alert-warning">
                                {{Session::get('danger')}}
                            </div>
                            @endif
                            <div class="card card-printable style-default-light">
                                <div class="card-head">
                                    <div class="tools">
                                        <div class="btn-group">
                                            <!-- <a class="btn btn-floating-action btn-primary" href="javascript:void(0);" onclick="javascript:window.print();"><i class="md md-print"></i></a> -->
                                            <!-- <a class="btn btn-floating-action btn-primary" href="{{ url('dashboardSeguridad') }}" ><i class="md md-arrow-back"></i></a> -->
                                        </div>
                                        <div class="btn-group">

                                        </div>
                                    </div>
                                </div>
                                <!--end .card-head -->
                                <div class="card-body style-default-bright">

                                    <!-- BEGIN INVOICE HEADER -->
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <h1 class="text-light text-default-light">Tickets {{$titulo}}</h1>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="well">
                                                <a href="{{ route('ticket.create') }}" style="width: 100%;"
                                                    class="btn btn-info">
                                                    <div class="clearfix">
                                                        <div class="btn-group">
                                                            Crear Ticket
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end .row -->
                                    <!-- END INVOICE HEADER -->

                                    <br>
                                    <!-- END INVOICE DESCRIPTION -->
                                    <!-- BEGIN INVOICE PRODUCTS -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="display nowrap" id="example" style="width: 100%;">
                                                    <thead>
                                                        <th>Numero de caso</th>
                                                        <th>Problema</th>
                                                        <th>Cliente</th>
                                                        <th>Codigo Cliente</th>
                                                        <th>Contacto</th>
                                                        <th>Descripcion</th>
                                                        <th>Prioridad</th>
                                                        <th>Tecnico</th>
                                                        <th>Ejecutivo</th>
                                                        <th>Estado del Ticket</th>
                                                        <th>Estado</th>
                                                        <th>Tipo de ticket</th>
                                                        <th>Abierto</th>
                                                        <th>Programado</th>
                                                        <th>Acciones</th>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($tickets as $ticket)
                                                        <tr
                                                            onclick="location.href='{{action('TicketController@show', $ticket->idticket)}}'">
                                                            <td>{{$ticket->numeroDeCaso}}</td>
                                                            <td>{{$ticket->titulo}}</td>
                                                            <td>
                                                                {{$ticket->cliente->empresa->nombre}}
                                                            </td>
                                                            <td>
                                                                {{$ticket->cliente->empresa->codigo}}
                                                            </td>
                                                            <td>{{ $ticket->cliente->nombres }}
                                                                {{ $ticket->cliente->apellidos }}
                                                            </td>
                                                            <td>
                                                                @php
                                                                echo substr($ticket->descripcion,15,55);
                                                                @endphp
                                                            ...</td>
                                                            <td>
                                                                
                                                                <button type="button"
                                                                    class="btn ink-reaction btn-raised btn-xs"
                                                                    style="background: {{$ticket->prioridad->color}}; color: white; font-weight: bold;">{{$ticket->prioridad->nombre}}</button>
                                                            </td>
                                                            

                                                            <td>
                                                                @php if($ticket->tecnico == 0 ){
                                                                    echo 'No asignado';
                                                                }else{
                                                                    echo $ticket->tecnicos->nombres;
                                                                }

                                                                @endphp
                                                            </td>
                                                            <td>
                                                                @php if($ticket->comercial == 0 ){
                                                                  echo 'No asignado';
                                                              }else{
                                                                  echo $ticket->ejecutivo->nombres;
                                                              }
                                  
                                                              @endphp
                                                              </td>
                                                            <td>
                                                                @if(date("Y-m-d") <= date("Y-m-d", strtotime($ticket->cliente->empresa->fechaFin)))
                                                                    <button type="button"
                                                                        class="btn ink-reaction btn-raised btn-xs btn-success">Vigente</button>
                                                                    @else
                                                                    <button type="button"
                                                                        class="btn ink-reaction btn-raised btn-xs btn-danger">Caducado</button>
                                                                    @endif
                                                            </td>
                                                            <td>
                                                                <button type="button"
                                                                    class="btn ink-reaction btn-raised btn-xs"
                                                                    style="background: {{$ticket->estado->color}}; color: white; font-weight: bold;">{{$ticket->estado->nombre}}</button>
                                                            </td>
                                                            <td>
                                                                @if($ticket->programado == 1)
                                                                <button type="button"
                                                                    class="btn ink-reaction btn-raised btn-xs btn-success">Programado</button>
                                                                @else
                                                                <button type="button"
                                                                    class="btn ink-reaction btn-raised btn-xs btn btn-secondary">   </button>
                                                                @endif
                                                            </td>

                                                            
                                                            <td width="1%">
                                                                @if($ticket->abierto == 0)
                                                                <button class="btn btn-danger btn-xs"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    data-original-title="Ticket Cerrado" type="submit">
                                                                    <span class="md md-lock-outline"></span></button>
                                                                @else
                                                                <button class="btn btn-default btn-xs"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    data-original-title="Ticket Abierto" type="submit">
                                                                    <span class="md md-lock-open"></span></button>
                                                                @endif
                                                            </td>
                                                            <td width="1%">
                                                                @if($ticket->programado == 1)
                                                                <button hover="Enviar alerta" data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    data-original-title="Ticket Programado"
                                                                    onclick="modal();" type="button" id="set"
                                                                    class="btn btn-success btn-xs">
                                                                    <i id="clase"
                                                                        class="glyphicon glyphicon-calendar"></i>
                                                                </button>
                                                                @endif
                                                            </td>
                                                            <td width="1%">
                                                                @if($ticket->EstadoTicket_idEstadoTicket == 5)
                                                                <form
                                                                    action="{{action('TicketController@destroy', $ticket->idticket)}}"
                                                                    method="post">
                                                                    {{csrf_field()}}
                                                                    <input name="_method" type="hidden" value="DELETE">
                                                                    <button class="btn btn-danger btn-xs"
                                                                        data-toggle="tooltip" data-placement="top"
                                                                        data-original-title="Archivar Ticket"
                                                                        type="submit">
                                                                        <span class="md md-exit-to-app"></span></button>
                                                                </form>

                                                                @endif
                                                            </td>

                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--end .col -->
                                    </div>
                                    <!--end .row -->
                                    <!-- END INVOICE PRODUCTS -->

                                </div>
                                <!--end .card-body -->
                            </div>
                            <!--end .card -->
                        </div>
                        <!--end .col -->
                    </div>
                    <!--end .row -->
                </div>
                <!--end .section-body -->
            </section>
        </div>
        <!--end #content-->



        <!-- BEGIN OFFCANVAS RIGHT -->
        <div class="offcanvas">


        </div>
        <!--end .offcanvas-->
        <!-- END OFFCANVAS RIGHT -->

    </div>



    <!-- end informe -->


    @include('../Layout/menu')


    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
    <script src="{{ asset('js/libs/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/libs/spin.js/spin.min.js')}}"></script>
    <script src="{{ asset('js/libs/autosize/jquery.autosize.min.js')}}"></script>
    <script src="{{ asset('js/libs/moment/moment.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.time.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.resize.min.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.orderBars.js')}}"></script>
    <script src="{{ asset('js/libs/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('js/libs/flot/curvedLines.js')}}"></script>
    <script src="{{ asset('js/libs/jquery-knob/jquery.knob.min.js')}}"></script>
    <script src="{{ asset('js/libs/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{ asset('js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
    <script src="{{ asset('js/libs/d3/d3.min.js')}}"></script>
    <script src="{{ asset('js/libs/d3/d3.v3.js')}}"></script>
    <script src="{{ asset('js/libs/rickshaw/rickshaw.min.js')}}"></script>
    <script src="{{ asset('js/core/source/App.js')}}"></script>
    <script src="{{ asset('js/core/source/AppNavigation.js')}}"></script>
    <script src="{{ asset('js/core/source/AppOffcanvas.js')}}"></script>
    <script src="{{ asset('js/core/source/AppCard.js')}}"></script>
    <script src="{{ asset('js/core/source/AppForm.js')}}"></script>
    <script src="{{ asset('js/core/source/AppNavSearch.js')}}"></script>
    <script src="{{ asset('js/core/source/AppVendor.js')}}"></script>
    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'pdf', 'excel'
                ],
                "language": {
                    "search": "Buscar:",
                    paginate: {
                        next: '&#8594;', // or '→'
                        previous: '&#8592;' // or '←'
                    }
                },
                "order": [[0, "desc"]]
            });
        });
    </script>
</body>

</html>
