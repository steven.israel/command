@extends('Layout.layout')
@section('content')
<div id="base">
<div class="row">
	<section class="content" id="content">
		<div class="col-md-8 col-md-offset-2">
			<div class="section-header">
	            <ol class="breadcrumb">
	               <li><a href="{{ route('cliente.index') }}">Lista de Clientes</a></li>
	              <li class="active">Nuevo Cliente</li>
	            </ol>
          	</div>
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Nuevo Cliente</h3>
				</div>
				<div class="panel-body">
					<div class="table-container">
						<form method="POST" action="{{ route('cliente.store') }}"  role="form" id="accion_form">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Nombres</label>
										<input type="text" name="nombres" value="{{ old('nombres') }}" class="form-control input-sm">
										<p style="color: red;">
											@if($errors->first('nombres'))
												{{ $errors->first('nombres') }}
											@else
												Campo Requerido
											@endif
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Apellidos</label>
										<input type="text" name="apellidos" value="{{ old('apellidos') }}" class="form-control input-sm">
										<p style="color: red;">
											@if($errors->first('apellidos'))
												{{ $errors->first('apellidos') }}
											@else
												Campo Requerido
											@endif
										</p>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="form-group">
										<label>Correo electrónico</label>
										<input type="email" name="email" value="{{ old('email') }}" class="form-control input-sm">
										<p style="color: red;">
											@if($errors->first('email'))
												{{ $errors->first('email') }}
											@else
												Campo Requerido
											@endif
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Telefono</label>
										<input type="text" name="telefono" value="{{ old('telefono') }}" class="form-control input-sm">
										<p style="color: red;">
											@if($errors->first('telefono'))
												{{ $errors->first('telefono') }}
											@else
												Campo Requerido
											@endif
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Celular</label>
										<input type="text" name="celular" value="{{ old('celular') }}" class="form-control input-sm">
										@if($errors->first('nombres'))
										<p style="color: red;">{{ $errors->first('nombres') }}</p></p>
										@else
										<p style="color: green;">Opcional</p>
										@endif
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Cargo</label>
										<input type="text" name="cargo" value="{{ old('cargo') }}" class="form-control input-sm">
										<p style="color: red;">
											@if($errors->first('cargo'))
												{{ $errors->first('cargo') }}
											@else
												Campo Requerido
											@endif
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Estado</label>
										<select name="estado" id="estado" class="form-control input-sm">
											<option value="0">Seleccionar Estado</option>
											<option value="1">Activo</option>
											<option value="2">Inactivo</option>
										</select>
										<p style="color: red;">{{ $errors->first('estado') }}</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="form-group">

										<label for="idempresa">Empresas</label>
										<select id="idempresa" onchange="select()" name="idempresa" class="form-control selectpicker"  data-show-subtext="true" data-live-search="true">
											<option value="0">Seleccionar Empresa</option>
											@if($empresas->count())
											@foreach($empresas as $list)
											<option value="{{ $list->idempresa}}">{{ $list->nombre }}</option>
											@endforeach
											@endif
										</select>
										<p style="color: red;">
											@if($errors->first('idempresa'))
												{{ $errors->first('idempresa') }}
											@else
												Campo Requerido
											@endif
										</p>

									</div>
								</div>
							</div>
							<div class="row">

								<div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
									<input type="submit"  value="Guardar" class="btn btn-success btn-block">
								</div>

							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>
	</div>
    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
	@endsection
