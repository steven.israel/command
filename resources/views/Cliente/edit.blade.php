@extends('Layout.layout')
@section('content')
<div class="row">
    <section class="content" id="content">
        <div class="col-md-8 col-md-offset-2">
            <div class="section-header">
                <ol class="breadcrumb">
                    <li><a href="{{ route('cliente.index') }}">Lista de Clientes</a></li>
                    <li class="active">Actualizar Cliente</li>
                </ol>
            </div>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Error!</strong> Revise los campos obligatorios.<br>
            </div>
            @endif
            @if(Session::has('success'))
            <div class="alert alert-info">
                {{Session::get('success')}}
            </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Nuevo cliente</h3>
                </div>
                <div class="panel-body">
                    <div class="table-container">
                        <form method="POST" action="{{ route('cliente.update',$cliente->idcliente) }}" role="form"
                            id="accion_form">
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value="PATCH">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="nombres" id="nombres" class="form-control input-sm"
                                            value="{{$cliente->nombres}}">
                                        <p style="color: red;">
                                            @if($errors->first('nombres'))
                                            {{ $errors->first('nombres') }}
                                            @else
                                            Campo Requerido
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Apellidos</label>
                                        <input type="text" name="apellidos" id="apellidos" class="form-control input-sm"
                                            value="{{$cliente->apellidos}}">
                                        <p style="color: red;">
                                            @if($errors->first('apellidos'))
                                            {{ $errors->first('apellidos') }}
                                            @else
                                            Campo Requerido
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>Correo</label>
                                        <input type="text" name="email" id="email" class="form-control input-sm"
                                            value="{{$cliente->email}}">
                                        <p style="color: red;">
                                            @if($errors->first('email'))
                                            {{ $errors->first('email') }}
                                            @else
                                            Campo Requerido
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Telefono</label>
                                        <input type="text" name="telefono" id="telefono" class="form-control input-sm"
                                            value="{{$cliente->telefono}}">
                                        <p style="color: red;">
                                            @if($errors->first('telefono'))
                                            {{ $errors->first('telefono') }}
                                            @else
                                            Campo Requerido
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Celular</label>
                                        <input type="text" name="celular" id="celular" class="form-control input-sm"
                                            value="{{$cliente->celular}}">
                                        @if($errors->first('nombres'))
                                        <p style="color: red;">{{ $errors->first('nombres') }}</p>
                                        </p>
                                        @else
                                        <p style="color: green;">Opcional</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Cargo</label>
                                        <input type="text" name="cargo" id="cargo" class="form-control input-sm"
                                            value="{{$cliente->cargo}}">
                                        <p style="color: red;">
                                            @if($errors->first('cargo'))
                                            {{ $errors->first('cargo') }}
                                            @else
                                            Campo Requerido
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <select name="estado" id="estado" class="form-control input-sm">
                                            <option value="0">Seleccionar Estado</option>
                                            <option value="1" <?php if ($cliente->estado == 1): ?> selected
                                                <?php endif ?>>Activo</option>
                                            <option value="2" <?php if ($cliente->estado == 2): ?> selected
                                                <?php endif ?>>Inactivo</option>
                                        </select>
                                        <p style="color: red;">{{ $errors->first('estado') }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">

                                        <label for="idempresa">Empresas</label>
                                        <select id="idempresa" onchange="select()" name="idempresa"
                                            class="form-control">
                                            @if($empresas->count())
                                            @foreach($empresas as $list)
                                            @if($list->idempresa == $cliente->idempresa)
                                            <option value="{{ $list->idempresa}}" selected>{{ $list->nombre }}</option>
                                            @else
                                            <option value="{{ $list->idempresa}}">{{ $list->nombre }}</option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>
                                        <p style="color: red;">
                                            @if($errors->first('idempresa'))
                                            {{ $errors->first('idempresa') }}
                                            @else
                                            Campo Requerido
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
                                    <input type="submit" value="Actualizar" class="btn btn-success btn-block">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
    @endsection
