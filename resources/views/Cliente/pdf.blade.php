@extends('Layout.layout')
@section('content')
<!-- BEGIN BASE-->
<div id="base">
<div class="row">
  <section class="content" id="content">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="pull-left"><h3>Lista de Clientes</h3></div>
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>Nombres</th>
               <th>Apellidos</th>
               <th>Correo</th>
               <th>Telefono</th>
             </thead>
             <tbody>
              @if(isset($data))  
              @foreach($data as $libro)  
              <tr>
                <td>{{$libro->nombres}}</td>
                <td>{{$libro->apellidos}}</td>
                <td>{{$libro->email}}</td>
                <td>{{$libro->telefono}}</td>
                <td><a class="btn btn-primary btn-xs" href="{{action('ClienteController@edit', $libro->idcliente)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('ClienteController@destroy', $libro->idcliente)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">

                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </form>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
            </tbody>

          </table>
        </div>
      </div>
      
    </div>
  </div>
</section>
</div>

@endsection