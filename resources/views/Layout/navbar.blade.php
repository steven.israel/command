<!-- BEGIN HEADER-->
		<header id="header" class="menubar-hoverable header-inverse">
			<div class="headerbar">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="headerbar-left">
					<ul class="header-nav header-nav-options">
						<li class="header-nav-brand" >
							<div class="brand-holder">
								<a href="#">
									<span class="text-lg text-bold text-primary">Commandline</span>
								</a>
							</div>
						</li>
						<li>
							<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
								<i class="fa fa-bars"></i>
							</a>
						</li>
					</ul>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="headerbar-right">
					<ul class="header-nav header-nav-options">
						<li>
							<form class="navbar-search" method="get" action="{{ route('ticket.busqueda') }}"  role="form"  enctype="multipart/form-data" role="search">
								<div class="form-group">
									<input id="search" type="text" autocomplete="off" list="lista" class="form-control" name="headerSearch" placeholder="Enter your keyword">
									<datalist id="lista">
										
									  </datalist>
								</div>
								<button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
							</form>
						</li>
						<li class="dropdown hidden-xs" id="notificaciones">
							<a href="javascript:void(0);" id="visto" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
								<i class="fa fa-bell"></i><sup id="sup" class="badge style-danger"><span id="tot"></span></sup>
							</a>
							<ul class="dropdown-menu animation-expand" id="notificacion" style="height: 200px; overflow-y: scroll;">
									
							</ul>
						</li>
						<!-- <li class="dropdown hidden-xs">
							<a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
								<i class="fa fa-area-chart"></i>
							</a>
							<ul class="dropdown-menu animation-expand">
								<li class="dropdown-header">Server load</li>
								<li class="dropdown-progress">
									<a href="javascript:void(0);">
										<div class="dropdown-label">
											<span class="text-light">Server load <strong>Today</strong></span>
											<strong class="pull-right">93%</strong>
										</div>
										<div class="progress"><div class="progress-bar progress-bar-danger" style="width: 93%"></div></div>
									</a>
								</li>
								<li class="dropdown-progress">
									<a href="javascript:void(0);">
										<div class="dropdown-label">
											<span class="text-light">Server load <strong>Yesterday</strong></span>
											<strong class="pull-right">30%</strong>
										</div>
										<div class="progress"><div class="progress-bar progress-bar-success" style="width: 30%"></div></div>
									</a>
								</li>
								<li class="dropdown-progress">
									<a href="javascript:void(0);">
										<div class="dropdown-label">
											<span class="text-light">Server load <strong>Lastweek</strong></span>
											<strong class="pull-right">74%</strong>
										</div>
										<div class="progress"><div class="progress-bar progress-bar-warning" style="width: 74%"></div></div>
									</a>
								</li>
							</ul>
						</li> --><!--end .dropdown -->
					</ul><!--end .header-nav-options -->
					<ul class="header-nav header-nav-profile">
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
								<span class="profile-info">
									{{auth()->user()->nombres}} {{auth()->user()->apellidos}}
									<small>
										@if(auth()->user()->role == 1)
											Administrador
										@elseif(auth()->user()->role == 2)
											Moderador
										@elseif(auth()->user()->role == 3)
											Técnico 1 (junior)
										@elseif(auth()->user()->role == 4)
											Técnico 2 (senior) 
										@elseif(auth()->user()->role == 5)
											Dpto. Ejecutivo/ Comercial.
										@endif
									</small>
								</span>
							</a>
							<ul class="dropdown-menu animation-dock">
								<li class="dropdown-header">Config</li>
								<!-- <li><a href="../../html/pages/profile.html">My profile</a></li>
								<li><a href="../../html/pages/blog/post.html">My blog <span class="badge style-danger pull-right">16</span></a></li>
								<li><a href="../../html/pages/calendar.html">My appointments</a></li> -->
								<li class="divider"></li>
								<!-- <li><a href="../../html/pages/locked.html"><i class="fa fa-fw fa-lock"></i> Lock</a></li> -->
								<li><a href="{{ url('logout') }}"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
							</ul><!--end .dropdown-menu -->
						</li><!--end .dropdown -->
					</ul><!--end .header-nav-profile -->
					<ul class="header-nav header-nav-toggle">
						<li>
							<a class="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
								<i class="fa fa-ellipsis-v"></i>
							</a>
						</li>
					</ul><!--end .header-nav-toggle -->
				</div><!--end #header-navbar-collapse -->
			</div>
			<div id="notifications" style="cursor: pointer;
			position: fixed;
			right: 0px;
			z-index: 9999;
			bottom: 0px;
			margin-bottom: 22px;
			margin-right: 15px;
			max-width: 300px;" ></div>
			
		</header>
		<!-- END HEADER-->