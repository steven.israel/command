<!-- BEGIN STYLESHEETS -->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/bootstrap.css') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/materialadmin.css') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/font-awesome.min.css') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/material-design-iconic-font.min.css') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/libs/rickshaw/rickshaw.css') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/libs/morris/morris.core.css') }}" />
	<!-- licks para tablas -->
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/libs/DataTables/jquery.dataTables.css?1423553989') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/libs/DataTables/extensions/dataTables.colVis.css') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/libs/DataTables/extensions/dataTables.tableTools.css') }}" />
	<link type="text/css" rel="stylesheet" href="{{ asset('css/theme-2/libs/wizard/wizard.css?1425466601') }}" />
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />

	<style>
	
		.table-responsive::-webkit-scrollbar-track
		{
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
			background-color: #F5F5F5;
		}

		.table-responsive::-webkit-scrollbar
		{
			width: 10px;
			background-color: #F5F5F5;
		}

		.table-responsive::-webkit-scrollbar-thumb
		{
			background-color: #F90; 
			background-image: -webkit-linear-gradient(45deg,
													rgba(255, 255, 255, .2) 25%,
													transparent 25%,
													transparent 50%,
													rgba(255, 255, 255, .2) 50%,
													rgba(255, 255, 255, .2) 75%,
													transparent 75%,
													transparent)
		}

	</style>
		
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script> 
<!-- END STYLESHEETS -->	 