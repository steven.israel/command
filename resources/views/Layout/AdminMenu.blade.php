

<!-- BEGIN DASHBOARD -->
				<li>
					<a href="{{ url('/dashboard') }}" class="active">
						<div class="gui-icon"><i class="md md-home"></i></div>
						<span class="title">Dashboard </span>
					</a>
				</li>

				@include('Layout.MenuComun') {{--ACA DEJAMOS EL MENU COMUN DE TODOS LOS ROLES  CON SUS DATOS--}}
@if(auth()->user()->role == 1 || auth()->user()->role == 2 || auth()->user()->role == 4 )
				<li>
					<a href="{{ url('/empresa') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'empresa') ? 'active' : '' }}">
						<div class="gui-icon"><i class="md md-account-balance"></i></div>
						<span class="title">Empresas Cliente</span>
					</a>
				</li><!--end /menu-li -->
				<!-- END CONTRATOS -->

				<!-- BEGIN EMAIL -->
				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="md md-person"></i></div>
						<span class="title">Usuarios</span>
					</a>
					<!--start submenu -->
					<ul>
						<li><a href="{{ url('/cliente') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'cliente') ? 'active' : '' }}"><span class="title">Cliente</span></a></li>
						@if(auth()->user()->role == 1)					
						<li><a href="{{ url('/usuario') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'usuario') ? 'active' : '' }}"><span class="title">Usuario</span></a></li>
						@endif
					</ul><!--end /submenu -->
				</li><!--end /menu-li -->
				<!-- END EMAIL -->

				<!-- BEGIN UI -->
				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="md md-settings"></i></div>
						<span class="title">Configuración</span>
					</a>
					<!--start submenu -->
					<ul>
						<li><a href="{{ url('/tipoContrato') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'tipoContrato') ? 'active' : '' }}"><span class="title">Tipos de Contratos</span></a></li>
						<li><a href="{{ url('/prioridad') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'prioridad') ? 'active' : '' }}"><span class="title">Prioridades</span></a></li>
						@if(auth()->user()->role == 1)		
						<li><a href="{{ route('marca.index') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'marca') ? 'active' : '' }}"><span class="title">Marcas</span></a></li>
						@endif
						<li><a href="{{ url('/archivadosrma') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'archivadosrma') ? 'active' : '' }}"><span class="title">RMA Archivados</span></a></li>
					</ul><!--end /submenu -->
				</li><!--end /menu-li -->
				<!-- END UI -->
		
@endif