<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
	<title>Commandline</title>
	@include('Layout/links')

</head>
<body class=" header-fixed menubar-pin">

	@include('Layout/navbar');

	@yield('content')

	@include('Layout/menu')

	@include('Layout/scripts')
	<script src="{{ asset('js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
	<script type="text/javascript">

	<?php
                if ( !empty($chart_area) ) {
                    ?>
                        Morris.Line({
                            element: 'chart_area',
                            data: [<?php echo $chart_area;?>],
                            xkey: 'titulo',
                            ykeys: ['abiertos', 'cerrados'],
                            labels: ['Abiertos', 'Cerrados'],
                            pointSize: 3,
                            hideHover: 'auto',
                            pointFillColors: ['#4DB6AC','#FFC107','#c62828','#212121'],
                            pointStrokeColors: ['#4DB6AC','#FFC107','#c62828','#212121'],
                            lineColors: ['#4DB6AC','#FFC107','#c62828','#212121'],
                            fillOpacity: 0.8,
                            parseTime: false,
                            resize: true,
                            redraw: true
                        });
                    <?php
                }
            ?>


	</script>

	@yield('script')

</body>
</html>
