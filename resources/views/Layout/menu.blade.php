<?php 

	namespace App\Http\Controllers;

	use App\user;
	use App\ticket;
	use DB;

	if (auth()->user()->role == 5 || auth()->user()->role == 3 || auth()->user()->role == 4) {

		$tickets = DB::select('SELECT COUNT(t.idticket) AS total, t.EstadoTicket_idEstadoTicket AS estado, e.nombre FROM ticket as t  
									INNER JOIN estadoticket AS e ON e.idEstadoTicket = t.EstadoTicket_idEstadoTicket 
									WHERE t.tipo_ingreso = 1 and tecnico = '.auth()->user()->id.' or comercial = '.auth()->user()->id.'
									GROUP BY t.EstadoTicket_idEstadoTicket;');
		
		$archivado = DB::select('SELECT COUNT(*) as total, EstadoTicket_idEstadoTicket as estado  FROM ticket WHERE archivado = 1 and tecnico = '.auth()->user()->id.' GROUP BY EstadoTicket_idEstadoTicket;');
		
	}else{

	    $correo = DB::select('SELECT COUNT(*) as total, EstadoTicket_idEstadoTicket as estado  FROM ticket WHERE tipo_ingreso = 2 GROUP BY EstadoTicket_idEstadoTicket;');

		$tickets = DB::select('SELECT COUNT(t.idticket) AS total, t.EstadoTicket_idEstadoTicket AS estado, e.nombre FROM ticket as t  
									INNER JOIN estadoticket AS e ON e.idEstadoTicket = t.EstadoTicket_idEstadoTicket 
									WHERE t.tipo_ingreso = 1
									GROUP BY t.EstadoTicket_idEstadoTicket;');

		$archivado = DB::select('SELECT COUNT(*) as total, EstadoTicket_idEstadoTicket as estado  FROM ticket WHERE archivado = 1 GROUP BY EstadoTicket_idEstadoTicket;');

	}
	$rol = auth()->user()->role;
	if ($rol == 3 || $rol == 4 || $rol == 5) {
            $todos = ticket::where('comercial', auth()->user()->id)->orWhere('tecnico', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        } else {
            $todos = ticket::orderBy('created_at', 'DESC')->get();
        }

?>

<!-- BEGIN MENUBAR-->
<div id="menubar" class="menubar">
	<div class="menubar-fixed-panel">
		<div>
			<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
				<i class="fa fa-bars"></i>
			</a>
		</div>
		<div class="expanded">
			<a href="{{ url('/dashboard') }}">
				<span class="text-lg text-bold text-primary ">Commandline </span>
			</a>
		</div>
	</div>
	<div class="menubar-scroll-panel">

		<!-- BEGIN MAIN MENU -->
		<ul id="main-menu" class="gui-controls">

			@if(auth()->user()->role == 1 || auth()->user()->role == 2 || auth()->user()->role == 5 )
				@include('Layout.AdminMenu') {{--menu de administradores only--}}
			@elseif(auth()->user()->role == 3 || auth()->user()->role == 4)
				@include('Layout.MenuComun') {{--menu de los demas roles--}}
			@endif

			
		</ul><!--end .main-menu -->
		<!-- END MAIN MENU -->

		<div class="menubar-foot-panel">
			<small class="no-linebreak hidden-folded">
				<span class="opacity-75">Copyright &copy; 2019</span> <strong>Commandline</strong>
			</small>
		</div>
	</div><!--end .menubar-scroll-panel-->
</div><!--end #menubar-->
			<!-- END MENUBAR -->

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
	function salida(e,href){
		e.preventDefault();
		if ( $("#accion_form").length > 0) {
			console.log($("#accion_form").data("changed",true));
			if ($("#accion_form").data("changed",true)) {
				swal({
					title: "Usteda está seguro de salir de esta sección?",
					icon: "warning",
					buttons: ["Confirmar", "Cancelar"],
					dangerMode: true,
				}).then((willDelete) => {
					if (!willDelete) {
						window.location.href = href.href;
					} else {
						return false;
					}
				});
			}else{
				window.location.href = href.href;
			}
		}else{
			window.location.href = href.href;
		}
	}
</script>