				
	@if(auth()->user()->role != 5)				

				<li>
					<a href="{{ route('ticket.create') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'ticket/create') ? 'active' : '' }}">
						<div class="gui-icon"><i class="md md-receipt"></i></div>
						<span class="title">Crear Tickets</span>
					</a>
				</li>

				@if (Auth()->user()->role ==1 || Auth()->user()->role == 2)
					@if($correo)
						<li>
							<a href="{{ url('/mails') }}" onclick="salida(event,this)"  class="{{ (\Request::path() == 'mails') ? 'active' : '' }}">
								<div class="gui-icon"><i class="md md-email"></i></div>
								<span class="title">Correos Recibidos <span class="badge">{{ $correo[0]->total }}</span></span>
							</a>
						</li>
					@endif
				@endif
	@endif
				
				@if($tickets)
				
					@foreach($tickets as $list)
						<li>
							<?php if ($list->estado == 1): ?>
								<a href="{{ url('/ingresados') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'ingresados') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-call-received"></i></div>
									<span class="title">Mis tickets <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 2): ?>
								<a href="{{ url('/revision') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'revision') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-remove-red-eye"></i></div>
									<span class="title">Tickets en Revisión <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 3): ?>
								<a href="{{ url('/proceso') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'proceso') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-settings"></i></div>
									<span class="title">Tickets en Proceso <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 4): ?>
								<a href="{{ url('/respuesta') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'respuesta') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-quick-contacts-mail"></i></div>
									<span class="title">Tickets en Espera <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 5): ?>
								<a href="{{ url('/cerrado') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'cerrado') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-lock-outline"></i></div>
									<span class="title">Tickets Cerrados <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 6): ?>
								<a href="{{ url('/re-abierto') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 're-abierto') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-lock-open"></i></div>
									<span class="title">Tickets {{ $list->nombre }} <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 7): ?>
								<a href="{{ url('/cierre_1') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'cierre_1') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-close"></i></div>
									<span class="title">Tickets {{ $list->nombre }} <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 8): ?>
								<a href="{{ url('/cierre_3') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'cierre_3') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-close"></i></div>
									<span class="title">Tickets {{ $list->nombre }} <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 9): ?>
								<a href="{{ url('/cierre_4') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'cierre_4') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-close"></i></div>
									<span class="title">Tickets {{ $list->nombre }} <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 10): ?>
								<a href="{{ url('/cierre_5') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'cierre_5') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-close"></i></div>
									<span class="title">Tickets {{ $list->nombre }} <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php elseif ($list->estado == 11): ?>
								<a href="{{ url('/cierre_6') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'cierre_6') ? 'active' : '' }}">
									<div class="gui-icon"><i class="md md-close"></i></div>
									<span class="title">Tickets {{ $list->nombre }} <span class="badge">{{ $list->total }}</span></span>
								</a>
							<?php endif ?>
						
					@endforeach

					<?php if ($todos): ?>
					<a href="{{ url('/todos') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'todos') ? 'active' : '' }}">
						<div class="gui-icon"><i class="md md-call-received"></i></div>
						<span class="title">Todos <span class="badge">@php
							count($todos);
						@endphp</span></span>
					</a>
					<?php endif ?>
				</li>

				@endif

				{{-- @if($archivado)
					<li>
						<a href="{{ url('/ticketArchivados') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'ticketArchivados') ? 'active' : '' }}">
							<div class="gui-icon"><i class="md md-account-balance"></i></div>
							<span class="title">Tickets Archivados <span class="badge">{{ $archivado[0]->total }}</span></span>
						</a>
					</li>
				@endif --}}

				@if(auth()->user()->role >= 1 && auth()->user()->role <= 4)
				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="md md-description"></i></div>
						<span class="title">RMA</span>
					</a>
					<!--start submenu -->
					<ul>
						<li><a href="{{ route('rma.index') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'rma') ? 'active' : '' }}"><span class="title">Listado</span></a></li>
						@if(auth()->user()->role != 3 )

							<li><a href="{{ url('/entrada') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'entrada') ? 'active' : '' }}"><span class="title">Crear Entrada</span></a></li>
						@endif

						<!-- <li><a href="{{ url('/salida') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'salida') ? 'active' : '' }}"><span class="title">Crear Salida</span></a></li> -->
					</ul><!--end /submenu -->
				</li>
				@endif

@if(auth()->user()->role == 1 || auth()->user()->role == 2 || auth()->user()->role == 4 )
				<li>
					<a href="{{ url('/empresa') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'empresa') ? 'active' : '' }}">
						<div class="gui-icon"><i class="md md-account-balance"></i></div>
						<span class="title">Empresas Cliente</span>
					</a>
				</li><!--end /menu-li -->
				<!-- END CONTRATOS -->

				<!-- BEGIN EMAIL -->
				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="md md-person"></i></div>
						<span class="title">Usuarios</span>
					</a>
					<!--start submenu -->
					<ul>
						<li><a href="{{ url('/cliente') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'cliente') ? 'active' : '' }}"><span class="title">Cliente</span></a></li>
						@if(auth()->user()->role == 1)					
						<li><a href="{{ url('/usuario') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'usuario') ? 'active' : '' }}"><span class="title">Usuario</span></a></li>
						@endif
					</ul><!--end /submenu -->
				</li><!--end /menu-li -->
				<!-- END EMAIL -->

				<!-- BEGIN UI -->
				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="md md-settings"></i></div>
						<span class="title">Configuración</span>
					</a>
					<!--start submenu -->
					<ul>
						<li><a href="{{ url('/tipoContrato') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'tipoContrato') ? 'active' : '' }}"><span class="title">Tipos de Contratos</span></a></li>
						<li><a href="{{ url('/prioridad') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'prioridad') ? 'active' : '' }}"><span class="title">Prioridades</span></a></li>
						@if(auth()->user()->role == 1)		
						<li><a href="{{ route('marca.index') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'marca') ? 'active' : '' }}"><span class="title">Marcas</span></a></li>
						@endif
						<li><a href="{{ url('/archivadosrma') }}" onclick="salida(event,this)" class="{{ (\Request::path() == 'archivadosrma') ? 'active' : '' }}"><span class="title">RMA Archivados</span></a></li>
					</ul><!--end /submenu -->
				</li><!--end /menu-li -->
				<!-- END UI -->
		
@endif
