<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Document</title>
    </head>
    <body>
        <div style="">
            <section>
                <div style="">
                    <center>
                        <img
                            width="280px"
                            height="170px"
                            src="https://command.latmobile.com/files/Commandline-id2020-logotipo-vertical-color-plano.png"
                            alt=""
                        />
                    </center>
                </div>
            </section>
            <div style="">
                <!-- 1-enviar notificacion al cliente al crear el ticket  -->
                @if($mail->tipo == 1)

                <p>Estimado Cliente,</p>
                <br />
                <p style="">
                    Su solicitud ha sido recibida y está siendo procesada en
                    nuestro sistema de gestión de incidentes técnicos.
                </p>
                <br />
                <p align="" style="">
                    Podrá hacer seguimiento a su caso con el número de ticket:
                    <b>{{$mail->numeroDeCaso}}</b><br />
                    <br />Dado que usted actualmente <b>NO</b> cuenta con
                    soporte técnico por parte de nuestra empresa, su caso será
                    enviado a nuestro departamento comercial, desde el cual un
                    agente se comunicará con usted en breve.<br /><br />
                </p>
                <br /><br />
                <p>¡Muchas gracias por contactarnos!</p>
                <p>Equipo de Soporte.</p>
                <br />
                <p>
                </p>
                <!--  -->
                @elseif($mail->tipo == 2)
                <p>Estimado Cliente,</p>
                <br />
                <p style="">
                    Su solicitud ha sido recibida y está siendo procesada en
                    nuestro sistema de gestión de incidentes técnicos.
                </p>
                <br />
                <p align="" style="">
                    Podrá hacer seguimiento a su caso con el número de ticket:
                    <b>{{$mail->numeroDeCaso}}</b><br />
                    <br />Dado que usted actualmente cuenta con soporte técnico
                    por parte de nuestra empresa, su caso será enviado a nuestro
                    departamento comercial, desde el cual un agente se
                    comunicará con usted en breve.<br /><br />
                </p>
                <br /><br />
                <p>¡Muchas gracias por contactarnos!</p>
                <p>Equipo de Soporte.</p>
                <br />
                <p>
                </p>
                <!--  -->
                @elseif($mail->tipo == 3)
                <p><b>{{$mail->tecnico}}</b>,</p>
                <br />
                <p align="" style="">
                    Se te ha asignado el ticket número:
                    <b>{{$mail->numeroDeCaso}}</b> con la siguiente descripción
                    del problema: <br /><br />
                    <b>{!!$mail->descripcion!!}</b> <br /><br />
                    Este ticket es de prioridad {{$mail->prioridad}} y fue
                    recibido en la fecha {{$mail->fecha}} y hora
                    {{$mail->hora}}. <br /><br />
                    Procura enviar una contestación lo más pronto posible y
                    recuerda que en caso de no poder realizar esta tarea puedes
                    asignarle este ticket a un compañero.
                </p>
                <br />
                <p>
                </p>
                <!-- 4- notificacion para reasignacion de ticket de un tecnico a otro -->
                @elseif($mail->tipo == 31)
                <p style="">
                    Te fue asignado el ticket {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    {{$mail->tecnico}}, Se te ha asignado el ticket número:
                    {{$mail->numeroDeCaso}} con la siguiente descripción del
                    problema: <br /><br />
                    {!!$mail->descripcion!!} <br /><br />
                    Este ticket es de prioridad {{$mail->prioridad}} y fue
                    recibido en la fecha {{$mail->fecha}} y hora
                    {{$mail->hora}}. <br /><br />
                </p>

                <!-- 4- notificacion para reasignacion de ticket de un tecnico a otro -->
                @elseif($mail->tipo == 4)
                <p><b>{{$mail->tecnico}}</b>,</p>
                <br />
                <p align="" style="">
                    Se te ha reasignado el ticket número
                    <b>{{$mail->numeroDeCaso}}</b> por parte de tu compañero
                    <b>{{$mail->tecnico_anterior}}</b>. El problema está
                    descrito en el siguiente mensaje: <br /><br />
                    <b>{!!$mail->descripcion!!}.</b> <br />
                    Este ticket es de prioridad {{$mail->prioridad}} y fue
                    recibido en la fecha {{$mail->fecha}} y hora
                    {{$mail->hora}}. <br /><br />
                    Procura enviar una contestación lo más pronto posible y
                    recuerda que en caso de no poder realizar esta tarea puedes
                    asignarle este ticket a otro compañero.
                </p>
                <!-- 5 - enviar notificacion al cliente de que su ticket ya fue asignado al tecnico -->
                @elseif($mail->tipo == 5)
                <p style="">
                    Su ticket del caso {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} - ha sido asignado a uno de nuestros
                    técnicos.
                </p>
                <p align="" style="">
                    Estimado Cliente, Nos complace informarle que su ticket
                    número {{$mail->numeroDeCaso}} - {{$mail->titulo}} - ha sido
                    recibido y asignado a uno de nuestros técnicos para iniciar
                    el proceso de resolución del caso. <br /><br />
                    Por favor tome en consideración lo siguiente: <br /><br />
                    El agente de soporte que registra el caso no es
                    necesariamente el mismo que desarrolle su seguimiento. Para
                    su correcta resolución, el caso podría ser remitido al
                    técnico correspondiente. <br />
                    Nuestro técnico podrá ponerse en contacto con usted de
                    manera recurrente si se requiere de información adicional
                    para solucionar su problema. <br />
                    El tiempo que transcurre entre la solicitud de información
                    por parte de nuestro técnico y su respuesta, no será
                    incluido como horas de trabajo invertidas en la resolución
                    del ticket. <br /><br />
                    Muchas gracias por contactarnos Equipo de Soporte.
                    <br /><br />
                @elseif($mail->tipo == 100){{--Primer aviso de mail--}}
                <p>Estimado Cliente,</p>
                <br />
                <p>
                    Se le informa que el ticket <b>{{$mail->numeroDeCaso}}</b> cambió de estado a Cerrado 
					debido a una falta de respuesta, si necesita solución al problema le 
					recomendamos enviar otro correo con su necesidad.
                </p>
                <br />

                <p>
					¡Gracias por contactarte a Commandline!
                </p><br>
                <p align="" style="">
                    Equipo de Soporte.
                </p>
                @elseif($mail->tipo == 23){{--segundo aviso de mail--}}
                <p style="">
                    Ticket con revisión pendiente. Caso {{$mail->numeroDeCaso}}
                    - {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    {{$mail->moderador}}, El técnico {{$mail->tecnico}} te
                    solicita una revisión del ticket {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} -. <br /><br />
                    {!!$mail->descripcion!!}. <br /><br />
                    Este ticket es de prioridad {{$mail->prioridad}} y fue
                    recibido en la fecha {{$mail->fecha}} y hora
                    {{$mail->hora}}. <br /><br />
                    Procura enviar una contestación lo más pronto posible.
                </p>
                @elseif($mail->tipo == 24){{-- aviso de respuesta de cliente
                mail--}}
                <p style="">
                    Nueva respuesta en la conversacion del Ticket
                    {{$mail->numeroDeCaso}}-.
                </p>
                <p align="" style="">
                    NOTA (NO CONTESTAR A ESTE CORREO) <br /><br />

                    Este ticket es de prioridad {{$mail->prioridad}} y fue
                    recibido en la fecha {{$mail->fecha}} y hora
                    {{$mail->hora}}. <br /><br />
                    Procura revisar lo más pronto posible.

                    <button>
                        <a
                            href="https://command.latmobile.com/ticket/{{$mail->nota}}"
                            >Ir a la conversacion</a
                        >
                    </button>
                </p>
                @elseif($mail->tipo == 25){{--Actualizacion del ticket a cerrado
                por falta de respuesta--}}
                <p style="">
                    Ticket con revisión3 pendiente. Caso {{$mail->numeroDeCaso}}
                    - {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    {{$mail->moderador}}, El técnico {{$mail->tecnico}} te
                    solicita una revisión del ticket {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} -. <br /><br />
                    {!!$mail->descripcion!!}. <br /><br />
                    Este ticket es de prioridad {{$mail->prioridad}} y fue
                    recibido en la fecha {{$mail->fecha}} y hora
                    {{$mail->hora}}. <br /><br />
                    Procura enviar una contestación lo más pronto posible.
                </p>
                @elseif($mail->tipo == 7)
                <p style="">
                    Su ticket del caso {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} - se encuentra en revisión por parte del
                    departamento comercial.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} - está siendo revisado en este momento por
                    un agente de nuestro departamento comercial. <br /><br />
                    Muchas gracias por contactarnos Equipo de Soporte.
                    <br /><br />
                </p>
                @elseif($mail->tipo == 8)
                <p style="">
                    Presupuesto pendiente de aprobación para ticket número
                    {{$mail->numeroDeCaso}} - {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} - fue revisado por nuestro departamento
                    comercial. <br /><br />
                    El caso requiere de recursos adicionales por lo que hemos
                    creado una cotización según sus requerimientos. Esta
                    cotización está a la espera de su aprobación. <br /><br />
                    Muchas gracias por contactarnos Área Comercial. <br /><br />
                </p>
                @elseif($mail->tipo == 9)
                <p style="">
                    El ticket presupuestado {{$mail->numeroDeCaso}} -
                    {{$mail->titulo}} - fue aceptado por el cliente.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su solicitud ha sido recibida y está
                    siendo procesada en nuestro sistema de gestión de incidentes
                    técnicos. <br /><br />
                    Podrá hacer seguimiento de su caso con el número de ticket:
                    {{$mail->numeroDeCaso}}. <br /><br />
                    Dado que usted actualmente cuenta con soporte técnico por
                    parte de nuestra empresa, su caso ha sido remitido a nuestro
                    equipo para brindarle una solución. <br />
                    De ser necesario, en breve un agente de soporte se
                    contactará con usted. <br /><br />
                    Por favor tome en consideración lo siguiente: <br />
                    Es importante que antes de solicitar soporte técnico el
                    cliente verifique la documentación del producto otorgada por
                    Commandline. <br />
                    Las solicitudes de soporte técnico serán recibidas dentro
                    del horario establecido acorde a la modalidad de servicio
                    contratado. Las solicitudes hechas fuera de este horario
                    serán tratadas el próximo día hábil. <br />
                    En el caso de que su solicitud no esté en el marco de lo
                    establecido en su contrato, usted será contactado por uno de
                    nuestros agentes del departamento comercial. <br />
                    El tiempo de respuesta a una solicitud podría diferir del
                    tiempo de resolución de la misma. El período de respuesta a
                    una solicitud se contará desde el registro inicial del caso
                    hasta la primera interacción registrada del técnico con el
                    cliente. <br />
                    Posterior a esto, el caso será remitido al técnico o equipo
                    de trabajo correspondiente. <br /><br />
                    Muchas gracias por contactarnos Equipo de Soporte.
                    <br /><br />
                </p>
                @elseif($mail->tipo == 10)
                <p style="">
                    Su ticket sobre el caso - {{$mail->numeroDeCaso}} - ha sido
                    creado satisfactoriamente.
                </p>
                <p align="" style="">
                    El ticket número: {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} - será cerrado dado que el presupuesto fue
                    rechazado por parte del cliente.
                </p>
                @elseif($mail->tipo == 11)
                <p style="">
                    Su ticket {{$mail->numeroDeCaso}} – {{$mail->titulo}} - ha
                    cambiado de estado.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número {{$mail->numeroDeCaso}}
                    ha cambiado de estado: <br /><br />
                    Su nuevo estado es: {{$mail->estado}}. <br /><br />
                    Si el cambio se trata de un error, agradecemos nos envíe una
                    nueva solicitud explicando el problema. <br /><br />
                    Muchas gracias Equipo de Soporte. <br /><br />
                </p>
                @elseif($mail->tipo == 12)
                <p style="">
                    Creación de nota en su ticket {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    Estimado Cliente, A su ticket número {{$mail->numeroDeCaso}}
                    le fue agregado una nueva nota. <br /><br />
                    {{$mail->nota}} <br /><br />
                    Muchas gracias, Equipo de Soporte. <br /><br />
                </p>
                @elseif($mail->tipo == 13)
                <p style="">
                    Solicitud de información sobre su ticket
                    {{$mail->numeroDeCaso}} – {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    Estimado Cliente, En este momento el técnico asignado a su
                    caso requiere que usted le ofrezca información adicional
                    para poder dar solución a su problema. <br /><br />
                    <b>{!!$mail->respuesta!!} </b><br /><br />
                    Por favor tome en consideración lo siguiente: <br />
                    El tiempo que transcurre entre la solicitud de información
                    por parte de nuestro técnico y su respuesta, no será
                    incluido como horas de trabajo invertidas en la resolución
                    del ticket. El pedido de información puede ser recurrente
                    dependiendo de la posible solución. <br /><br />
                    Muchas gracias por contactarnos Equipo de Soporte.
                    <br /><br />
                </p>
                @elseif($mail->tipo == 28)
                <p style="">
                    Mensaje interno sobre ticket {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    Estimado, En este momento el {{$mail->usuarioTipo}} asignado
                    al ticket {{$mail->numeroDeCaso}} requiere que usted le
                    ofrezca información adicional para poder dar solución.
                    <br /><br />
                    {!!$mail->respuesta!!} <br /><br />

                </p>

                @elseif($mail->tipo == 14)
                <p style="">
                    Solicitud (2 de 3) de información sobre su ticket
                    {{$mail->numeroDeCaso}} – {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número {{$mail->numeroDeCaso}}
                    tiene una solicitud de información que aún espera su
                    respuesta. <br /><br />
                    {!!$mail->respuesta!!} <br /><br />
                    Por favor tome en consideración lo siguiente: <br />
                    Esta es su segunda notificación. En caso de que se envíe una
                    tercera notificación y no recibamos respuesta de su parte,
                    el sistema cerrará el ticket automáticamente. El tiempo que
                    transcurre entre la solicitud de información por parte de
                    nuestro técnico y su respuesta, no será incluido como horas
                    de trabajo invertidas en la resolución del ticket. <br />
                    La solicitud de información puede ser recurrente dependiendo
                    de la posible solución. <br /><br />
                    Muchas gracias por su atención Equipo de Soporte.
                    <br /><br />
                </p>
                @elseif($mail->tipo == 15)
                <p style="">
                    Solicitud (3 de 3) de información sobre su ticket
                    {{$mail->numeroDeCaso}} – {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número {{$mail->numeroDeCaso}}
                    tiene una solicitud de información que aún espera su
                    respuesta. <br /><br />
                    {{$mail->respuesta_cliente}} <br /><br />
                    Por favor tome en consideración lo siguiente: <br />
                    En el caso de que no conteste esta solicitud de información,
                    el sistema cerrará su ticket automáticamente. Si su ticket
                    es dado de baja, puede solicitar una reanudación del mismo.
                    El tiempo que transcurre entre la solicitud de información
                    por parte de nuestro técnico y su respuesta, no será
                    incluido como horas de trabajo invertidas en la resolución
                    del ticket La solicitud de información puede ser recurrente
                    dependiendo de la posible solución. <br /><br />
                    Muchas gracias por su atención Equipo de Soporte.
                    <br /><br />
                </p>
                @elseif($mail->tipo == 16)
                <p style="">
                    El cliente ha respondido tu solicitud de información sobre
                    el ticket {{$mail->numeroDeCaso}} – {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    {{$mail->tecnico}}, El cliente que solicitó el ticket
                    {{$mail->numeroDeCaso}} – {{$mail->titulo}} - respondió a tu
                    solicitud de información con el siguiente mensaje:
                    <br /><br />
                    {{$mail->respuesta_cliente}} <br /><br />
                    Procura enviar una contestación lo más pronto posible y
                    recuerda que en caso de no poder realizar esta tarea puedes
                    asignarle este ticket a un compañero.
                </p>
                @elseif($mail->tipo == 17)
                <p style="">
                    Su ticket {{$mail->numeroDeCaso}} – {{$mail->titulo}} – ha
                    sido cerrado por falta de respuesta.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número: {{$mail->numeroDeCaso}}
                    ha sido cerrado automáticamente por nuestro sistema dado que
                    no se contestó a ninguna de las 3 (tres) solicitudes previas
                    de información realizada por nuestros técnicos. <br /><br />
                    Recuere que usted puede reabrir su ticket nuevamente en
                    cualquier momento si esto fuece necesario. <br />
                    Si esto es un error, agradecemos nos envíe una nueva
                    solicitud explicando el problema. <br /><br />
                    Muchas gracias, Equipo de Soporte. <br /><br />
                </p>
                @elseif($mail->tipo == 18)
                <p style="">
                    Cierre satisfactorio de su ticket {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} -.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número {{$mail->numeroDeCaso}} -
                    {{$mail->titulo}} - ha sido resuelto de manera satisfactoria
                    por parte de nuestros técnicos. A continuación se describe
                    el resultado: <br /><br />
                    {!!$mail->respuesta!!} <br /><br />
                    Si esto es un error y sigue experimentando los mismos
                    contratiempos reportados en su ticket, agradecemos nos envíe
                    una nueva solicitud explicando el problema.
                    <br /><br />
                    Muchas gracias por confiar en nuestro servicio de soporte
                    técnico Equipo de Soporte. <br /><br />
                </p>
                @elseif($mail->tipo == 19)
                <p style="">
                    Su ticket {{$mail->numeroDeCaso}} – {{$mail->titulo}} – ha
                    sido cerrado sin solución.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número {{$mail->numeroDeCaso}}
                    se ha cerrado debido a que por factores externos nuestros
                    técnicos no están en posibilidad de ofrecer soporte. A
                    continuación se describe el motivo: <br /><br />
                    {!!$mail->respuesta!!} <br /><br />
                    Si esto es un error, agradecemos nos envíe una nueva
                    solicitud explicando el problema. <br /><br />
                    Muchas gracias Equipo de Soporte. <br /><br />
                </p>
                @elseif($mail->tipo == 20)
                <p style="">
                    Su ticket {{$mail->numeroDeCaso}} – {{$mail->titulo}}- ha
                    sido cerrado ya que debe ser solucionado por personal
                    externo a la empresa.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número {{$mail->numeroDeCaso}} –
                    {{$mail->titulo}} - se ha cerrado debido a que nuestros
                    técnicos consideran que el problema planteado es externo a
                    Commandline y debe ser solucionado por personal externo a
                    nuestra empresa. A continuación se describe el motivo.
                    <br /><br />
                    {!!$mail->respuesta!!} <br /><br />
                    Si esto es un error, agradecemos nos envíe una nueva
                    solicitud explicando el problema. <br /><br />
                    Muchas gracias Equipo de Soporte. <br /><br />
                </p>
                @elseif($mail->tipo == 21)
                <p style="">
                    Su ticket {{$mail->numeroDeCaso}} – {{$mail->titulo}}- ha
                    sido cerrado dado su rechazo de presupuesto.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su ticket número: {{$mail->numeroDeCaso}}
                    – {{$mail->titulo}} – ha sido cerrado automáticamente por el
                    sistema debido a que usted rechazó la cotización ofrecida
                    por nuestro departamento comercial. <br /><br />
                    Si esto es un error, agradecemos nos envié una nueva
                    solicitud explicando el problema. <br /><br />
                    Muchas gracias Equipo de Soporte. <br /><br />
                </p>

                @elseif($mail->tipo == 22)
                <p style="">
                    Su ticket sobre el caso - {{$mail->numeroDeCaso}} - ha sido
                    creado satisfactoriamente.
                </p>
                <p align="" style="">
                    Estimado Cliente, Su solicitud ha sido recibida y está
                    siendo procesada en nuestro sistema de gestión de incidentes
                    técnicos. <br />
                    Podrá hacer seguimiento de su caso con el número de ticket:
                    {{$mail->numeroDeCaso}} <br />
                    Dado que usted actualmente cuenta con soporte técnico por
                    parte de nuestra empresa, su caso ha sido remitido a nuestro
                    equipo para brindarle una solución. <br />
                    De ser necesario, en breve un agente de soporte se
                    contactará con usted. <br /><br />
                    Por favor tome en consideración lo siguiente: <br />
                    Es importante que antes de solicitar soporte técnico el
                    cliente verifique la documentación del producto otorgada por
                    Commandline. <br /><br />
                    Las solicitudes de soporte técnico serán recibidas dentro
                    del horario establecido acorde a la modalidad de servicio
                    contratado. Las solicitudes hechas fuera de este horario
                    serán tratadas el próximo día hábil. <br /><br />
                    En el caso de que su solicitud no esté en el marco de lo
                    establecido en su contrato, usted será contactado por uno de
                    nuestros agentes del departamento comercial. <br />
                    El tiempo de respuesta a una solicitud podría diferir del
                    tiempo de resolución de la misma. El período de respuesta a
                    una solicitud se contará desde el registro inicial del caso
                    hasta la primera interacción registrada del técnico con el
                    cliente. Posteriora esto, el caso será remitido al técnico o
                    equipo de trabajo correspondiente. <br /><br />
                    Muchas gracias por contactarnos Equipo de Soporte.
                    <br /><br />
                </p>

                @elseif($mail->tipo == 30)
                <p style="">
                    Su ticket sobre el caso - {{$mail->numeroDeCaso}} - ha sido
                    Reabierto.
                    Le escribe nuestro soporte             </p>
                <p align="" style="">
                    Estimado Cliente, Su solicitud ha sido Re abierta y está
                    siendo procesada en nuestro sistema de gestión de incidentes
                    técnicos. <br />
                    Podrá hacer seguimiento de su caso con el número de ticket:
                    {{$mail->numeroDeCaso}} <br />
                    Por favor tome en consideración lo siguiente: <br />
                    Es importante que antes de solicitar soporte técnico el
                    cliente verifique la documentación del producto otorgada por
                    Commandline. <br /><br />
                    Las solicitudes de soporte técnico serán recibidas dentro
                    del horario establecido acorde a la modalidad de servicio
                    contratado. Las solicitudes hechas fuera de este horario
                    serán tratadas el próximo día hábil. <br /><br />
                    En el caso de que su solicitud no esté en el marco de lo
                    establecido en su contrato, usted será contactado por uno de
                    nuestros agentes del departamento comercial. <br />
                    El tiempo de respuesta a una solicitud podría diferir del
                    tiempo de resolución de la misma. El período de respuesta a
                    una solicitud se contará desde el registro inicial del caso
                    hasta la primera interacción registrada del técnico con el
                    cliente. Posteriora esto, el caso será remitido al técnico o
                    equipo de trabajo correspondiente. <br /><br />
                    Muchas gracias por contactarnos Equipo de Soporte.
                    <br /><br />
                </p>
                @endif

                <div style="font-size:12px; color:gray;" >

                    <p>Por favor tome en consideración lo siguiente:</p><br>
                    <ul>
                        <li>Es importante que antes de solicitar soporte técnico el cliente verifique la documentación del producto otorgada por Commandline.</li>
                        <li>Las solicitudes de soporte técnico realizadas por fuera del horario de atención de la empresa serán tratadas el próximo día hábil.</li>
                    </ul>
                    <br>

                    <p> Commandline | Zabala 1327 Of. 206 | Montevideo – Uruguay Telefax: (+598) 2916 5354 <a href="https://files.voip-la.com/s/LsZQkYERqaawQZ2"> [Términos y condiciones]</a></p>
                </div>

            </div>
        </div>
    </body>
</html>
