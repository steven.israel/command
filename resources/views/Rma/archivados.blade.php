<!DOCTYPE html>
<html lang="en">
<head>
  <title>Commandline</title>

  <!-- BEGIN META -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" content="your,keywords">
  <meta name="description" content="Short explanation about this website">
  <!-- END META -->
  @include('../Layout/links')

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

</head>
<body class="menubar-hoverable header-fixed menubar-pin ">

  @include('../Layout/navbar');

  <!-- start informe -->

  <div id="base">

    <!-- BEGIN OFFCANVAS LEFT -->
    <div class="offcanvas">
    </div><!--end .offcanvas-->
    <!-- END OFFCANVAS LEFT -->

    <div id="content">
      <section>
        @if(Session::has('success'))
            <div class="alert alert-info">
              {{Session::get('success')}}
            </div>
          @endif
        <div class="section-header">
          <ol class="breadcrumb">
            <li class="active">RMA</li>
          </ol>
        </div>
        <div class="section-body">
          <div class="row">
            <div class="col-lg-12">
              <div class="card card-printable style-default-light">
                <div class="card-head">
                  <div class="tools">
                    <div class="btn-group">
                      <!-- <a class="btn btn-floating-action btn-primary" href="javascript:void(0);" onclick="javascript:window.print();"><i class="md md-print"></i></a> -->
                      <!-- <a class="btn btn-floating-action btn-primary" href="{{ url('dashboardSeguridad') }}" ><i class="md md-arrow-back"></i></a> -->
                    </div>
                    <div class="btn-group">

                    </div>
                  </div>
                </div><!--end .card-head -->
                <div class="card-body style-default-bright">
                  <!-- END INVOICE DESCRIPTION -->
                  <!-- BEGIN INVOICE PRODUCTS -->
                  <div class="row">
                    <div class="col-md-12">
                      <div class="table-responsive">
                      <table class="display" id="example" style="width: 100%;">
                         <thead>
                           <th>Codigo</th>
                           <th>fecha</th>
                           <th>Cliente</th>
                           <th>Descripcion</th>
                           <th>Marca</th>
                           <th>Modelo</th>
                           <th>Estado</th>
                           <th>Tipo</th>
                           <th>Visto por ultima vez</th>
                           <th></th>
                           <th></th>
                           <th></th>
                         </thead>
                         <tbody>
                          @if($rmas->count())
                          @foreach($rmas as $rm)
                          <tr>
                            <td>
                              @if($rm->tipo_rma == 1)
                                {{$rm->codigo_rma}}-E
                              @else
                                {{$rm->codigo_rma}}-S
                              @endif
                            </td>
                            <td>{{ date('Y-m-d h:i a',strtotime($rm->created_at)) }}</td>
                            <td>{{ $rm->cliente->nombres}} {{$rm->cliente->apellidos}}</td>
                            <td>
                              {{ (strlen($rm->descripcion) >= 20) ? substr($rm->descripcion, 0, 20).'...' : $rm->modelo}}
                            </td>
                            <td>{{ $rm->marcas->nombre}}</td>
                            <td>
                              {{ (strlen($rm->modelo) >= 20) ? substr($rm->modelo, 0, 20).'...' : $rm->modelo}}
                            </td>
                            <td>
                              @if($rm->estado_rma == 1)
                                En Proceso
                              @elseif($rm->estado_rma == 2)
                                Reparó con éxito
                              @elseif($rm->estado_rma == 3)
                                No Reparó
                              @elseif($rm->estado_rma == 4)
                                Terciario
                                @elseif($rm->estado_rma == 5)
                                Archivado
                              @endif
                            </td>
                            <td>
                               @if($rm->tipo_rma == 1)
                                Entrada
                              @else
                                Salida
                              @endif
                            </td>
                            <td>
                              {{ $rm->updated_at }}
                            </td>
                            <td>
                              <a class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Editar Rma" href="{{action('RmaController@edit', $rm->codigo_rma)}}" >
                                <span class="glyphicon glyphicon-pencil"></span>
                              </a>
                            </td>
                            <td>
                              <form action="{{action('RmaController@destroy', $rm->codigo_rma)}}" method="post">
                               {{csrf_field()}}
                               <input name="_method" type="hidden" value="DELETE">

                               <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Archivar Rma" type="submit"><span class="glyphicon glyphicon-folder-close"></span></button>
                             </form>
                             </td>
                             <td >
                              <a class="btn btn-primary btn-xs" href="{{action('RmaController@show', $rm->codigo_rma)}}" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir Rma">
                                <span class="md md-print"></span>
                              </a>
                             </td>
                           </tr>
                           @endforeach
                           @else
                           <tr>
                            <td colspan="8">No hay registro !!</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                    </div>
                    </div><!--end .col -->
                  </div><!--end .row -->
                <!-- END INVOICE PRODUCTS -->

              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->
        </div><!--end .row -->
      </div><!--end .section-body -->
    </section>
  </div><!--end #content-->



  <!-- BEGIN OFFCANVAS RIGHT -->
  <div class="offcanvas">


  </div><!--end .offcanvas-->
  <!-- END OFFCANVAS RIGHT -->

</div>

<!-- end informe -->


@include('../Layout/menu')

<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="{{ asset('js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{ asset('js/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/libs/spin.js/spin.min.js')}}"></script>
<script src="{{ asset('js/libs/autosize/jquery.autosize.min.js')}}"></script>
<script src="{{ asset('js/libs/moment/moment.min.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.min.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.time.min.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.orderBars.js')}}"></script>
<script src="{{ asset('js/libs/flot/jquery.flot.pie.js')}}"></script>
<script src="{{ asset('js/libs/flot/curvedLines.js')}}"></script>
<script src="{{ asset('js/libs/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{ asset('js/libs/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{ asset('js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
<script src="{{ asset('js/libs/d3/d3.min.js')}}"></script>
<script src="{{ asset('js/libs/d3/d3.v3.js')}}"></script>
<script src="{{ asset('js/libs/rickshaw/rickshaw.min.js')}}"></script>
<script src="{{ asset('js/core/source/App.js')}}"></script>
<script src="{{ asset('js/core/source/AppNavigation.js')}}"></script>
<script src="{{ asset('js/core/source/AppOffcanvas.js')}}"></script>
<script src="{{ asset('js/core/source/AppCard.js')}}"></script>
<script src="{{ asset('js/core/source/AppForm.js')}}"></script>
<script src="{{ asset('js/core/source/AppNavSearch.js')}}"></script>
<script src="{{ asset('js/core/source/AppVendor.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
      dom: 'Bfrtip',
      buttons: [
      'pdf', 'excel'
      ],
      "language": {
        "search": "Buscar:",
        paginate: {
          next: '&#8594;', // or '→'
          previous: '&#8592;' // or '←'
        }
      },
      "order": [[ 0, "desc" ]]
    } );
  } );

</script>

</body>
</html>
