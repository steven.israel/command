@extends('Layout.layout')
@section('content')

<div id="base">

    <!-- BEGIN OFFCANVAS LEFT -->
    <div class="offcanvas">
    </div>
    <!--end .offcanvas-->
    <!-- END OFFCANVAS LEFT -->

    <!-- BEGIN CONTENT-->
    <div id="content">
        <section>

            <div class="section-body contain-lg">
                <div class="section-header">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('rma.index') }}">Lista de RMA</a></li>
                        <li><a href="/rma/{{$rma->codigo_rma}}/edit">Regresar</a></li>
                        <li class="active">Modificar RMA</li>
                    </ol>
                </div>
                <div class="card">
                    <!-- BEGIN CONTACT DETAILS -->
                    <div class="card-tiles">
                        <div class="hbox-md col-md-12">
                            <div class="hbox-column col-md-12">
                                <div class="row">

                                    <!-- BEGIN CONTACTS MAIN CONTENT -->
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="col-xs-12">
                                            <div class="col-md-8">
                                                <img src="/images/logo.jpg" alt="">
                                            </div>
                                            <div class="col-md-4"><br>
                                                RUT: 170181190017
                                            </div>
                                        </div>
                                        <br><br>
                                        <ul class="nav nav-tabs">
                                            <li class=""><a href="/rma/{{$rma->codigo_rma}}/edit">@if($rma->tipo_rma ==
                                                    1) Entrada @else Salida @endif</a></li>
                                            <li class="active"><a href="#">Historial</a></li>
                                            <li class="active" style="float: right;"><a>ID: {{ $rma->codigo_rma }}</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- BEGIN CONTACTS NOTES -->
                                            <div class="tab-pane active" id="notes">
                                                <br>
                                                @if($rma->historial)
                                                <div class="row">
                                                    @foreach($rma->historial as $historial)
                                                    <div class="col-md-12">
                                                        <div class="row card">
                                                            <div class="col-md-12 d-flex">
                                                                <div class="col-md-6 justify-content-left">
                                                                    <small><strong>Estado Anterior: </strong>
                                                                        @if($historial->id_estado_anterior == 1)
                                                                        En Proceso
                                                                        @elseif($historial->id_estado_anterior == 2)
                                                                        Reparó con éxito
                                                                        @elseif($historial->id_estado_anterior == 3)
                                                                        No Reparó
                                                                        @else
                                                                        Terciario
                                                                        @endif
                                                                    </small>
                                                                </div>
                                                                <div class="col-md-6 justify-content-right">
                                                                    <small><strong>Estado Actual: </strong>
                                                                        @if($historial->id_estado_actual == 1)
                                                                        En Proceso
                                                                        @elseif($historial->id_estado_actual == 2)
                                                                        Reparó con éxito
                                                                        @elseif($historial->id_estado_actual == 3)
                                                                        No Reparó
                                                                        @else
                                                                        Terciario
                                                                        @endif
                                                                    </small>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <h3>{{ $historial->comentario }}</h3>
                                                            </div>
                                                            <div class="col-md-12 d-flex">
                                                                <div class="col-md-6 justify-content-left">
                                                                    <small><strong>Usuario:
                                                                        </strong>{{ $historial->user->nombres . ' ' . $historial->user->apellidos }}</small>
                                                                </div>
                                                                <div class="col-md-6 justify-content-right">
                                                                    <small><strong>Fecha y Hora:
                                                                        </strong>{{ $historial->created_at }}</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <!--end .tab-content -->
                                    </div>
                                    <!--end .col -->
                                    <!-- END CONTACTS MAIN CONTENT -->

                                </div>
                                <!--end .row -->
                            </div>
                        </div>
                        <!--end .hbox-md -->
                    </div>
                    <!--end .card-tiles -->
                    <!-- END CONTACT DETAILS -->

                </div>
                <!--end .card -->
            </div>
            <!--end .section-body -->
        </section>
    </div>

</div>

<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
@endsection
