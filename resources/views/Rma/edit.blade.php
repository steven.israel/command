@extends('Layout.layout')
@section('content')

<div id="base">

    <!-- BEGIN OFFCANVAS LEFT -->
    <div class="offcanvas">
    </div>
    <!--end .offcanvas-->
    <!-- END OFFCANVAS LEFT -->

    <!-- BEGIN CONTENT-->
    <div id="content">
        <section>

            <div class="section-body contain-lg">
                <div class="section-header">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('rma.index') }}">Lista de RMA</a></li>
                        <li class="active">Modificar RMA</li>
                    </ol>
                </div>
                <div class="card">
                    <!-- BEGIN CONTACT DETAILS -->
                    <div class="card-tiles">
                        <div class="hbox-md col-md-12">
                            <div class="hbox-column col-md-12">
                                <div class="row">

                                    <!-- BEGIN CONTACTS MAIN CONTENT -->
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="col-xs-12">
                                            <div class="col-md-8">
                                                <img src="/images/logo.jpg" alt="">
                                            </div>
                                            <div class="col-md-4"><br>
                                                RUT: 170181190017
                                            </div>
                                        </div>
                                        <br><br>
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#notes">@if($rma->tipo_rma == 1) Entrada @else
                                                    Salida @endif</a></li>
                                            <li>
                                                <a href="/rma/historial/{{$rma->codigo_rma}}">Historial</a>
                                            </li>
                                            <li class="active" style="float: right;"><a>ID: {{ $rma->codigo_rma }}</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- BEGIN CONTACTS NOTES -->
                                            <div class="tab-pane active" id="notes">
                                                <br>
                                                <form method="POST" action="{{ route('rma.update',$rma->codigo_rma) }}"
                                                    role="form" id="accion_form">
                                                    {{ csrf_field() }}
                                                    <input name="_method" type="hidden" value="PATCH">
                                                    <div class="">
                                                        <div class="">
                                                            <div class="form-group">
                                                                <label for="fecha">Fecha</label>
                                                                <input type="date"
                                                                    value="<?php echo date("Y-m-d", strtotime($rma->fecha)); ?>"
                                                                    class="form-control" id="fecha" name="fecha"
                                                                    required="" data-rule-minlength="2"
                                                                    aria-required="true">

                                                            </div>
                                                            <input type="hidden" name="tipo" value="{{$rma->tipo_rma}}">
                                                            <div class="row">
                                                                <div class="col-xs-6 col-sm-6 col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="idempresa">Empresas</label>
                                                                        <select id="idempresa" onchange="select()"
                                                                            name="idempresa" class="form-control">
                                                                            @if($empresas->count())
                                                                            @foreach($empresas as $list)
                                                                            <option value="{{ $list->idempresa}}"
                                                                                {{ ($rma->cliente->empresa->idempresa == $list->idempresa) ? 'selected' : '' }}>
                                                                                {{ $list->nombre }}</option>
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                    <p style="color: red;">
                                                                        {{ $errors->first('idempresa') }}</p>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="clientes">Clientes</label>
                                                                        <select id="clientes" name="idcliente"
                                                                            class="form-control">
                                                                        </select>
                                                                        <p style="color: red;">
                                                                            {{ $errors->first('idcliente') }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="falla">Falla</label>
                                                                        <textarea name="falla" id="falla"
                                                                            class="form-control" rows="3" required=""
                                                                            aria-required="true">{{ $rma->descripcion_falla }}</textarea>

                                                                    </div>
                                                                    <p style="color: red;">{{ $errors->first('falla') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                                    <div class="form-group">
                                                                        <textarea name="descripcion" id="descripcion"
                                                                            class="form-control" rows="3" required=""
                                                                            aria-required="true">{{ $rma->descripcion }}</textarea>
                                                                        <label for="descripcion">Descripcion</label>
                                                                    </div>
                                                                    <p style="color: red;">
                                                                        {{ $errors->first('descripcion') }}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="marca">Marca</label>
                                                                        <select id="marca" name="marca"
                                                                            class="form-control">
                                                                            @if($marcas->count())
                                                                            @foreach($marcas as $list)
                                                                            @if($list->idmarca == $rma->marca)
                                                                            <option value="{{ $list->idmarca}}"
                                                                                selected>{{ $list->nombre }}</option>
                                                                            @else
                                                                            <option value="{{ $list->idmarca}}">
                                                                                {{ $list->nombre }}</option>
                                                                            @endif
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                    <p style="color: red;">{{ $errors->first('marca') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-3 col-sm-3 col-md-3">
                                                                    <label>Accesorio</label>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-6 col-md-6">
                                                                    <div style="float: left;">
                                                                        <div class="form-group">
                                                                            <div class="radio radio-styled">
                                                                                <label>
                                                                                    <input type="radio" name="accesorio"
                                                                                        id="accesorio_1" value="1"
                                                                                        required="" aria-required="true"
                                                                                        <?php if ($rma->accesorio == 1): ?>checked
                                                                                        <?php endif ?>>
                                                                                    <span>Si</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div style="float: right;">
                                                                        <div class="form-group">
                                                                            <div class="radio radio-styled">
                                                                                <label>
                                                                                    <input type="radio" name="accesorio"
                                                                                        id="accesorio_2" value="2"
                                                                                        required="" aria-required="true"
                                                                                        <?php if ($rma->accesorio == 2): ?>checked
                                                                                        <?php endif ?>>
                                                                                    <span>No</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="describir_accesorio"
                                                                style="display: {{ ($rma->accesorio == 1) ? 'block' : 'none' }}">
                                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="describir">Describir
                                                                            Accesorio</label>
                                                                        <textarea name="describir" id="describir"
                                                                            class="form-control"
                                                                            rows="3">{{ $rma->describir }}</textarea>
                                                                    </div>
                                                                    <p style="color: red;">
                                                                        {{ $errors->first('describir') }}</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">

                                                                <label for="modelo">Modelo</label>
                                                                <input type="text" class="form-control"
                                                                    value="{{ $rma->modelo }}" id="modelo" name="modelo"
                                                                    required="" data-rule-minlength="2"
                                                                    aria-required="true">
                                                                <p style="color: red;">{{ $errors->first('modelo') }}
                                                                </p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="serie">N. de serie</label>
                                                                <input type="text" class="form-control" id="serie"
                                                                    value="{{ $rma->numero_serie }}" name="serie"
                                                                    required="" data-rule-minlength="2"
                                                                    aria-required="true">
                                                                <p style="color: red;">{{ $errors->first('serie') }}</p>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="comentario">Comentario</label>
                                                                        <textarea name="comentario" id="comentario"
                                                                            class="form-control" rows="3" required=""
                                                                            aria-required="true">{{ $rma->comentario }}</textarea>
                                                                    </div>
                                                                    <p style="color: red;">
                                                                        {{ $errors->first('comentario') }}</p>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="comentario_historial">Comentario
                                                                            Historial</label>
                                                                        <textarea name="comentario_historial"
                                                                            id="comentario_historial"
                                                                            class="form-control" rows="3" required
                                                                            aria-required="true"></textarea>
                                                                    </div>
                                                                    <p style="color: red;">
                                                                        {{ $errors->first('comentario_historial') }}</p>
                                                                </div>
                                                            </div>
                                                            @if($rma->tipo_rma == 2)
                                                            <div class="row">
                                                                <div class="col-xs-3 col-sm-3 col-md-3">
                                                                    <label>Devolucion de mercaderia</label>
                                                                </div>
                                                                <div class="col-xs-9 col-sm-9 col-md-9">
                                                                    <div class="form-group">
                                                                        <select id="devolucion" name="devolucion"
                                                                            class="form-control">
                                                                            <option value="1"
                                                                                <?php if ($rma->tipo_devolucion == 2): ?>selected
                                                                                <?php endif ?>>Cadeteria</option>
                                                                            <option value="2"
                                                                                <?php if ($rma->tipo_devolucion == 2): ?>selected
                                                                                <?php endif ?>>Retiro de cliente
                                                                            </option>
                                                                            <option value="3"
                                                                                <?php if ($rma->tipo_devolucion == 2): ?>selected
                                                                                <?php endif ?>>Enviado por CMD LINE
                                                                                S.R.L.</option>
                                                                        </select>
                                                                    </div>
                                                                    <p style="color: red;">
                                                                        {{ $errors->first('devolucion') }}</p>
                                                                </div>
                                                            </div>
                                                            @endif
                                                            <div class="row" align="center">
                                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                                                        <div class="form-group">
                                                                            <div class="radio radio-styled">
                                                                                <label>
                                                                                    <input type="radio" name="estado"
                                                                                        value="1" required=""
                                                                                        <?php if ($rma->estado_rma == 1): ?>checked
                                                                                        <?php endif ?>
                                                                                        aria-required="true">
                                                                                    <span>En Proceso</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                                                        <div class="form-group">
                                                                            <div class="radio radio-styled">
                                                                                <label>
                                                                                    <input type="radio" name="estado"
                                                                                        value="2" required=""
                                                                                        <?php if ($rma->estado_rma == 2): ?>checked
                                                                                        <?php endif ?>
                                                                                        aria-required="true">
                                                                                    <span>Reparó con éxito</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                                                        <div class="form-group">
                                                                            <div class="radio radio-styled">
                                                                                <label>
                                                                                    <input type="radio" name="estado"
                                                                                        value="3" required=""
                                                                                        <?php if ($rma->estado_rma == 3): ?>checked
                                                                                        <?php endif ?>
                                                                                        aria-required="true">
                                                                                    <span>No Reparó</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                                                        <div class="form-group">
                                                                            <div class="radio radio-styled">
                                                                                <label>
                                                                                    <input type="radio" name="estado"
                                                                                        value="4" required=""
                                                                                        <?php if ($rma->estado_rma == 4): ?>checked
                                                                                        <?php endif ?>
                                                                                        aria-required="true">
                                                                                    <span>Terciario</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" align="center">
                                                                <div class="col-xs-4"></div>
                                                                <div class="col-xs-4">
                                                                    <button type="submit"
                                                                        class="btn btn-block ink-reaction btn-info">Modificar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end .list-results -->
                                                </form>
                                            </div>
                                        </div>
                                        <!--end .tab-content -->
                                    </div>
                                    <!--end .col -->
                                    <!-- END CONTACTS MAIN CONTENT -->

                                </div>
                                <!--end .row -->
                            </div>
                        </div>
                        <!--end .hbox-md -->
                    </div>
                    <!--end .card-tiles -->
                    <!-- END CONTACT DETAILS -->

                </div>
                <!--end .card -->
            </div>
            <!--end .section-body -->
        </section>
    </div>

</div>

<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
      select();
    });

    function select(){
      //console.log("notificacion");
        var id = document.getElementById("idempresa");

        var cliente = <?php echo $rma->idcliente;?>;

        $.ajax({
          type: 'GET',
          url: '{{ url('/') }}/select_clientes/'+ id.value,
          data: $(this).serialize(),
          success: function(data) {
           $('#clientes').empty();
           if (data != ""){
            $('#c_cliente').css({"display":"none"});
            $('#info').css({"display":"none"});
                        //console.log(data);
                        $('#clientes').append('<option value="0">Seleccionar Cliente</option>');
                        data.forEach(function(element){
                          if (element['idcliente'] == cliente) {
                            $('#clientes').append('<option value="'+ element['idcliente'] +'" selected>'+ element['nombres'] +' '+element['apellidos'] +'</option>');
                          }else{
                            $('#clientes').append('<option value="'+ element['idcliente'] +'">'+ element['nombres'] +' '+element['apellidos'] +'</option>');
                          }

                        });
                      }else{
                       $('#clientes').append('<option value="0">Sin Resultados</option>');
                       $('#c_cliente').css({"display":"inline-block"});
                       $('#info').css({"display":"none"});
                     }
                   }
                 });
      }
      $('#accesorio_1').on('click',function (){
        $('#describir_accesorio').css('display','block');
      });

      $('#accesorio_2').on('click',function (){
        $('#describir_accesorio').css('display','none');
      });
</script>

@endsection
