@extends('Layout.layout')
@section('content')

<!-- BEGIN BASE-->
<div id="base">

    <!-- BEGIN OFFCANVAS LEFT -->
    <div class="offcanvas">
    </div>
    <!--end .offcanvas-->
    <!-- END OFFCANVAS LEFT -->

    <!-- BEGIN CONTENT-->
    <div id="content">
        <section>
            <div class="section-body">
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <h2 style="font-size: 50px">Sistema de Atención al Cliente</h2>
                    </div><br><br>
                </div><br><br><br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form" action="{{url('/dashboard')}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-daterange input-group" id="demo-date-range">
                                                    <div class="input-group-content">
                                                        <input type="date" class="form-control"
                                                            value="<?=date('Y-m-d')?>" name="start">
                                                        <label>Fecha</label>
                                                    </div>
                                                    <span class="input-group-addon">Hasta</span>
                                                    <div class="input-group-content">
                                                        <input type="date" class="form-control"
                                                            value="<?=date('Y-m-d')?>" name="end">
                                                        <div class="form-control-line"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select id="estado" name="estado" class="form-control">
                                                    <option value="">Todos</option>
                                                    @if($estados)
                                                    @foreach($estados as $list)
                                                    <option value="{{$list->idEstadoTicket}}">{{$list->nombre}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                <label for="estado">Seleccionar estado</label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit"
                                                class="btn btn-block ink-reaction btn-flat btn-info">Consultar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--end .card-body -->
                        </div>
                    </div>

                    @if($tickets->count())
                    @foreach($tickets as $list)
                    <!-- BEGIN ALERT - REVENUE -->
                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-body no-padding">
                                <div class="alert alert-callout alert-info no-margin">
                                    <strong class="pull-right text-success text-lg">{{$list->total}}</strong>

                                    <strong class="text-lg">{{$list->nombre}} </strong><br>
                                    <span class="opacity-50"></span>

                                </div>
                            </div>
                            <!--end .card-body -->
                        </div>
                        <!--end .card -->
                    </div>
                    <!--end .col -->
                    <!-- END ALERT - REVENUE -->
                    @endforeach
                    @endif

                    @if(!empty($chart_area))
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="titulo_2" role="alert">
                                    <strong style="color:#212121; font-size: 15px ;">
                                        Grafica de Tickets Abiertos Vrs Ticket Cerrados
                                    </strong>
                                </div>
                                <div id="chart_area" data-colors="#4DB6AC,#FFC107,#c62828,#CFD8DC"></div>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="col-sm-12">
                        <div id="poll_div"></div>
                        <?=$lava->render('BarChart', 'total', 'poll_div') ?>
                    </div>

                </div>
            </div>
            <!--end .section-body -->
        </section>
    </div>
    <!--end #content-->
    <!-- END CONTENT -->

    <!-- BEGIN OFFCANVAS RIGHT -->
    <div class="offcanvas">

    </div>
    <!--end .offcanvas-->
    <!-- END OFFCANVAS RIGHT -->

</div>
<!--end #base-->
<!-- END BASE -->

<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>

@endsection
