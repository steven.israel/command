@extends('Layout.layout')
@section('content')
<div id="base">
	<div class="row">
		<section class="content" id="content">
			<div class="col-md-8 col-md-offset-2">
				<div class="section-header">
		            <ol class="breadcrumb">
		               <li><a href="{{ route('usuario.index') }}">Lista de Usuarios</a></li>
		              <li class="active">Actualizar Usuario</li>
		            </ol>
	          	</div>
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Error!</strong> Revise los campos obligatorios.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				@if(Session::has('success'))
				<div class="alert alert-info">
					{{Session::get('success')}}
				</div>
				@endif

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Actualizar Usuario</h3>
					</div>
					<div class="panel-body">
						<div class="table-container">
							<form method="POST" action="{{ route('usuario.update',$usuario->id) }}" onsubmit="update(event,this)" role="form" id="accion_form">
								{{ csrf_field() }}
								<input name="_method" type="hidden" value="PATCH">
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Usuario</label>
											<input type="text" name="username" id="username" class="form-control input-sm" value="{{$usuario->username}}" placeholder="Usuario">
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
										 	<label>Nombres</label>
											<input type="text" name="nombres" value="{{$usuario->nombres}}" id="nombres" class="form-control input-sm" placeholder="Nombres">
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Apellidos</label>
											<input type="hidden" name="avatar" id="avatar" class="form-control input-sm" placeholder="" value="fav.png">
											<input type="text" name="apellidos" value="{{$usuario->apellidos}}" id="apellidos" class="form-control input-sm" placeholder="Apellidos">
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Correo Electrónico</label>
											<input type="email" name="email" id="email" value="{{$usuario->email}}" class="form-control input-sm" placeholder="Correo">
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Nueva Contraseña</label>
											<input type="password" name="password" id="password" class="form-control input-sm" placeholder="Nueva Contraseña">
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Confirmar contraseña</label>
											<input type="password" name="passwordC" id="passwordC" class="form-control input-sm name" placeholder="Confirmar contraseña">
											<p ><span class="emsg hidden text-danger " style="
												color: red;">Las contraseñas no coinciden</span></p>
										</div>
									</div>
								</div>
								<div class="row">
									@if(auth()->user()->id != $usuario->id)
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label for="role">Roles</label>
											<select id="role" name="role" class="form-control">
												 <option value="1" {{ ($usuario->role == 1) ? 'selected' : '' }}>Administrador</option>
												 <option value="2" {{ ($usuario->role == 2) ? 'selected' : '' }}>Moderador</option>
												 <option value="3" {{ ($usuario->role == 3) ? 'selected' : '' }}>Técnico 1 (junior)</option>
												 <option value="4" {{ ($usuario->role == 4) ? 'selected' : '' }}>Técnico 2 (senior)</option>
												 <option value="5" {{ ($usuario->role == 5) ? 'selected' : '' }}>Dpto. Ejecutivo/ Comercial</option>
											</select>
										</div>
									</div>
									@endif
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label for="status">Estado</label>
											<select id="status" name="status" class="form-control">
												 <option value="1" {{ ($usuario->status == 1) ? 'selected' : '' }}>Activo</option>
												 <option value="2" {{ ($usuario->status == 2) ? 'selected' : '' }}>Inactivo</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">

									<div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
										<input type="submit"  value="Guardar" class="btn btn-success btn-block">
									</div>

								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
		</section>
	</div>
	@endsection

@section('script')

<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  function update(e,form){
    e.preventDefault();
    swal({
      title: "Confirmar Actualización de registro?",
      icon: "warning",
      buttons: ["Confirmar", "Cancelar"],
      dangerMode: true,
    }).then((willDelete) => {
      if (!willDelete) {
        form.submit();
      } else {
        return false;
      }
    });
  }

  bandera = true;
  contraseña = "";
$("#password").on("keypress keydown keyup", function () {
  console.log($(this).val());
  contraseña = $(this).val();

  if ($("#passwordC").val() != "") {
    if ($("#passwordC").val() != contraseña) {
      // there is a mismatch, hence show the error message
      $(".emsg").removeClass("hidden");
      $(".emsg").show();
	  bandera = false;
    } else {
      $(".emsg").addClass("hidden");
	  bandera = true;
    }
  }
});

$("#passwordC").on("keypress keydown keyup", function () {
  if ($(this).val() != contraseña) {
    // there is a mismatch, hence show the error message
    $(".emsg").removeClass("hidden");
    $(".emsg").show();
	bandera = false;
  } else {
    $(".emsg").addClass("hidden");
	bandera = true;
  }
});


$("form").submit(function (e) {
	


    if ($("#passwordC").val() != $("#password").val() ) {
      e.preventDefault(e);
      swal({
        title:
          "Las contraseñas no coinciden, por favor vuelva a revisarlas",
        icon: "warning",
        buttons: ["Cerrar"],
        dangerMode: true,
      });
    }
  
});



  


</script>
@endsection
