@extends('Layout.layout')
@section('content')
<!-- BEGIN BASE-->
<div id="base">
<div class="row">
  <section class="content" id="content">
  @if(Session::has('success'))
        <div class="alert alert-info">
          {{Session::get('success')}}
        </div>
      @endif
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
              <div class="pull-left"><h3>Lista de Usuarios del Sistema</h3>

              </div>
              <div class="pull-right">
                <div class="btn-group">
                  <a href="{{ route('usuario.create') }}" class="btn btn-info" >Crear Usuario del Sistema</a>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="table-container">
                <div class="table-responsive">
                  <table id="example" class="display nowrap" style="width: 100%;">
                  <thead>
                      <th>Usuario</th>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Correo</th>
                      <th>Rol</th>
                      <th>Estado</th>
                      <th>Acciones</th>
                  </thead>
                  <tbody>
                    @if($usuarios->count())
                    @foreach($usuarios as $usuario)
                    <tr>
                      <td id="nombre-{{ $usuario->id }}">{{$usuario->username}}</td>
                      <td>{{$usuario->nombres}}</td>
                      <td>{{$usuario->apellidos}}</td>
                      <td>{{$usuario->email}}</td>
                      <td>
                        @if($usuario->role == 1)
                          Administrador
                        @elseif($usuario->role == 2)
                          Moderador
                        @elseif($usuario->role == 3)
                          Tecnico 1 (junior)
                        @elseif($usuario->role == 4)
                          Tecnico 2 (senior)
                        @else
                          Dpto. Ejecutivo/ Comercial
                        @endif
                      </td>
                      <td>@if($usuario->status == 1) activo @else Inactivo @endif</td>
                      <td>
                        <div class="row">
                          <div class="col-md-4 col-xs-4">
                            <a class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Editar Usuario" href="{{action('UsuarioController@edit', $usuario->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a>
                          </div>
                          <div class="col-md-4 col-xs-4">
                            @if($usuario->status == 1)
                              <a onclick="act_inac({{$usuario->id}},0)" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Desactivar Usuario">
                                <span class="md md-block"></span>
                              </a>
                            @else
                              <a onclick="act_inac({{$usuario->id}},1)" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Activar Usuario">
                                <span class="md md-check"></span>
                              </a>
                            @endif
                          </div>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                      <td colspan="8">No hay registro !!</td>
                    </tr>
                    @endif
                    </tbody>

                  </table>
              </div>
            </div>
          </div>
      </div>
      </div>

    </div>
  </div>
</section>
</div>

@endsection

@section('script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script>

  
  $(document).ready(function() {
    $('#example').DataTable( {
      dom: 'Bfrtip',
      "paging": true,
      "ordering": false,
      "info":     true,
      buttons: [],
      "language": {
        "search": "Buscar:",
        paginate: {
          next: '&#8594;', // or '→'
          previous: '&#8592;' // or '←'
        }
      },
      "order": [[ 0, "desc" ]]
    } );
  });

  function borrar(e,form){
    e.preventDefault();
    swal({
      title: "Confirmar eliminación de registro?",
      icon: "warning",
      buttons: ["Confirmar", "Cancelar"],
      dangerMode: true,
    }).then((willDelete) => {
      if (!willDelete) {
        form.submit();
      } else {
        return false;
      }
    });
  }

  function act_inac(id,value){
    var title = 'Activar';
    var url   = 'activar';
    if (!value) {
      title = 'Desactivar';
      url   = 'desactivar';
    }
    swal({
      title: 'Está usted seguro de '+title+' al usuario "'+ $('#nombre-'+id).text() +'"?',
      icon: "warning",
      buttons: ["Confirmar", "Cancelar"],
      dangerMode: true,
    }).then((willDelete) => {
      if (!willDelete) {
        window.location.href = "/usuario/"+url+'/'+id;
      } else {
        return false;
      }
    });
  }
</script>
@endsection
