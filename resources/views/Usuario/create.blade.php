@extends('Layout.layout')
@section('content')
<div id="base">
	<div class="row">
		<section class="content" id="content">
			<div class="col-md-8 col-md-offset-2">
			<div class="section-header">
	            <ol class="breadcrumb">
	               <li><a href="{{ route('usuario.index') }}">Lista de Usuarios</a></li>
	              <li class="active">Nuevo Usuario</li>
	            </ol>
          	</div>
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Error!</strong> Revise los campos obligatorios.<br>
				</div>
				@endif
				@if(Session::has('success'))
				<div class="alert alert-info">
					{{Session::get('success')}}
				</div>
				@endif

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Nuevo Usuario Del Sistema</h3>
					</div>
					<div class="panel-body">
						<div class="table-container">
							<form method="POST" action="{{ route('usuario.store') }}"  role="form" id="accion_form">
								{{ csrf_field() }}
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Usuario</label>
											<input type="text" name="username" id="username" value="{{ old('username') }}" class="form-control input-sm">
											<p style="color: red;" id="text-username">
												{{ ($errors->first('username')) ? $errors->first('username') : 'Campo requerido' }}
											</p>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Nombres</label>
											<input type="text" name="nombres" id="nombres" value="{{ old('nombres') }}" class="form-control input-sm" readonly>
											<p style="color: red;"  id="text-nombres">
											{{ ($errors->first('nombres')) ? $errors->first('nombres') : 'Campo requerido' }}
											</p>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Apellidos</label>
											<input type="hidden" name="avatar" id="avatar" class="form-control input-sm" placeholder="" value="fav.png">
											<input type="text" name="apellidos" id="apellidos" value="{{ old('apellidos') }}" class="form-control input-sm" readonly>
											<p style="color: red;"  id="text-apellidos">
											{{ ($errors->first('apellidos')) ? $errors->first('apellidos') : 'Campo requerido' }}
											</p>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Correo Electrónico</label>
											<input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control input-sm" readonly>
											<p style="color: red;" id="text-email">
											{{ ($errors->first('email')) ? $errors->first('email') : 'Campo requerido' }}
											</p>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Contraseña</label>
											<input type="password" name="password" id="password" class="form-control input-sm" readonly>
											<p style="color: red;"  id="text-password">
												{{ ($errors->first('password')) ? $errors->first('password') : 'Campo requerido' }}
											</p>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Confirmar Contraseña</label>
											<input type="password" name="c-password" id="c-password" class="form-control input-sm" readonly>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="form-group">
											<label for="role">Rol</label>
											<select id="role" name="role" class="form-control" disabled>
												<option value="0">Seleccionar Rol</option>
												<option value="1">Administrador</option>
												<option value="2">Moderador</option>
												<option value="3">Técnico 1 (junior)</option>
												<option value="4">Técnico 2 (senior) </option>
												<option value="5">Dpto. Ejecutivo/ Comercial</option>
											</select>
											<p style="color: red;"  id="text-role">
												{{ ($errors->first('role')) ? 'Debe seleccionar un rol' : '' }}
											</p>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label for="status">Estado</label>
											<select id="status" name="status" class="form-control" disabled>
												<option value="0">Seleccionar Estado</option>
												<option value="1">Activo</option>
												<option value="2">Inactivo</option>
											</select>
											<p style="color: red;" id="text-status">
												{{ ($errors->first('status')) ? 'Debe seleccionar el estado del usuario' : '' }}
											</p>
										</div>
									</div>
								</div>
								<div class="row">

									<div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
										<input type="submit"  value="Guardar Usuario del Sistema" class="btn btn-success btn-block">
									</div>

								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
		</section>
	</div>
	@endsection

@section('script')
<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script>

$( document ).ready(function() {
    if ($("#username").val() != "") {
		$('#text-username').text('');
		$("#nombres").prop('readonly', false);
	}
	if ($("#nombres").val() != "") {
		$('#text-nombres').text('');
		$("#apellidos").prop('readonly', false);
	}
	if ($("#apellidos").val() != "") {
		$('#text-apellidos').text('');
		$("#email").prop('readonly', false);
	}
	if ($("#email").val() != "") {
		$('#text-email').text('');
		$("#password").prop('readonly', false);
	}
	if ($("#password").val() != "") {
		$('#text-password').text('');
		$("#c-password").prop('readonly', false);
	}
	if ($("#c-password").val() != "") {
		$("#role").prop('disabled', false);
	}
	if ($("#role").val() != "") {
		$("#status").prop('disabled', false);
	}
});

$("#username").on("input", function(){
	if ($(this).val() == "") {
		$('#text-username').text('Campo requerido')
		$("#nombres").prop('readonly', true);
	}else{
		$('#text-username').text('');
		$("#nombres").prop('readonly', false);
		$.ajax({
			type: 'GET',
			url: '/usuario/validateUsername/'+$('#username').val(),
			data: $(this).serialize(),
			success: function(data) {
				if (data == 1){
					$('#text-username').text('El usuario ya existe');
					$("#nombres").prop('readonly', true);
				}else{
					$("#nombres").prop('readonly', false);
				}
			}
		});
	}
});

$("#nombres").on("input", function(){
	if ($(this).val() == "") {
		$('#text-nombres').text('Campo requerido')
		$("#apellidos").prop('readonly', true);
	}else{
		$('#text-nombres').text('');
		$("#apellidos").prop('readonly', false);
	}
});

$("#apellidos").on("input", function(){
	if ($(this).val() == "") {
		$('#text-apellidos').text('Campo requerido')
		$("#email").prop('readonly', true);
	}else{
		$('#text-apellidos').text('');
		$("#email").prop('readonly', false);
	}
});

$("#email").on("input", function(){
	if ($(this).val() == "") {
		$('#text-email').text('Campo requerido')
		$("#password").prop('readonly', true);
	}else{
		$('#text-email').text('');
		$.ajax({
			type: 'GET',
			url: '/usuario/validateCorreo/'+$('#email').val(),
			data: $(this).serialize(),
			success: function(data) {
				if (data == 1){
					$('#text-email').text('El correo ya existe');
					$("#password").prop('readonly', true);
				}else{
					$("#password").prop('readonly', false);
				}
			}
		});
	}
});

$("#password").on("input", function(){
	if ($(this).val() == "") {
		$('#text-password').text('Campo requerido')
		$("#c-password").prop('readonly', true);
	}else{
		$('#text-password').text('');
		$("#c-password").prop('readonly', false);
	}
});

$("#c-password").on("input", function(){
	if ($(this).val() == "") {
		$("#role").prop('disabled', true);
	}else{
		$("#role").prop('disabled', false);
	}
});

$("#role").on("change", function(){
	if ($(this).val() == "") {
		$("#status").prop('disabled', true);
	}else{
		$("#status").prop('disabled', false);
	}
});


</script>
@endsection
