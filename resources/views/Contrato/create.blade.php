@extends('Layout.layout')
@section('content')
<div id="base">
    <div class="row">
        <section class="content" id="content">
            <div class="col-md-8 col-md-offset-2">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Error!</strong> Revise los campos obligatorios.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    {{Session::get('success')}}
                </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Nuevo Contrato</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-container">
                            <form method="POST" action="{{ route('contrato.store') }}" role="form" id="accion_form">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="date" name="fechaInicio" min="<?=date('Y-m-d')?>"
                                                id="fechaInicio" data-date="" data-date-format="YYYY-MMMM-DD"
                                                value="<?=date('Y-m-d')?>" class="form-control input-sm"
                                                placeholder="Nombres">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="date" name="fechaFin" min="<?=date('Y-m-d')?>" id="fechaFinal"
                                                data-date="" data-date-format="YYYY-MMMM-DD" value="<?=date('Y-m-d')?>"
                                                class="form-control input-sm" placeholder="Apellidos">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">

                                            <label for="TipoContrato_idtipoContrato">Tipo de contrato</label>
                                            <select id="TipoContrato_idtipoContrato" onchange="select()"
                                                name="TipoContrato_idtipoContrato" class="form-control">
                                                @if($tipoContratos->count())
                                                @foreach($tipoContratos as $list)
                                                <option value="{{ $list->idtipoContrato}}">{{ $list->nombre }}</option>
                                                @endforeach
                                                @endif
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">

                                            <label for="Cliente_idcliente">Clientes</label>
                                            <select id="Cliente_idcliente" onchange="select()" name="Cliente_idcliente"
                                                class="form-control">
                                                @if($clientes->count())
                                                @foreach($clientes as $list)
                                                <option value="{{ $list->idcliente}}">{{ $list->nombres }}
                                                    {{$list->apellidos }}</option>
                                                @endforeach
                                                @endif
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <input type="submit" value="Guardar" class="btn btn-success btn-block">
                                        <a href="{{ route('cliente.index') }}" class="btn btn-info btn-block">Atrás</a>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
    @endsection
