@extends('Layout.layout')
@section('content')
<!-- BEGIN BASE-->
<div id="base">
    <div class="row">
        <section class="content" id="content">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-left">
                            <h3>Lista de contrato</h3>
                        </div>
                        <div class="pull-right">
                            <div class="btn-group">
                                <a href="{{ route('contrato.create') }}" class="btn btn-info">Añadir Contrato</a>
                            </div>
                        </div>
                        <div class="table-container">
                            <table id="mytable" class="table table-bordred table-striped">
                                <thead>
                                    <th>Cliente</th>
                                    <th>Tipo de Contrato</th>
                                    <th>Estado</th>
                                    <th>Fecha de Inicio</th>
                                    <th>Fecha Final</th>
                                </thead>
                                <tbody>
                                    @if($contratos->count())
                                    @foreach($contratos as $contrato)
                                    <tr>
                                        <td>{{$contrato->cliente->nombres}} {{$contrato->cliente->apellidos}}</td>
                                        <td>{{$contrato->tipoContrato->nombre}}</td>
                                        <td><?php if( $contrato->created_at < NOW() ){ echo "Vigente"; }else{ echo "Vencido"; } ?>
                                        </td>
                                        <td>{{$contrato->fechaInicio}}</td>
                                        <td>{{$contrato->fechaFin}}</td>
                                        <td><a class="btn btn-primary btn-xs"
                                                href="{{action('ContratoController@edit', $contrato->idcontrato)}}"><span
                                                    class="glyphicon glyphicon-pencil"></span></a></td>
                                        <td>
                                            <form
                                                action="{{action('ContratoController@destroy', $contrato->idcontrato)}}"
                                                method="post">
                                                {{csrf_field()}}
                                                <input name="_method" type="hidden" value="DELETE">

                                                <button class="btn btn-danger btn-xs" type="submit"><span
                                                        class="glyphicon glyphicon-trash"></span></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="8">No hay registro !!</td>
                                    </tr>
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        {{ $contratos->links() }}
                    </div>

                </div>
            </div>
        </section>
    </div>

    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
    @endsection
