@extends('Layout.layout')
@section('content')
<!-- BEGIN BASE-->
  <div id="base">
    <div class="row">
      <section class="content" id="content">
        @if(Session::has('success'))
          <div class="alert alert-info">
            {{Session::get('success')}}
          </div>
        @endif
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="pull-left"><h3>Lista de tipo Contratos</h3></div>
                  <div class="pull-right">
                    <div class="btn-group">
                      <a href="{{ route('tipoContrato.create') }}" class="btn btn-info" >Crear Tipo de Contrato</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="table-container">
                    <div class="table-responsive">
                      <table id="mytable" class="table table-bordred table-striped">
                        <thead>
                          <th>Nombre</th>
                          <th>Descripción</th>
                          <th>Fecha de Creación</th>
                          <th>Prioridad</th>
                          <th>Tiempo max días abierto</th>
                        </thead>
                        <tbody>
                          @if($tipoContratos->count())
                            @foreach($tipoContratos as $tipoContrato)
                              <tr>
                                <td id="tipo-{{$tipoContrato->idtipoContrato}}">{{$tipoContrato->nombre}}</td>
                                <td width="40%" >{{$tipoContrato->descripcion}}</td>
                                <td width="15%">{{$tipoContrato->created_at}}</td>
                                <td>
                                  <div style="width: 100%; padding: 10px;">
                                    <span style=" padding: 1em; border: 2px solid {{$tipoContrato->color}}; border-radius: 5px;">
                                      {{$tipoContrato->prioridad->nombre}}
                                    </span>
                                  </div>
                                </td>
                                <td>{{$tipoContrato->prioridad->tiempoMaximoContacto}}</td>
                                <td>
                                  <div class="row">
                                    <div class="col-md-4 col-xs-4">
                                      <a class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Editar Tipo de contrato" href="{{action('TipoContratoController@edit', $tipoContrato->idtipoContrato)}}" onclick="editar(event,this)"  ><span class="glyphicon glyphicon-pencil"></span></a>
                                    </div>
                                    @if($tipoContrato->idtipoContrato != 1)
                                    <div class="col-md-4 col-xs-4">
                                      @if($tipoContrato->estado == 1)
                                        <a onclick="act_inac({{$tipoContrato->idtipoContrato}},0)" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Desactivar Tipo Contrato">
                                          <span class="md md-block"></span>
                                        </a>
                                      @else
                                        <a onclick="act_inac({{$tipoContrato->idtipoContrato}},1)" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Activar Tipo Contrato">
                                          <span class="md md-check"></span>
                                        </a>
                                      @endif
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                      @if($tipoContrato->is_delete())
                                        <form action="{{action('TipoContratoController@destroy', $tipoContrato->idtipoContrato)}}" onclick="borrar(event,this)" method="post">
                                          {{csrf_field()}}
                                          <input name="_method" type="hidden" value="DELETE">
                                          <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar Tipo de Contrato" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                        </form>
                                      @endif
                                    </div>
                                    @endif
                                  </div>
                                </td>
                              </tr>
                            @endforeach
                          @else
                            <tr>
                              <td colspan="8">No hay registro !!</td>
                            </tr>
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                  {{ $tipoContratos->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>

@endsection

@section('script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  function borrar(e,form){
    e.preventDefault();
    swal({
      title: "Confirmar eliminación de registro?",
      icon: "warning",
      buttons: ["Confirmar", "Cancelar"],
      dangerMode: true,
    }).then((willDelete) => {
      if (!willDelete) {
        form.submit();
      } else {
        return false;
      }
    });
  }

        function editar(e,href){
        e.preventDefault();
        swal({
          title: "Seguro que quieres editar este registro?",
          icon: "warning",
          buttons: ["Confirmar", "Cancelar"],
          dangerMode: true,
        }).then((willDelete) => {
          if (!willDelete) {
            window.location.href = href.href;
          } else {
            return false;
          }
        });
      }

  function act_inac(id,value){
    var title = 'Activar';
    var url   = 'activar';
    if (!value) {
      title = 'Desactivar';
      url   = 'desactivar';
    }
    swal({
      title: 'Está usted seguro de '+title+' el tipo contrato "'+ $('#tipo-'+id).text() +'"?',
      icon: "warning",
      buttons: ["Confirmar", "Cancelar"],
      dangerMode: true,
    }).then((willDelete) => {
      if (!willDelete) {
        window.location.href = "/tipoContrato/"+url+'/'+id;
      } else {
        return false;
      }
    });
  }
</script>
<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
@endsection
