@extends('Layout.layout')
@section('content')
<div id="base">
    <div class="row">
        <section class="content" id="content">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-header">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('tipoContrato.index') }}">Lista de Tipos de Contrato</a></li>
                        <li class="active">Actualizar Tipo de Contrato</li>
                    </ol>
                </div>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Error!</strong> Revise los campos obligatorios.<br>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-info">
                    {{Session::get('success')}}
                </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Actualizar Tipo de Contrato</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-container">
                            <form method="POST"
                                action="{{ route('tipoContrato.update',$tipoContrato->idtipoContrato) }}"
                                onsubmit="guardar(event,this)" role="form" id="accion_form">
                                {{ csrf_field() }}
                                <input name="_method" type="hidden" value="PATCH">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="nombre" id="nombre"
                                                value="{{ $tipoContrato->nombre }}" class="form-control input-sm"
                                                placeholder="Nombre">
                                            <p style="color: red;">{{ $errors->first('nombre') }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="color" name="color" id="color" value="{{$tipoContrato->color}}"
                                                class="form-control input-sm" placeholder="Color">
                                            <p style="color: red;">{{ $errors->first('color') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea name="descripcion" class="form-control input-sm"
                                        placeholder="Descripcion">{{$tipoContrato->descripcion}}</textarea>
                                    <p style="color: red;">{{ $errors->first('descripcion') }}</p>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="form-group">

                                            <label for="prioridadticket_idprioridadticket">Prioridades</label>
                                            <select id="prioridadticket_idprioridadticket"
                                                name="prioridadticket_idprioridadticket" class="form-control">
                                                @if($prioridades->count())
                                                @foreach($prioridades as $list)
                                                <option value="{{ $list->idPrioridadTicket}}"
                                                    {{ ($list->idPrioridadTicket == $tipoContrato->prioridadticket_idprioridadticket) ? 'selected' : '' }}>
                                                    {{ $list->nombre }}</option>
                                                @endforeach
                                                @endif
                                            </select>

                                        </div>
                                    </div>
                                    <p style="color: red;">{{ $errors->first('prioridadticket_idprioridadticket') }}</p>
                                </div>
                                <div class="row">

                                    <div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
                                        <input type="submit" value="Guardar" class="btn btn-success btn-block">
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    @endsection

    @section('script')

    <script src="{{ asset('js/notify.js')}}"></script>
    <script src="{{ asset('js/notificaciones.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        function guardar(e,form){
        e.preventDefault();
        swal({
          title: "Esta seguro de actualizar la informacion?",
          icon: "warning",
          buttons: ["Confirmar", "Cancelar"],
          dangerMode: true,
        }).then((willDelete) => {
          if (!willDelete) {
  			form.submit();
          } else {
            return false;
          }
        });
    }
    </script>
    @endsection
