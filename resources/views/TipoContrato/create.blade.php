@extends('Layout.layout')

@section('content')
<div id="base">
<div class="row">
	<section class="content" id="content">
		<div class="col-md-8 col-md-offset-2">
			<div class="section-header">
	            <ol class="breadcrumb">
	               <li><a href="{{ route('tipoContrato.index') }}">Lista de Tipos de Contrato</a></li>
	              <li class="active">Nuevo Tipo de Contrato</li>
	            </ol>
          	</div>
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Nuevo Tipo de Contrato</h3>
				</div>
				<div class="panel-body">
					<div class="table-container">
						<form method="POST" action="{{ route('tipoContrato.store') }}"  role="form" id="accion_form">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Nombre</label>
										<input type="text" name="nombre" id="nombre" value="{{ old('nombre') }}" class="form-control input-sm">
										<p style="color: red;" id="text-nombre">
											@if($errors->first('nombre'))
												{{ $errors->first('nombre') }}
											@else
												Campo Requerido
											@endif
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Color</label>
										<input type="color" name="color" value="{{ old('color') }}" class="form-control input-sm">
										<p style="color: red;">{{ $errors->first('color') }}</p>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Descripción</label>
								<textarea name="descripcion" class="form-control input-sm">{{ old('descripcion') }}</textarea>
								<p style="color: red;" id="text-nombre">
									@if($errors->first('descripcion'))
										{{ $errors->first('descripcion') }}
									@else
										Campo Requerido
									@endif
								</p>
							</div>
							<div class="row">
								<div class="col-xs-4 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="prioridadticket_idprioridadticket">Prioridades</label>
										<select id="prioridadticket_idprioridadticket" onchange="select()" name="prioridadticket_idprioridadticket" class="form-control">
											<option value="0">seleccione tipo de prioridad</option>
											@if($prioridades->count())
												@foreach($prioridades as $list)
												 	<option value="{{ $list->idPrioridadTicket}}">{{ $list->nombre }}</option>
												@endforeach
											@endif
										</select>
										<p style="color: red;">{{ $errors->first('prioridadticket_idprioridadticket') }}</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
									<input type="submit"  value="Guardar" class="btn btn-success btn-block">
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script src="{{ asset('js/notify.js')}}"></script>
<script src="{{ asset('js/notificaciones.js')}}"></script>
<script>
	$("#nombre").on("keyup", function(){
		if ($(this).val() == "") {
			$('#text-nombre').text('Campo requerido')
		}else{
			$.ajax({
				type: 'GET',
				url: '/tipoContrato/validateNombre/'+$('#nombre').val(),
				data: $(this).serialize(),
				success: function(data) {
					if (data == 1){
						$('#text-nombre').text('El tipo contrato ya existe');
					}else{
						$('#text-nombre').text('');
					}
				}
			});
		}
	});
</script>
@endsection
