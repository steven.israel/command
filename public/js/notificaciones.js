

buscar = document.getElementById("search");
buscar.addEventListener('keyup', busqueda);
var x = document.getElementById("lista");



function busqueda() {
    
    $.ajax({
        type: 'GET',
        url: '/busqueda/'+buscar.value,
        data: $(this).serialize(),
        success: function (data) {
            if (data != "Sin coincidencias") {
                x.innerHTML="";  
                data.forEach(function(element){
                x.innerHTML+='<option onclick"alert('+element['idticket']+')" >'+ element['numeroDeCaso'] +'</option>';  
                });
            }else{
                x.innerHTML="";  
                x.innerHTML='<option>Sin coincidencias</option>';   
            }
        }
    });
}


function programados() {
    console.log("programacion");
    $.ajax({
        type: 'GET',
        url: '/listaProgramados',
        data: $(this).serialize(),
        success: function (data) {
        }
    });
}


function mail() {
    $.ajax({
        type: 'GET',
        url: '/mail/send',
        data: $(this).serialize(),
        success: function (data) {
            //control del response
            console.log(data);
        }
    });
}

function listaNotificaciones() {
    $.ajax({
        type: 'GET',
        url: '/listadoAbiertos',
        data: $(this).serialize(),
        success: function (data) {
            if (data != "") {
                visto = data[1];
                j = visto > 0 ? visto : 0;
                $('#notificacion').empty();
                $("#notificacion").append('<li class="dropdown-header">Notificaciones de Tickets sin Actualizar A</li>');
                data[0].forEach(function (element) {
                    $('#notificacion').append('<li onclick="VerTicket(' + element['idticket'] + ')">' +
                        '<a class="alert alert-callout alert-info" href="javascript:void(0);">' +
                        '<img class="pull-right img-circle dropdown-avatar" src="" alt="" />' +
                        '<strong>Ticket: ' + element['titulo'] + '</strong><br/>' +
                        '<small>El ticket fue creado el ' + element['date'] + ' </small>' +
                        '</a>' +
                        '</li>');
                });
                // console.log(j);
                $("#tot").empty();
                $("#tot").append(j);
                $("#tot").show();
                $('#notificacion').append('<li class="dropdown-header">Options</li><li><a href="/ticket">Ver todos los tickets <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>');
            }
        }
    });
}

document.getElementById("visto").onclick = function () {
    $.ajax({
        url: '/actualizarVistos',
        type: "get",
        processData: false,
        success: function (data) {
            $("#tot").empty();
        },
        error: function () { }
    });
};

function NotificacionesPush() {
    $.ajax({
        type: 'GET',
        url: '/notificacionesSistema',
        data: $(this).serialize(),
        success: function (data) {
            if (data != "") {

                data.forEach(function (element) {


                    if (element["tecnico"] == undefined) {
                        actualizarNotificacion(element["idnotificaciones"])
                        var d = new Date(element["created_at"]);

                        Notify(element["titulo"] + " El dia: " + d,
                            function () {
                                window.location.href = "/ticket/" + element["idticket"];
                            },
                            function () {
                                actualizarNotificacion(element["idnotificaciones"])
                            },
                            'success'
                        );
                    } else {
                        console.log(element["idnotificaciones"]);
                        actualizarNotificacionTecnico(element["idnotificaciones"])
                        var d = new Date(element["created_at"]);

                        Notify(element["titulo"] + " El dia: " + d,
                            function () {
                                window.location.href = "/ticket/" + element["idticket"];
                            },
                            function () {
                                actualizarNotificacionTecnico(element["idnotificaciones"])
                            },
                            'success'
                        );
                    }
                });
            }
        }
    });
}


function actualizarNotificacion(id) {

    $.ajax({
        type: 'GET',
        url: '/notificacionVista/' + id,
        data: $(this).serialize(),
        success: function (data) {
            if (data != "") {

                console.log(data);

            }
        }
    });
}

function actualizarNotificacionTecnico(id) {

    $.ajax({
        type: 'GET',
        url: '/notificacionVistaTecnico/' + id,
        data: $(this).serialize(),
        success: function (data) {
            if (data != "") {

                console.log(data);

            }
        }
    });


}

function VerTicket(id) {
    //alert(id);
    location.href = "/ticket/" + id;
}

$(document).ready(function () {
    NotificacionesPush()
    listaNotificaciones();
    mail();
    //notificacion();

    //cargar pickers cada 10 segundos
    setInterval(function () { mail(); }, 5000);

    setInterval(function () { NotificacionesPush() }, 10000);

    //cargar pickers cada 10 segundos
    // setInterval(function(){ notificacion(); },40000);
    setInterval(function () { listaNotificaciones(); }, 10000);

});

