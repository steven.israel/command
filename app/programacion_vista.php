<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class programacion_vista extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'programacion_vista';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idvista';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['iduser','idprogramado','created_at','updated_at',];
}
