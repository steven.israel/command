<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_ingreso';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idingreso';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','color','created_at','updated_at',];
}
