<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialRma extends Model
{
    protected $table = 'historial_rmas';

    protected $fillable = ['comentario','id_rma','id_user','id_estado_anterior','id_estado_actual','fecha','hora','created_at','updated_at'];

    public function user(){
        return $this->hasOne('App\usuario','id','id_user');
    }

    public function rma(){
        return $this->hasOne('App\Rma','id','id_rma');
    }
}
