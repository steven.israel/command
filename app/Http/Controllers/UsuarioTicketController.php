<?php

namespace App\Http\Controllers;

use App\UsuarioTicket;
use Illuminate\Http\Request;

class UsuarioTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsuarioTicket  $usuarioTicket
     * @return \Illuminate\Http\Response
     */
    public function show(UsuarioTicket $usuarioTicket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsuarioTicket  $usuarioTicket
     * @return \Illuminate\Http\Response
     */
    public function edit(UsuarioTicket $usuarioTicket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsuarioTicket  $usuarioTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsuarioTicket $usuarioTicket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsuarioTicket  $usuarioTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsuarioTicket $usuarioTicket)
    {
        //
    }
}
