<?php

namespace App\Http\Controllers;

use PDF;
use App\cliente;
use App\empresa;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function __construct() { 
        $this->middleware('preventBackHistory'); 
        $this->middleware('auth'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clientes=cliente::orderBy('idcliente','DESC')->get();
        return view('Cliente.index',compact('clientes')); 
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $empresas = empresa::all();
        return view('Cliente.create')->with('empresas', $empresas);
    }

    public function export_pdf()
    {
        //set_time_limit(0);
         // Obtener todos los clientes de la base de datos
        $data = cliente::all();
        // Enviar datos a la vista usando la función loadView de la fachada PDF
        $pdf = PDF::loadView('Cliente.pdf',compact('data')) ;
        // If you want to store the generated pdf to the server then you can use the store function
        $pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->download('Cliente.pdf');
    }

    public function seleccion($id, $empresa)
    {
        
        $cliente = cliente::find($id);

        $cliente->estado = 2;

        if ($cliente->save()) {
            return 1;
        }else{
            return 0;
        }

    }

    public function unSeleccion($id, $empresa){

        $cliente = cliente::find($id);

        $cliente->estado = 1;

        if ($cliente->save()) {
            return 2;
        }else{
            return 0;
        }

    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
                'nombres'   =>'required|string|max:255', 
                'apellidos' =>'required|string|max:255', 
                'email'     =>'required|email|unique:cliente,email', 
                'telefono'  =>'required|string', 
                'celular'   =>'nullable|string', 
                'cargo'     =>'required|string|max:255',
                'estado'    =>'required|int|min:1', 
                'idempresa' =>'required|int|exists:empresa,idempresa'
        ],[
            'idempresa.exists'  => 'Debe seleccionar una empresa valida',
            'estado.min'        => 'Debe seleccionar el estado'    
        ]);
        cliente::create($request->all());
        return redirect()->route('cliente.index')->with('success','Registro creado satisfactoriamente');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $libros= cliente::find($id);
        return  view('Cliente.show',compact('libros'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cliente= cliente::find($id);
        $empresas = empresa::all();
        return view('Cliente.edit',compact('cliente'))->with('empresas', $empresas);
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {
        //
        $this->validate($request,[
                'nombres'   =>'required|string|max:255', 
                'apellidos' =>'required|string|max:255', 
                'email'     =>'required|email', 
                'telefono'  =>'required|string', 
                'celular'   =>'nullable|string', 
                'cargo'     =>'required|string|max:255',
                'estado'    =>'required|int|min:1', 
                'idempresa' =>'required|int',
        ],[
            'estado.min'        => 'Debe seleccionar el estado'  
        ]);
 
        cliente::find($id)->update($request->all());
        return redirect()->route('cliente.index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        cliente::find($id)->delete();
        return redirect()->route('cliente.index')->with('success','Registro eliminado satisfactoriamente');
    }

    public function desactivar($id)
    {
        //
        $cliente = cliente::find($id);

        $cliente->estado = 2;

        $cliente->save();

        return redirect()->back()->with('success','Cliente Deshabilitada');
    }

    public function activar($id)
    {
        //
        $cliente = cliente::find($id);

        $cliente->estado = 1;

        $cliente->save();

        return redirect()->back()->with('success','Cliente Activada');
    }
}
