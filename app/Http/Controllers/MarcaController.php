<?php

namespace App\Http\Controllers;

use App\Marca;
use Illuminate\Http\Request;

class MarcaController extends Controller
{
    public function __construct() { 
        $this->middleware('preventBackHistory');
        $this->middleware('auth'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marcas = Marca::orderBy('idmarca','DESC')->get();

        return view('Marca.index',compact('marcas')); 
    }
    
    public function validateNombre($nombre){
        $marca = Marca::where('nombre',$nombre)->count();

        if($marca){ return 1; }else{ return 0; }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Marca.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nombre' => 'required|string|unique:marca,nombre', 
            'estado' => 'required|int|min:1'
        ]);

        $marca = new Marca();

        $marca->nombre = $request->nombre;
        $marca->estado = $request->estado;

        if ( $marca->save() ) {
            return redirect()->route('marca.index')->with('success','Registro creado satisfactoriamente');
        }else{
            return redirect()->route('marca.index')->with('danger','Registro creado satisfactoriamente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function show(Marca $marca)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marca = Marca::find($id);
        return view('Marca.edit',compact('marca'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'nombre' => 'required|string', 
            'estado' => 'required|int|min:1'
        ]);

        $marca = Marca::find($id);

        $marca->nombre = $request->nombre;
        $marca->estado = $request->estado;

        if ( $marca->save() ) {
            return redirect()->route('marca.index')->with('success','Registro actualizado exitosamente');
        }else{
            return redirect()->route('marca.index')->with('danger','Registro actualizado exitosamente');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Marca::find($id)->delete();
        return redirect()->route('marca.index')->with('success','Registro eliminado satisfactoriamente');
    }

    public function desactivar($id)
    {
        //
        $marca = Marca::find($id);

        $marca->estado = 2;

        $marca->save();

        return redirect()->route('marca.index')->with('success','Marca deshabilitada');
    }

    public function activar($id)
    {
        //
        $marca = Marca::find($id);

        $marca->estado = 1;

        $marca->save();

        return redirect()->route('marca.index')->with('success','Marca Activada');
    }
}
