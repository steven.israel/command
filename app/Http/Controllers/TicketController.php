<?php

namespace App\Http\Controllers;

use DB;
use App\user;
use App\ticket;
use App\estados;
use App\empresa;
use App\cliente;
use App\Ingreso;
use App\prioridad;
use App\notificaciones;
use App\programacion_vista;
use App\UsuarioTicket;
use App\contrato;
use App\usuario;
use App\historico;
use App\Http\Requests\validateTicketCreate;
use Illuminate\Support\Facades\Hash;
use App\programacion;
use App\tipoContrato;
use DateTime;
use Illuminate\Http\Request;
use App\Mail\EstadosMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

use Webklex\IMAP\Client;

class TicketController extends Controller
{
    private $ticket_id;
    private $verificar_cliente;
    private $ticket;
    private $temail;

    public function __construct()
    {
        $this->middleware('preventBackHistory');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */





    public function index()
    {
        //
        $rol = auth()->user()->role;

        if ($rol == 3 || $rol == 4 || $rol == 5) {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('programado', 0)->where('comercial', auth()->user()->id)->orWhere('tecnico', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        } else {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('programado', 0)->orderBy('created_at', 'DESC')->get();
        }


        return view('Ticket.index', compact('tickets'))->with('titulo', "");
    }


    
    public function allTickets()
    {
        //
        $rol = auth()->user()->role;

        if ($rol == 3 || $rol == 4 || $rol == 5) {
            $tickets = ticket::where('comercial', auth()->user()->id)->orWhere('tecnico', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        } else {
            $tickets = ticket::orderBy('created_at', 'DESC')->get();
        }


        return view('Ticket.index', compact('tickets'))->with('titulo', "Todos");
    }


    /**
     * Lisado de mails recibidos.
     *
     * @return \Illuminate\Http\Response
     */
    public function tickets_mails()
    {

        $rol = auth()->user()->role;

        if ($rol == 5) {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 2)->orWhere('comercial', auth()->user()->id)->where('tecnico', auth()->user()->id)->orderBy('idticket', 'DESC')->get();
        } else {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 2)->where('programado', 0)->orderBy('created_at', 'DESC')->get();
        }

       

        return view('Ticket.mails', compact('tickets'))->with('titulo', "Ingresados por correo");
    }
    /**
     * Tickets ingresados y validados.
     *
     * @return \Illuminate\Http\Response
     */
    public function tickets_ingresados()
    {
        $rol = auth()->user()->role;

        if ($rol == 3 || $rol == 4 || $rol == 5) {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 1)->where('tecnico', auth()->user()->id)->orWhere('comercial', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        } else {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 1)->orderBy('created_at', 'DESC')->get();
        }

        return view('Ticket.index', compact('tickets'))->with('titulo', "Validados");
    }
    /**
     * Tickets ingresados y validados en revision.
     *
     * @return \Illuminate\Http\Response
     */
    public function tickets_revision()
    {
        $rol = auth()->user()->role;

        if ($rol == 3 || $rol == 4 || $rol == 5) {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 2)->where('tecnico', auth()->user()->id)->orWhere('comercial', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        } else {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 2)->orderBy('created_at', 'DESC')->get();
        }

        return view('Ticket.index', compact('tickets'))->with('titulo', "en revisión");
    }
    /**
     * Tickets ingresados y validados en proceso.
     *
     * @return \Illuminate\Http\Response
     */
    public function tickets_proceso()
    {
        $rol = auth()->user()->role;

        if ($rol == 3 || $rol == 4 || $rol == 5) {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 3)->where('tecnico', auth()->user()->id)->orWhere('comercial', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        } else {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 3)->orderBy('created_at', 'DESC')->get();
        }

        return view('Ticket.index', compact('tickets'))->with('titulo', "en proceso");
    }
    /**
     * Tickets ingresados y validados en proceso.
     *
     * @return \Illuminate\Http\Response
     */
    public function tickets_respuesta()
    {
        $rol = auth()->user()->role;

        if ($rol == 3 || $rol == 4 || $rol == 5) {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 4)->where('tecnico', auth()->user()->id)->orWhere('comercial', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        } else {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 4)->orderBy('created_at', 'DESC')->get();
        }

        return view('Ticket.index', compact('tickets'))->with('titulo', "en repuesta del cliente");
    }
    /**
     * Tickets ingresados y validados en proceso.
     *
     * @return \Illuminate\Http\Response
     */
    public function tickets_cerrado()
    {
        $rol = auth()->user()->role;

        if ($rol == 3 || $rol == 4 || $rol == 5) {
            $tickets = ticket::where('archivado', 1)->orWhere('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 5)->where('tecnico', auth()->user()->id)->orWhere('comercial', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        } else {
            $tickets = ticket::where('archivado', 1)->orWhere('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 5)->orderBy('created_at', 'DESC')->get();
        }

        return view('Ticket.index', compact('tickets'))->with('titulo', "Cerrados");
    }

    public function tickets_reabierto()
    {
        $rol = auth()->user()->role;

        if ($rol == 3 || $rol == 4 || $rol == 5) {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 6)->where('tecnico', auth()->user()->id)->orWhere('comercial', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        } else {
            $tickets = ticket::where('archivado', 0)->where('tipo_ingreso', 1)->where('EstadoTicket_idEstadoTicket', 6)->orderBy('created_at', 'DESC')->get();
        }

        return view('Ticket.index', compact('tickets'))->with('titulo', "Re-Abiertos");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function archivados()
    {
        //
        $rol = auth()->user()->role;

        if ($rol == 3 || $rol == 4 || $rol == 5) {
            $tickets = ticket::where('archivado', 1)->where('tipo_ingreso', 1)->where('tecnico', auth()->user()->id)->orWhere('comercial', auth()->user()->id)->orderBy('idticket', 'DESC')->paginate(5);
        } else {
            $tickets = ticket::where('archivado', 1)->where('tipo_ingreso', 1)->orderBy('idticket', 'DESC')->paginate(5);
        }
        return view('Ticket.archivados', compact('tickets'))->with('titulo', "");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function notificacionesSistema()
    {
        if (Auth()->user()->role == 1) {
            $noti = notificaciones::where('status', 0)->orderby('created_at', 'desc')->get()->take(4);
            return $noti;
        }else if (Auth()->user()->role != 1 && Auth()->user()->role != 3 && Auth()->user()->role != 5) {
            $noti2 = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
                ->whereIn('notificaciones.status', [0,2])
                ->where('ticket.tecnico', Auth()->user()->id)
                ->where('ticket.mensaje','<>', 'interno')
                ->select(
                    'ticket.tecnico as tecnico',
                    'notificaciones.idnotificaciones',
                    'notificaciones.created_at',
                    'notificaciones.titulo as titulo',
                    'notificaciones.idticket'
                )
                ->get()
                ->take(4);
            return $noti2;
        }

        else if (Auth()->user()->role == 5) {
            $noti2 = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
                ->where('notificaciones.status', 2)
                ->where('notificaciones.mensaje','interno-')
                ->where('ticket.comercial', Auth()->user()->id)
                ->select(
                    'ticket.tecnico as tecnico',
                    'notificaciones.idnotificaciones',
                    'notificaciones.created_at',
                    'notificaciones.titulo as titulo',
                    'notificaciones.idticket'
                )
                ->get()
                ->take(4);
            return $noti2;
        }
    }

    public function actualizarNotificacionesVista($id)
    {
        $notificacion = notificaciones::find($id);
        $notificacion->status = 1;
        $notificacion->save();

        $tickets = ticket::Find($notificacion->idticket);
        if($tickets->abierto != 1){
            $tickets->abierto   = 1;
        }
    }

    public function actualizarNotificacionesVistaTecnico($id)
    {
        $notificacion = notificaciones::find($id);
        $notificacion->status = 3;
        $notificacion->save();
        
        $tickets = ticket::Find($notificacion->idticket);
        if($tickets->abierto != 1){
            $tickets->abierto   = 1;
        }
    }



    public function listaAbiertos()
    {
        //se verifica que el usuario autenticado sea el admin y se procede a traer las notificaciones perteneciente
        //y lo mismo se hace cuando no es admin
        if (Auth::user()->role == 1 || Auth::user()->role == 3) {
            $notificaciones = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
                ->whereBetween('notificaciones.mensaje',  ['Existe', 'Programado'])
                // ->orWhere('notificaciones.mensaje', 'like', '%Ticket programado para este dia%')
                ->select(
                    'ticket.tecnico as tecnico',
                    'notificaciones.idnotificaciones',
                    'notificaciones.created_at as date',
                    'notificaciones.titulo as titulo',
                    'notificaciones.idticket as idticket'
                )
                ->take(10)
                ->orderBy('notificaciones.created_at', 'desc')
                ->get();
            $vistos = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
                ->whereBetween('notificaciones.mensaje',  ['Existe', 'Programado'])
                ->where('visto', 0)->get();
            $data = [$notificaciones, count($vistos)];
            return $data;
        } else if(Auth()->user()->role != 1 && Auth()->user()->role != 3 && Auth()->user()->role != 5) {
            $notificaciones2 = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
                ->where('ticket.tecnico', Auth::user()->id)
                ->whereBetween('notificaciones.mensaje',  ['Asignacion', 'Respuesta'])
                ->where('notificaciones.mensaje', '<>', 'Existe')
                ->select(
                    'ticket.tecnico as tecnico',
                    'notificaciones.idnotificaciones',
                    'notificaciones.created_at as date',
                    'notificaciones.titulo as titulo',
                    'notificaciones.idticket as idticket',
                    'notificaciones.visto as vistoN'
                )
                ->orderBy('notificaciones.created_at', 'desc')
                ->take(10)
                ->get();
            $vistos2 = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
                ->where('visto', 0)
                ->where('ticket.tecnico', Auth::user()->id)
                ->whereBetween('notificaciones.mensaje',  ['Asignacion', 'Respuesta'])
                ->where('notificaciones.mensaje', '<>', 'Existe')
                ->get();
            $data = [$notificaciones2, count($vistos2)];
            return $data;
        } else if(Auth()->user()->role == 5) {

            $notificaciones2 = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
            ->where('ticket.comercial', Auth::user()->id)
            ->where('notificaciones.mensaje','interno-')
            ->where('notificaciones.mensaje', '<>', 'Existe')
            ->select(
                'ticket.comercial as tecnico',
                'notificaciones.idnotificaciones',
                'notificaciones.created_at as date',
                'notificaciones.titulo as titulo',
                'notificaciones.idticket as idticket',
                'notificaciones.visto as vistoN'
            )
            ->orderBy('notificaciones.created_at', 'desc')
            ->take(10)
            ->get();
        $vistos2 = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
            ->where('visto', 0)
            ->where('ticket.comercial', Auth::user()->id)
            ->where('notificaciones.mensaje','interno-')
            ->where('notificaciones.mensaje','<>','Existe')
            ->get();
        $data = [$notificaciones2, count($vistos2)];
        return $data;
        }
    }
    //funcion para mostrar las notificaciones en visto
    public function actualizarVisto()
    {

        if (Auth::user()->role == 1) {
            $vistos = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
                ->whereBetween('notificaciones.mensaje',  ['Existe', 'Programado'])
                ->where('visto', 0)
                ->get();
            if (count($vistos) > 0) {
                foreach ($vistos as $v) {
                    $v->visto = 1;
                    $v->update();
                }
                echo 'entre';
            } else {
                echo " no hay registros";
            }
        } else {
            $vistos = notificaciones::join('ticket', 'ticket.idticket', '=', 'notificaciones.idticket')
                ->whereBetween('notificaciones.mensaje',  ['Asignacion', 'Respuesta'])
                ->where('visto', 0)
                ->get();
            if (count($vistos) > 0) {
                foreach ($vistos as $v) {
                    $v->visto = 1;
                    $v->update();
                }
                echo 'entre';
            } else {
                echo " no hay registros";
            }
        }
    }

    public function clientes($id)
    {

        $clientes = cliente::where('idempresa', '=', $id)->where('estado', 1)->get(['idcliente', 'nombres', 'apellidos']);

        if (count($clientes) > 0) {
            return $clientes;
        } else {
            return "";
        }
    }

    public function cliente_info($id)
    {

        $info = DB::table('cliente')
            ->join('empresa', 'empresa.idempresa', '=', 'cliente.idempresa')
            ->join('tipocontrato', 'tipocontrato.idtipoContrato', '=', 'empresa.idtipocontrato')
            ->join('prioridadticket', 'prioridadticket.idPrioridadTicket', '=', 'tipocontrato.prioridadticket_idprioridadticket')
            ->select('cliente.*', 'tipocontrato.nombre as contrato', 'prioridadticket.nombre as prioridad')
            ->where('cliente.idcliente', $id)
            ->where('cliente.estado', 1)
            ->get();

        if (count($info) > 0) {
            return $info;
        } else {
            return "";
        }
    }

    // funcion para enviar correos
    public function send()
    {

        $auxB = true;
       
        $oClient = new Client([
            'host'          => 'imap.gmail.com',
            'port'          => 993,
            'encryption'    => 'ssl',
            'validate_cert' => true,
            'username'      => 'soportetickets2018@gmail.com',
            'password'      => '10010110',
            'protocol'      => 'imap'
        ]);


        /* Alternative by using the Facade
        $oClient = Webklex\IMAP\Facades\Client::account('default');
        */

        //Connect to the IMAP Server
        $oClient->connect();

        //Get all Mailboxes
        /** @var \Webklex\IMAP\Support\FolderCollection $aFolder */
        $aFolder = $oClient->getFolders();

        //Loop through every Mailbox
        /** @var \Webklex\IMAP\Folder $oFolder */
        foreach ($aFolder as $oFolder) {

                        


            //Get all Messages of the current Mailbox $oFolder
            /** @var \Webklex\IMAP\Support\MessageCollection $aMessage */
            $aMessage = $oFolder->messages()->unseen()->all()->get();

            // $aMessage = $oFolder->messages()->since('22.05.2021')->get();

            /** @var \Webklex\IMAP\Message $oMessage */
            foreach ($aMessage as $oMessage) {


                $verificar_ticket = UsuarioTicket::select('*')->join('ticket', 'ticket.idticket', '=', 'usuario_ticket.id_ticket')->get();

                    $mystring = $oMessage->getSubject();
                    
                    $bandera = 0;

                    if (count($verificar_ticket) > 0) {
                        foreach ($verificar_ticket as $value) {
                            $findme   = $value->titulo;
                            $pos = strpos($mystring, $findme);
                            $pos2 = strpos("Re: ".$mystring,"Re: ".$findme);
    
                            if($pos2 === false && $pos === false && $bandera == 0) {
                                echo "no ha ticket";
                            } else {
                                $historico = new historico();
                                $historico->Ticket_idticket = $value->idticket;
                                $historico->descripcion = $oMessage->getHTMLBody(true); //cuando el cliente manda una respuesta
                                $historico->tipo = 9;
                                $historico->usuario = 1;
                                //tipo 9 cuando es respuesta del usuario
                                $historico->tipo_usuario = 2;
                                $historico->estado_ticket = 1;
                                $historico->save();
    
    
                                if ($oMessage->getAttachments()->count() > 0) {
                                    $aAttachment = $oMessage->getAttachments();
    
                                    $aAttachment->each(function ($oAttachment) {
                                        /** @var \Webklex\IMAP\Attachment $oAttachment */
                                        if ($oAttachment->getType() == 'image') {
                                            $oAttachment->save(public_path('/attach'), $oAttachment->getName());
                                            $historico = new historico();
                                            $historico->Ticket_idticket = $value->idticket;
                                            $historico->descripcion = $value->descripcion;
                                            $historico->tipo = 13;
                                            $historico->usuario = $value->idcliente;
                                            //tipo usuario 2: cuando es agente y 1: cuando es cliente
                                            $historico->tipo_usuario = 2;
                                            $historico->estado_ticket = 1;
                                            $historico->save();
                                        } else {
                                            $oAttachment->save(public_path('/attach'), $oAttachment->getName());
                                            $historico = new historico();
                                            $historico->Ticket_idticket = $value->idticket;
                                            $historico->descripcion =$value->descripcion;
                                            $historico->tipo = 14;
                                            $historico->usuario = $value->idcliente;
                                            //tipo usuario 2: cuando es agente y 1: cuando es cliente
                                            $historico->tipo_usuario = 2;
                                            $historico->estado_ticket = 1;
                                            $historico->save();
                                        }
                                    });
                                }
    
    
    
                                $auxB = false;
                                $tk = ticket::findOrFail($value->idticket);
                                $nc = $tk->numeroDeCaso;
                                //notificacion de nueva respuesta en ticket epecifico al tecnico que se le ha asignado dicho
                                $notificacion = new notificaciones();
                                $notificacion->titulo = "Nueva respuesta en Ticket " . $nc;
                                $notificacion->mensaje = "Respuesta";
                                $notificacion->idticket = $value->idticket;
                                $notificacion->status = 2;
        
                                $notificacion->save();
                                break;
                            }
                        }
                    } else {
                        echo "no hay tickets (if si hay cliente)";
                    }


               
                



                
                

                // echo $oMessage->from[0]->mail;
                // echo ' ' . $oMessage->from[0]->personal;
                // echo ' ' . $oMessage->getSubject() . '<br />';
                // echo "<br>";

                // obtenemos el nomrbe y apellido del cliente
                $nombreCliente = explode(" ", $oMessage->from[0]->personal);

                //Buscando usuarios copiados en el mensaje

                //Obtener e tamaño del arreglo de message, to y cc del correo
                
                //si son mayores que 0 obtener las direcciones email 

                //ignorar el codigo de la empresa de

                //hacer a validacion de una respuesta por numero de ticket por el asunto 

                $cliente_nombre = "";
                $cliente_apellido = "";

                if (isset($nombreCliente[0])) {
                    $cliente_nombre = $nombreCliente[0];
                }

                if (isset($nombreCliente[1])) {
                    $cliente_apellido = $nombreCliente[1];
                } else {
                    $cliente_apellido = "---";
                }

                // verificar si existe el cliente con el correo
                $this->verificar_cliente = cliente::where('email', $oMessage->from[0]->mail)->get();

                echo $this->verificar_cliente;
                echo count($this->verificar_cliente);
                if (count($this->verificar_cliente) == 0) {


                    




                    echo " <script> console.log('cliente no existe');</script>";

                    // si el cliente no existe
                    echo "el cliente no existe en la base";
                    // procedemos a crear el cliente en la base
                    $cliente = new cliente();
                    $cliente->nombres   = $cliente_nombre;
                    $cliente->apellidos = $cliente_apellido;
                    $cliente->email     = $oMessage->from[0]->mail;
                    $cliente->password  = 12345;
                    $cliente->telefono  = "12345678";
                    $cliente->idempresa = 1;
                    $cliente->cargo = "Sin cargo registrado";
                    $cliente->estado = 1;
                    echo "verificar guardado";
                    if ($cliente->save()) {

                        echo "el cliente ha sido registrado";
                        // luego de crear al cliente le creamos el ticket
                        // creamos el numero de caso
                        $ultimo = ticket::orderby('created_at', 'DESC')->take(1)->get(['idticket']);

                        if ($ultimo->count()) {
                            if ($ultimo[0]->idticket < 10) {
                                $cero = "00000";
                            } else if ($ultimo[0]->idticket > 10) {
                                $cero = "0000";
                            } else if ($ultimo[0]->idticket > 100) {
                                $cero = "000";
                            } else if ($ultimo[0]->idticket > 1000) {
                                $cero = "00";
                            } else if ($ultimo[0]->idticket > 10000) {
                                $cero = "0";
                            } else {
                                $cero = "";
                            }
                            $numero = date('Ymd') . $cero . $ultimo[0]->idticket;
                        } else {
                            $cero = "000000";
                            $numero = date('Ymd') . $cero;
                        }

                        // buscamos la empresa para obtener la prioridad
                        $empresa = empresa::find(1);

                        //asignamos el ticket al cliente
                        $tecnicos = usuario::whereIn('role', [3,4])->where('status',1)->orderBy('role', 'ASC')->first();

                        
                        if (empty($tecnicos)) {
                            $tecnicoAsignado = 1;
                        }else{
                            $tecnicoAsignado = $tecnicos->id;
                        }


                        $this->ticket = new ticket();

                        $this->ticket->numeroDeCaso = $numero;
                        $this->ticket->PrioridadTicket_idPrioridadTicket = 1;
                        echo "prioridad normal";
                        $this->ticket->tipoCierre_idtipoCierre = 1;
                        $this->ticket->Cliente_idcliente = $cliente->idcliente;
                        $this->ticket->EstadoTicket_idEstadoTicket = 1;
                        $this->ticket->descripcion = $oMessage->getHTMLBody(true);
                        $this->ticket->titulo = $oMessage->getSubject();
                        $this->ticket->tecnico =  $tecnicoAsignado;
                        $this->ticket->idusuario = 1;
                        $this->ticket->comentario = "Ingresado automaticamente";
                        $this->ticket->tipo_ingreso = 2;
                        $this->ticket->abierto = 0;
                        $this->ticket->idingreso = 2;
                        $id = $this->ticket->idticket;
               
                        if ($this->ticket->save()) {
                            if(isset($oMessage->cc)){
                                foreach ($oMessage->cc as $mail){                                    
                                    if($mail->mail != "soportetickets2018@gmail.com"){
                                        // echo '<br>';
                                        // echo 'Asunto: '.$oMessage->getSubject();
                                        // echo '<br>';
                                        // echo 'To: ';
                                        // print_r($mail->mail);
                                        // echo '<br>';            
                                        $usuarioMail = new UsuarioTicket;            
                                        $usuarioMail->email = $mail->mail;
                                        $usuarioMail->id_ticket = $this->ticket->idticket;
                                        $usuarioMail->save();                    
                                    }                            
                                }            
                            }
            
                            if(isset($oMessage->to)){            
                                foreach ($oMessage->to as $mail){                                    
                                    if($mail->mail != "soportetickets2018@gmail.com"){
                                        // echo '<br>';
                                        // echo 'Asunto: '.$oMessage->getSubject();
                                        // echo '<br>';
                                        // echo 'To: ';
                                        // print_r($mail->mail);
                                        // echo '<br>';            
                                        $usuarioMail = new UsuarioTicket;
                                        $usuarioMail->email = $mail->mail;
                                        $usuarioMail->id_ticket = $this->ticket->idticket;
                                        $usuarioMail->save();
            
                                    }
                                }
                            } 

                            echo "<br>Guardo el ticket <br>";
                            $historico = new historico();
                            $historico->Ticket_idticket = $this->ticket->idticket;
                            $historico->descripcion = $this->ticket->descripcion;
                            $historico->tipo = 8;
                            $historico->usuario = 1;
                            //tipo usuario 2: cuando es agente y 1: cuando es cliente
                            $historico->tipo_usuario = 2;
                            $historico->estado_ticket = 1;

                            if($auxB){

                                $historico->save();

                            }

                            if ($auxB) {

                                if ($oMessage->getAttachments()->count() > 0) {
                                    $aAttachment = $oMessage->getAttachments();

                                    $aAttachment->each(function ($oAttachment) {
                                        /** @var \Webklex\IMAP\Attachment $oAttachment */
                                        if ($oAttachment->getType() == 'image') {
                                            $oAttachment->save(public_path('/attach'), $oAttachment->getName());
                                            $historico = new historico();
                                            $historico->Ticket_idticket = $this->ticket->idticket;
                                            $historico->descripcion = $this->ticket->descripcion;
                                            $historico->tipo = 13;
                                            $historico->usuario = $this->verificar_cliente[0]->idcliente;
                                            //tipo usuario 2: cuando es agente y 1: cuando es cliente
                                            $historico->tipo_usuario = 2;
                                            $historico->estado_ticket = 1;
                                            $historico->save();
                                        } else {
                                            $oAttachment->save(public_path('/attach'), $oAttachment->getName());
                                            $historico = new historico();
                                            $historico->Ticket_idticket = $this->ticket->idticket;
                                            $historico->descripcion = $this->ticket->descripcion;
                                            $historico->tipo = 14;
                                            $historico->usuario = $this->verificar_cliente[0]->idcliente;
                                            //tipo usuario 2: cuando es agente y 1: cuando es cliente
                                            $historico->tipo_usuario = 2;
                                            $historico->estado_ticket = 1;
                                            $historico->save();
                                        }
                                    });
                                }

                                //guardar notificacion

                                $notificacion = new notificaciones();

                                $notificacion->titulo = "Nuevo Ticket " . $this->ticket->numeroDeCaso;
                                $notificacion->mensaje = "Existe";
                                $notificacion->idticket = $this->ticket->idticket;
                                $notificacion->save();


                                echo "se guardo el historial";

                                

                                Mail::to($this->ticket->tecnicos->email)->send(new EstadosMail($sendMail));

                                //return redirect()->route('ticket.index')->with('success','Registro creado satisfactoriamente');
                            } else {
                                //return redirect()->route('ticket.index')->with('danger','Error: Intente Nuevamente');
                            }
                        } else {
                            //return redirect()->route('ticket.index')->with('danger','Error: Intente Nuevamente');
                        }
                    }
                } else if($auxB){ // SI HAY CLIENTE HACE ESTO

                    echo " <script> console.log('cliente existe');</script>";

                    // obtenemos el nomrbe y apellido del cliente
                    // $titulo = explode(":", $oMessage->getSubject());
                    //echo " <br> sub " . $titulo[1];
                    // verificamos si el cliente tiene un ticket creado
                    $verificar_ticket = ticket::all()->where('Cliente_idcliente', $this->verificar_cliente[0]->idcliente);

                    $mystring = $oMessage->getSubject();
                    // if (count($verificar_ticket) > 0) {
                    //     foreach ($verificar_ticket as $value) {
                    //         $findme   = $value->titulo;
                    //         $pos = strpos($mystring, $findme);
                    //         $pos2 = strpos("Re: ".$mystring, $findme);

                    //         if($pos2 === false && $pos === false && $bandera == 0) {
                    //             $this->ticket_id = 0;
                    //         } else {
                    //             $this->ticket_id = $value->idticket;
                    //             $bandera = 1;
                    //         }
                    //     }
                    // } else {
                    //     echo "no hay tickets (if si hay cliente)";
                    // }

                    $bandera = 0;

                    if (count($verificar_ticket) > 0) {
                        foreach ($verificar_ticket as $value) {
                            $findme   = $value->titulo;
                            $pos = strpos($mystring, $findme);
                            $pos2 = strpos("Re: ".$mystring,"Re: ".$findme);
    
                            if($pos2 === false && $pos === false && $bandera == 0) {
                                $this->ticket_id = 0;
                            } else {
                                $this->ticket_id = $value->idticket;
                                $bandera = 1;
                            }
                        }
                    } else {
                        echo "no hay tickets (if si hay cliente)";
                    }


                    print_r($this->ticket_id);

                    // Nótese el uso de ===. Puesto que == simple no funcionará como se espera
                    // porque la posición de 'a' está en el 1° (primer) caracter.
                    if ($this->ticket_id == 0) {
                        echo "La cadena no fue encontrada en la cadena (if si hay cliente)";

                        echo "<br> el cliente no tiene ticket ingresados (if si hay cliente)";
                        // luego de verificar si el cliente tiene ticket
                        // si no tiene
                        // creamos el numero de caso
                        $ultimo = ticket::orderby('created_at', 'DESC')->take(1)->get(['idticket']);

                        if ($ultimo->count()) {
                            if ($ultimo[0]->idticket < 10) {
                                $cero = "00000";
                            } else if ($ultimo[0]->idticket > 10) {
                                $cero = "0000";
                            } else if ($ultimo[0]->idticket > 100) {
                                $cero = "000";
                            } else if ($ultimo[0]->idticket > 1000) {
                                $cero = "00";
                            } else if ($ultimo[0]->idticket > 10000) {
                                $cero = "0";
                            } else {
                                $cero = "";
                            }
                            $numero = date('Ymd') . $cero . $ultimo[0]->idticket;
                        } else {
                            $cero = "000000";
                            $numero = date('Ymd') . $cero;
                        }

                        // buscamos la empresa para obtener la prioridad

                        $empresaID = $this->verificar_cliente[0]->idempresa;


                        $empresa = empresa::find($empresaID);

                        

                        if($empresa->idempresa == 1){
                            $tipo_ingreso = 2;
                        }else{

                            $tipo_ingreso = 1;
                        }

                        //asignamos el ticket al cliente
                        $tecnicos = usuario::whereIn('role', [3,4])->where('status',1)->orderBy('role', 'ASC')->first();

                        if (empty($tecnicos)) {
                            $tecnicoAsignado = 1;
                        }else{
                            $tecnicoAsignado = $tecnicos->id;
                        }

                        $this->ticket = new ticket();

                        $this->ticket->numeroDeCaso = $numero;
                        $this->ticket->PrioridadTicket_idPrioridadTicket = $empresa->tipoContrato->prioridadticket_idprioridadticket;
                        $this->ticket->tipoCierre_idtipoCierre = 1;
                        $this->ticket->Cliente_idcliente = $this->verificar_cliente[0]->idcliente;
                        $this->ticket->EstadoTicket_idEstadoTicket = 1;
                        $this->ticket->descripcion = $oMessage->getHTMLBody(true);
                        $this->ticket->titulo = $oMessage->getSubject();
                        $this->ticket->tecnico =  $tecnicoAsignado;
                        $this->ticket->idusuario = 1;
                        $this->ticket->comentario = "Ingresado automaticamente";
                        $this->ticket->tipo_ingreso = $tipo_ingreso;
                        $this->ticket->abierto = 0;
                        $this->ticket->idingreso = 2;
                        $id = $this->ticket->idticket;

                        if ($this->ticket->save()) {

                            self::validar($this->ticket->idticket);
                            if(isset($oMessage->cc)){
                                foreach ($oMessage->cc as $mail){

                                    if($mail->mail != "soportetickets2018@gmail.com"){
                                        // echo '<br>';
                                        // echo 'Asunto: '.$oMessage->getSubject();
                                        // echo '<br>';
                                        // echo 'To: ';
                                        // print_r($mail->mail);
                                        // echo '<br>';
                                        $usuarioMail = new UsuarioTicket;
                                        $usuarioMail->email = $mail->mail;
                                        $usuarioMail->id_ticket = $this->ticket->idticket;
                                        $usuarioMail->save();
                                    }
                                }
                            }
            
                            if(isset($oMessage->to)){
                                foreach ($oMessage->to as $mail){
                                    if($mail->mail != "soportetickets2018@gmail.com"){
                                        // echo '<br>';
                                        // echo 'Asunto: '.$oMessage->getSubject();
                                        // echo '<br>';
                                        // echo 'To: ';
                                        // print_r($mail->mail);
                                        // echo '<br>';
                                        $usuarioMail = new UsuarioTicket;
                                        $usuarioMail->email = $mail->mail;
                                        $usuarioMail->id_ticket = $this->ticket->idticket;
                                        $usuarioMail->save();
                                    }
            
            
            
                                }
            
                            }
                            // obtener el id de la plantilla segun el estado
                            $plantilla = estados::find(1);

                            $historico = new historico();
                            $historico->Ticket_idticket = $this->ticket->idticket;
                            $historico->descripcion = $this->ticket->descripcion;
                            $historico->tipo = 8;
                            $historico->usuario = 1;
                            //tipo usuario 2: cuando es agente y 1: cuando es cliente
                            $historico->tipo_usuario = 2;
                            $historico->estado_ticket = 1;
                            if ($historico->save()) {

                                if ($oMessage->getAttachments()->count() > 0) {
                                    $aAttachment = $oMessage->getAttachments();

                                    $aAttachment->each(function ($oAttachment) {
                                        /** @var \Webklex\IMAP\Attachment $oAttachment */
                                        if ($oAttachment->getType() == 'image') {
                                            $oAttachment->save(public_path('/attach'), $oAttachment->getName());
                                            $historico = new historico();
                                            $historico->Ticket_idticket = $this->ticket->idticket;
                                            $historico->descripcion = $oAttachment->getName();
                                            $historico->tipo = 13;
                                            $historico->usuario = $this->verificar_cliente[0]->idcliente;
                                            //tipo usuario 2: cuando es agente y 1: cuando es cliente
                                            $historico->tipo_usuario = 2;
                                            $historico->estado_ticket = 1;
                                            $historico->save();
                                        } else {
                                            $oAttachment->save(public_path('/attach'), $oAttachment->getName());
                                            $historico = new historico();
                                            $historico->Ticket_idticket = $this->ticket->idticket;
                                            $historico->descripcion = $oAttachment->getName();
                                            $historico->tipo = 14;
                                            $historico->usuario = $this->verificar_cliente[0]->idcliente;
                                            //tipo usuario 2: cuando es agente y 1: cuando es cliente
                                            $historico->tipo_usuario = 2;
                                            $historico->estado_ticket = 1;
                                            $historico->save();
                                        }
                                    });
                                }


                                $notificacion = new notificaciones();

                                $notificacion->titulo = "Nuevo Ticket " . $this->ticket->numeroDeCaso;
                                $notificacion->mensaje = "Existe";
                                $notificacion->idticket = $this->ticket->idticket;

                                $notificacion->save();


                                echo "se guardo el historial";

                                Mail::to($this->ticket->tecnicos->email)->send(new EstadosMail($sendMail));


                                echo "se guardo el historial (if si hay cliente)";

                                //return redirect()->route('ticket.index')->with('success','Registro creado satisfactoriamente');
                            } else {
                                //return redirect()->route('ticket.index')->with('danger','Error: Intente Nuevamente');
                            }
                        } else {
                            //return redirect()->route('ticket.index')->with('danger','Error: Intente Nuevamente');
                        }
                    } else {
                        echo " <script> console.log('entre aqui');</script>";
                        //echo " y existe en la posición " . $pos;




                        //si tiene un ticket creado verificamos que no este cerrado
                        // si no esta cerrado agergamos su respuesta
                        $historico = new historico();
                        $historico->Ticket_idticket = $this->ticket_id;
                        $historico->descripcion = $oMessage->getHTMLBody(true); //cuando el cliente manda una respuesta
                        $historico->tipo = 9;
                        $historico->usuario = 1;
                        //tipo 9 cuando es respuesta del usuario
                        $historico->tipo_usuario = 2;
                        $historico->estado_ticket = 1;
                        $historico->save();
                        $tk = ticket::findOrFail($this->ticket_id);
                        $nc = $tk->numeroDeCaso;

                        //notificacion de nueva respuesta en ticket epecifico al tecnico que se le ha asignado dicho
                        $notificacion = new notificaciones();
                        $notificacion->titulo = "Nueva respuesta en Ticket " . $nc;
                        $notificacion->mensaje = "Respuesta";
                        $notificacion->idticket = $this->ticket_id;
                        $notificacion->status = 2;

                        $notificacion->save();

                        //notificarle por correo al tecnico
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $nc;
                        $sendMail->tipo             = 24;
                        $sendMail->fecha            = date("F j, Y", strtotime($tk->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($tk->created_at));
                        $sendMail->descripcion      = "Tienes un nuevo mensaje del ticket numero " . $nc;
                        $sendMail->tecnico          = $tk->tecnicos->nombres;
                        $sendMail->prioridad        = $tk->prioridad->nombre;
                        $sendMail->estado           = $tk->estado->nombre;
                        $sendMail->titulo           = $tk->idticket;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = $tk->idticket;
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        $sendMail->subject          = $tk->titulo;
                        $sendMail->file = "";
                        $sendMail->pdf = "";



                        Mail::to($tk->tecnicos->email)->send(new EstadosMail($sendMail));

                        if($tk->comercial != 0){
                            Mail::to($tk->ejecutivo->email)->send(new EstadosMail($sendMail));
                        }
                       



                        if ($historico->save()) {

                            echo " <script> console.log('entre aqui2');</script>";

                            if ($oMessage->getAttachments()->count() > 0) {
                                $aAttachment = $oMessage->getAttachments();

                                $aAttachment->each(function ($oAttachment) {
                                    /** @var \Webklex\IMAP\Attachment $oAttachment */



                                    if ($oAttachment->getType() == 'image') {
                                        $oAttachment->save(public_path('/attach'), $oAttachment->getName());
                                        $historico = new historico();
                                        $historico->Ticket_idticket = $this->ticket_id;
                                        $historico->descripcion = $oAttachment->getName();
                                        $historico->tipo = 13;
                                        $historico->usuario = $this->verificar_cliente[0]->idcliente;
                                        //tipo usuario 2: cuando es agente y 1: cuando es cliente
                                        $historico->tipo_usuario = 2;
                                        $historico->estado_ticket = 1;
                                        $historico->save();
                                    } else {
                                        $oAttachment->save(public_path('/attach'), $oAttachment->getName());
                                        $historico = new historico();
                                        $historico->Ticket_idticket = $this->ticket_id;
                                        $historico->descripcion = $oAttachment->getName();
                                        $historico->tipo = 14;
                                        $historico->usuario = $this->verificar_cliente[0]->idcliente;
                                        //tipo usuario 2: cuando es agente y 1: cuando es cliente
                                        $historico->tipo_usuario = 2;
                                        $historico->estado_ticket = 1;
                                        $historico->save();
                                    }
                                });
                            }

                            echo "guardo el historial con la respuesta del cliente";
                        }
                    }
                }

                // //echo 'Attachments: '.$oMessage->getAttachments()->count().'<br />';
                // //echo $oMessage->getHTMLBody(true);

                // //Move the current Message to 'INBOX.read'
                // // if($oMessage->moveToFolder('Vistos') == true){
                // //     echo 'Message has ben moved';
                // // }else{
                // //     echo 'Message could not be moved';
                // // }
            }
        }
    }

    // funcion para enviar correos
    public function enviar()
    {
        // Enviar correo de creacioón de ticket
        $sendMail = new \stdClass();
        $sendMail->numeroDeCaso     = "numero";
        $sendMail->tipo             = 1;
        $sendMail->fecha            = "";
        $sendMail->hora             = "";
        $sendMail->descripcion      = "";
        $sendMail->tecnico          = "";
        $sendMail->prioridad        = "";
        $sendMail->estado           = "";
        $sendMail->titulo           = "";
        $sendMail->moderador        = "";
        $sendMail->nota             = "";
        $sendMail->tecnico_anterior = "";
        $sendMail->respuesta        = "";
        $sendMail->subject        = "envio";

        Mail::to("andrade209701@gmail.com")->send(new EstadosMail($sendMail));
    }

    // funcion para enviar correos
    public function send1()
    {

        echo "entre";


        $oClient = new Client([
            'host'          => 'imap.gmail.com',
            'port'          => 993,
            'encryption'    => 'ssl',
            'validate_cert' => true,
            'username'      => 'soportetickets2018@gmail.com',
            'password'      => '10010110',
            'protocol'      => 'imap'
        ]);
        /* Alternative by using the Facade
        $oClient = Webklex\IMAP\Facades\Client::account('default');
        */

        //Connect to the IMAP Server
        $oClient->connect();

        //Get all Mailboxes
        /** @var \Webklex\IMAP\Support\FolderCollection $aFolder */
        $aFolder = $oClient->getFolders();

        //Loop through every Mailbox
        /** @var \Webklex\IMAP\Folder $oFolder */
        foreach ($aFolder as $oFolder) {

            //Get all Messages of the current Mailbox $oFolder
            /** @var \Webklex\IMAP\Support\MessageCollection $aMessage */
            $aMessage = $oFolder->messages()->unseen()->all()->get();



            /** @var \Webklex\IMAP\Message $oMessage */
            foreach ($aMessage as $oMessage) {


                // verificamos si el cliente tiene un ticket creado
                $verificar_ticket = UsuarioTicket::select('*')->join('ticket', 'ticket.idticket', '=', 'usuario_ticket.id_ticket')->get();

                $mystring = $oMessage->getSubject();

                $bandera = 0;

                if (count($verificar_ticket) > 0) {
                    foreach ($verificar_ticket as $value) {
                        $findme   = $value->titulo;
                        $pos = strpos($mystring, $findme);
                        $pos2 = strpos("Re: ".$mystring,"Re: ".$findme);

                        if ($pos2 === false && $pos === false) {
                            echo "no ha ticket";
                        } else {
                            echo '<br/>';
                           echo "Si hay ticket";
                           echo '<br/>';
                           echo $mystring;
                           echo '<br/>';
                           echo $value->titulo;
                        }
                    }
                } else {
                    echo "no hay tickets (if si hay cliente)";
                }
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $ultimo = ticket::orderby('created_at', 'DESC')->take(1)->get(['idticket']);

        if ($ultimo->count()) {
            if ($ultimo[0]->idticket < 10) {
                $cero = "00000";
            } else if ($ultimo[0]->idticket > 10) {
                $cero = "0000";
            } else if ($ultimo[0]->idticket > 100) {
                $cero = "000";
            } else if ($ultimo[0]->idticket > 1000) {
                $cero = "00";
            } else if ($ultimo[0]->idticket > 10000) {
                $cero = "0";
            } else {
                $cero = "";
            }
            $numero = date('Ymd') . $cero . $ultimo[0]->idticket;
        } else {
            $cero = "000000";
            $numero = date('Ymd') . $cero;
        }

        //traer empresas
        $empresas = empresa::where('estado', 1)->where('idempresa','<>',1)->get(['idempresa', 'nombre']);
        //traer prioridades
        $prioridades = prioridad::all(['idPrioridadTicket', 'nombre']);
        //traer los tipos de ingreso
        $ingresos = Ingreso::all(['idingreso', 'nombre']);
        //return $ultimo;
        return view('Ticket.create')->with('numero', $numero)->with('empresas', $empresas)->with('prioridades', $prioridades)->with('ingresos', $ingresos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(validateTicketCreate $request)
    {


        $empresa = empresa::find($request->idempresa);


        $ticket = new ticket();


                                //asignamos el ticket al cliente
        $tecnicos = usuario::whereIn('role', [3,4])->where('status',1)->orderBy('role', 'ASC')->first();

    

        if (empty($tecnicos)) {

            $tecnicoTicket = 1;

        }else{

            if(auth()->user()->role == 3 || auth()->user()->role == 4){
                $tecnicoTicket = auth()->user()->id;
            }else{

                $tecnicoTicket = $tecnicos->id;
                
            }   

        }



        $ticket->numeroDeCaso = $request->numeroDeCaso;
        // si el check viene seleccionado
        if ($request->cambio == 1) {
            $ticket->PrioridadTicket_idPrioridadTicket = $request->prioridad;
        } else {
            $ticket->PrioridadTicket_idPrioridadTicket = $empresa->tipoContrato->prioridadticket_idprioridadticket;
        }


        $ticket->tipoCierre_idtipoCierre = 1;
        $ticket->Cliente_idcliente = $request->Cliente_idcliente;
        $ticket->EstadoTicket_idEstadoTicket = 1;
        $ticket->descripcion = $request->descripcion;
        $ticket->titulo = $request->titulo;
        $ticket->tecnico = $tecnicoTicket;
        $ticket->idusuario = auth()->user()->id;
        $ticket->comentario = $request->comentario;
        $ticket->tipo_ingreso = 1;
        $ticket->abierto = 1;
        $ticket->idingreso = $request->idingreso;
        $ticket->inicioTiempo = DB::raw('CURRENT_TIMESTAMP');


        // instancia para guardar la progracion del ticket
        if ($request->programar == 1 || $request->programar == 2) {
            $ticket->programado = 1;
        }

        if ($ticket->save()) {

            if ($ticket->programado == 1) { // instancia para guardar la progracion del ticket
                if ($request->programar == 1) {
                    $programar = new programacion();
                    $programar->idticket    = $ticket->idticket;
                    $programar->tiempo      = 1;
                    $programar->descripcion = "Se programo un ticket para este dia, Ticket: " . $ticket->numeroDeCaso;
                    $programar->fecha_programacion = $request->fechai;
                    $programar->save();
                } else {
                    $dia = 24;
                    $semana = 168;
                    $quincena = 360;
                    $mes = 720;
                    $semestre = 1440;
                    $anual = 8760;
                    $diaInicial = strtotime($request->FecInicio);
                    $diaFinal = strtotime($request->FecFin);
                    $resta = round(($diaFinal - $diaInicial) / 3600);
                    // dd($resta);
                    $programar = new programacion();
                    $programar->idticket    = $ticket->idticket;
                    $programar->tiempo      = $request->tiempo;
                    $programar->fechaIni    = $request->FecInicio;
                    $programar->fechaFin    = $request->FecFin;
                    $programar->descripcion = "Nuevo Ticket " . $ticket->numeroDeCaso;
                    $programar->fecha_programacion = $request->fechai;
                    switch ($request->tiempo) {
                        case 1:
                            $cantidad = round($resta / $dia);
                            $programar->conteoNotificacion = $cantidad;
                            break;
                        case 2:
                            $cantidad = round($resta / $semana);
                            $programar->conteoNotificacion = $cantidad;
                            break;
                        case 3:
                            $cantidad = round($resta / $quincena);
                            $programar->conteoNotificacion = $cantidad;
                            break;
                        case 4:
                            $cantidad = round($resta / $mes);
                            $programar->conteoNotificacion = $cantidad;
                            break;
                        case 5:
                            $cantidad = round($resta / $semestre);
                            $programar->conteoNotificacion = $cantidad;
                            break;
                        case 6:
                            $cantidad = round($resta / $anual);
                            $programar->conteoNotificacion = $cantidad;
                            break;
                        default:
                            echo "no entro en ninguno";
                            break;
                    }
                    $programar->save();
                }

                if ($request->programar != 2) {
                    // instancia para guardar la notificacion
                    $notificacion = new notificaciones();

                    $notificacion->titulo = "Nuevo Ticket " . $ticket->numeroDeCaso;
                    $notificacion->mensaje = "Existe";
                    $notificacion->idticket = $ticket->idticket;
                    $notificacion->save();
                }
            }


            // obtener el id de la plantilla segun el estado
            $plantilla = estados::find(1);

            $historico = new historico();
            $historico->Ticket_idticket = $ticket->idticket;
            $historico->descripcion = $ticket->descripcion;
            $historico->tipo = 1;
            $historico->usuario = auth()->user()->id;
            //tipo usuario 2: cuando es agente y 1: cuando es cliente
            $historico->tipo_usuario = 1;
            $historico->estado_ticket = 1;
            if ($historico->save()) {

                //verificamos si viene algun archivo
                if (!empty($request->file)) {

                    $file = $request->file;

                    if ($file->move("files", $file->getClientOriginalName())) {
                        $historico = new historico();
                        $historico->Ticket_idticket = $ticket->idticket;
                        $historico->descripcion = $file->getClientOriginalName();
                        $historico->tipo = 11;
                        $historico->usuario = auth()->user()->id;
                        //tipo 11 cuando es adjunto
                        $historico->tipo_usuario = 1;
                        $historico->estado_ticket = 1;
                        $historico->save();
                    }
                }

                //verificamos si viene algun archivo
                if (!empty($request->pdf)) {

                    $pdf = $request->pdf;

                    if ($pdf->move("files", $pdf->getClientOriginalName())) {
                        $historico = new historico();
                        $historico->Ticket_idticket = $ticket->idticket;
                        $historico->descripcion = $pdf->getClientOriginalName();
                        $historico->tipo = 12;
                        $historico->usuario = auth()->user()->id;
                        //tipo 11 cuando es adjunto PDF
                        $historico->tipo_usuario = 1;
                        $historico->estado_ticket = 1;
                        $historico->save();
                    }
                }

                echo "se guardo el historial";

                if ($request->programar != 1 || $request->programar != 2) {

                    // Enviar correo de creacioón de ticket
                    $sendMail = new \stdClass();
                    $sendMail->numeroDeCaso     = $ticket->numeroDeCaso;
                    $sendMail->tipo             = $plantilla->idplantilla;
                    $sendMail->fecha            = date("F j, Y", strtotime($ticket->created_at));
                    $sendMail->hora             = date("H:i:s", strtotime($ticket->created_at));
                    $sendMail->descripcion      = $ticket->descripcion;
                    $sendMail->tecnico          = $ticket->tecnicos->nombres;
                    $sendMail->prioridad        = $ticket->prioridad->nombre;
                    $sendMail->estado           = $ticket->estado->nombre;
                    $sendMail->titulo           = $ticket->titulo;
                    $sendMail->moderador        = "";
                    $sendMail->nota             = "";
                    $sendMail->tecnico_anterior = "";
                    $sendMail->respuesta        = "";
                    $sendMail->subject          = $ticket->titulo;
                    if (!empty($request->file)) {

                        $file = $request->file;

                        $sendMail->file = $file->getClientOriginalName();
                    } else {
                        $sendMail->file = "";
                    }

                    if (!empty($request->pdf)) {

                        $pdf = $request->pdf;

                        $sendMail->pdf = $pdf->getClientOriginalName();
                    } else {
                        $sendMail->pdf = "";
                    }

                    Mail::to($ticket->cliente->email)->send(new EstadosMail($sendMail));

                    // Enviar correo de creacioón de ticket
                    $sendMail = new \stdClass();
                    $sendMail->numeroDeCaso     = $ticket->numeroDeCaso;
                    $sendMail->tipo             = 3;
                    $sendMail->fecha            = date("F j, Y", strtotime($ticket->created_at));
                    $sendMail->hora             = date("H:i:s", strtotime($ticket->created_at));
                    $sendMail->descripcion      = $ticket->descripcion;
                    $sendMail->tecnico          = $ticket->tecnicos->nombres;
                    $sendMail->prioridad        = $ticket->prioridad->nombre;
                    $sendMail->estado           = $ticket->estado->nombre;
                    $sendMail->titulo           = $ticket->titulo;
                    $sendMail->moderador        = "";
                    $sendMail->nota             = "";
                    $sendMail->tecnico_anterior = "";
                    $sendMail->respuesta        = "";
                    $sendMail->subject          = $ticket->titulo;
                    if (!empty($request->file)) {

                        $file = $request->file;

                        $sendMail->file = $file->getClientOriginalName();
                    } else {
                        $sendMail->file = "";
                    }

                    if (!empty($request->pdf)) {

                        $pdf = $request->pdf;

                        $sendMail->pdf = $pdf->getClientOriginalName();
                    } else {
                        $sendMail->pdf = "";
                    }

                    Mail::to($ticket->tecnicos->email)->send(new EstadosMail($sendMail));

                    if($ticket->comercial != 0 ){

                        $sendMail->tecnico          = $ticket->ejecutivo->nombres;
                        Mail::to($ticket->ejecutivo->email)->send(new EstadosMail($sendMail));

                    }
                    
                }

                return redirect()->route('ticket.create')->with('success', 'Registro creado satisfactoriamente');
            } else {
                return redirect()->route('ticket.create')->with('danger', 'Error: Intente Nuevamente');
            }
        } else {
            return redirect()->route('ticket.create')->with('danger', 'Error: Intente Nuevamente');
        }

        //ticket::create($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function eliminarTicket($id)
    {
        $ticket = ticket::findOrFail($id);
        $historial = historico::where('Ticket_idticket', $id);
        if ($historial->delete()) {
            $ticket->delete();
        }
        return redirect('/mails')->with('success', "Se ha eliminado el ticket ");
    }



    public function updateHistorico(Request $request)
    {

        $sizeFILE = Validator::make($request->all(), [
            'file'              => 'max:250000'
        ]);

        if ($sizeFILE->fails()) {
            $request->session()->flash('alert-danger', 'El archivo no debe exceder los 25MB de peso');
            return redirect('/ticket/' . $request->idticket);
        }

        $sizePDF = Validator::make($request->all(), [
            'pdf'               => 'max:250000'
        ]);

        if ($sizePDF->fails()) {
            $request->session()->flash('alert-danger', 'El archivo no debe exceder los 25MB de peso.');
            return redirect('/ticket/' . $request->idticket);
        }

        // $validator = Validator::make($request->all(), [
        //     'file'              => 'mimes:jpeg,jpg,bmp,png,avi,mp4,3gp,mpeg'
        // ]);

        // if ($validator->fails()) {
        //     $request->session()->flash('alert-danger', 'El archivo que selecciono no es valido');
        //     return redirect('/ticket/' . $request->idticket);
        // }

        // $validator2 = Validator::make($request->all(), [
        //     'pdf'               => 'mimes:pdf,docx,pptx,xlsx,csv,doc,txt'
        // ]);

        // if ($validator2->fails()) {
        //     $request->session()->flash('alert-danger', 'El archivo que selecciono no es valido.');
        //     return redirect('/ticket/' . $request->idticket);
        // }

        // 6- si se cierre
        if ($request->idestado == 5) {

            $validator = Validator::make($request->all(), [
                'respuesta'        => 'required|string|max:255'
            ]);

            if ($validator->fails()) {
                $request->session()->flash('alert-danger', 'Debe ingresar el mensaje de resolucion');
                return redirect('/ticket/' . $request->idticket);
            }
        }

        $updateTicket = ticket::find($request->idticket);
        $updateTicket->EstadoTicket_idEstadoTicket = $request->idestado;
        $updateTicket->PrioridadTicket_idPrioridadTicket = $request->prioridad;
        $updateTicket->archivado = 0;

        if($request->horas != $updateTicket->horas){

            $updateTicket->horas=$request->horas;

        }

        if (isset($request->tecnico)) {
            $updateTicket->tecnico = $request->tecnico;
        }

        if ($request->ejecutivo != 0) {
            $updateTicket->comercial = $request->ejecutivo;
        }

        if ($updateTicket->save()) {


            if($request->correos != ""){
                $direcciones = array();

                $mails = UsuarioTicket::select('email')->where('id_ticket',$request->idticket)->get();

                foreach($mails as $mail){
                    
                    array_push($direcciones,$mail->email);
                    
                }

                $correos = str_replace(' ', '',$request->correos);
                $correos = explode( ',', $correos);

                foreach($correos as $correo){
                    
                    if(!in_array( $correo, $direcciones)){

                    $usuarioMail = new UsuarioTicket;            
                    $usuarioMail->email = $correo;
                    $usuarioMail->id_ticket = $request->idticket;
                    $usuarioMail->save();

                    }
                    


                }


            }



            // tipo 1: cambio de estado
            // tipo 2: respuesta agente
            // tipo 3: cambio de prioridad
            // tipo 4: cambio de tecnico
            // tipo 5: respuesta cliente

            //verificamos si viene algun archivo
            if (!empty($request->file)  ) {

                $file = $request->file;

                if ($file->move("files", $file->getClientOriginalName())) {
                    $historico = new historico();
                    $historico->Ticket_idticket = $request->idticket;
                    $historico->descripcion = $file->getClientOriginalName();
                    $historico->tipo = 11;
                    $historico->usuario = auth()->user()->id;
                    //tipo 11 cuando es adjunto
                    $historico->tipo_usuario = 1;
                    $historico->estado_ticket = $request->idestado;
                    $historico->save();
                    $sendMail = new \stdClass();
                    $sendMail->subject          = $updateTicket->titulo;
                    $sendMail->tipo             = 32;

                    if(empty($request->respuesta) && $request->action == "todos" ){
                        $sendMail->file = $file->getClientOriginalName();
                        Mail::to($updateTicket->cliente->email)->send(new EstadosMail($sendMail));
                    }

                    if(empty($request->respuesta) && $request->action == "interno"){
                        $sendMail->file = $file->getClientOriginalName();
                        Mail::to($updateTicket->tecnicos->email)->send(new EstadosMail($sendMail));


                        if($updateTicket->comercial !=0 && $updateTicket->comercial != auth()->user()->id){
                            Mail::to($updateTicket->ejecutivo->email)->send(new EstadosMail($sendMail));
                        }


                    }

                }
            }

            

            //Si hay respuesta
            if (!empty($request->respuesta) && $request->idestado == $request->estado_actual && $request->idestado != 5  ) {

                $correos = UsuarioTicket::where('id_ticket',$request->idticket)->get();

                                 // Enviar correo de creacioón de ticket
                $sendMail = new \stdClass();
                $sendMail->numeroDeCaso     = $updateTicket->numeroDeCaso;
                $sendMail->tipo             = 13;
                $sendMail->fecha            = date("F j, Y", strtotime($updateTicket->created_at));
                $sendMail->hora             = date("H:i:s", strtotime($updateTicket->created_at));
                $sendMail->descripcion      = $updateTicket->descripcion;
                $sendMail->tecnico          = $updateTicket->tecnicos->nombres;
                $sendMail->prioridad        = $updateTicket->prioridad->nombre;
                $sendMail->estado           = $updateTicket->estado->nombre;
                $sendMail->titulo           = $updateTicket->titulo;
                $sendMail->moderador        = "";
                $sendMail->nota             = $updateTicket->idticket;
                $sendMail->tecnico_anterior = "";
                $sendMail->respuesta_cliente = $request->respuesta;
                $sendMail->respuesta        = $request->respuesta;
                $sendMail->subject          = $updateTicket->titulo;
                
                if (!empty($request->file)) {

                    $file = $request->file;

                    $sendMail->file = $file->getClientOriginalName();
                } else {
                    $sendMail->file = "";
                }

                if (!empty($request->pdf)) {

                    $pdf = $request->pdf;

                    $sendMail->pdf = $pdf->getClientOriginalName();
                } else {
                    $sendMail->pdf = "";
                }


                if(count($correos) > 0){

                    foreach ($correos as $correo) {

                        Mail::to($correo->email)->send(new EstadosMail($sendMail));

                    }
                }


                //enviar correo a todos los involucrados
               if($request->action == "todos"){ 
                
                $historico = new historico();
                $historico->Ticket_idticket = $request->idticket;
                $historico->descripcion = $request->respuesta;
                $historico->tipo = 2;
                $historico->usuario = auth()->user()->id;
                //tipo usuario 2: cuando es agente y 1: cuando es cliente
                $historico->tipo_usuario = 1;
                $historico->estado_ticket = $request->idestado;
                $historico->save(); 
                
               
                

                Mail::to($updateTicket->cliente->email)->send(new EstadosMail($sendMail));

                $sendMail->tipo= 24;
                
                if($updateTicket->comercial !=0 && $updateTicket->comercial != auth()->user()->id){
                    

                    Mail::to($updateTicket->ejecutivo->email)->send(new EstadosMail($sendMail));





                }

                if($updateTicket->tecnico !=0 && $updateTicket->tecnico != auth()->user()->id){

                    Mail::to($updateTicket->tecnicos->email)->send(new EstadosMail($sendMail));

                }


            

            }else{//enviar correo a interno

                
                $sendMail->tipo= 24;

                $historico = new historico();
                $historico->Ticket_idticket = $request->idticket;
                $historico->descripcion = $request->respuesta;
                $historico->tipo = 2;
                $historico->usuario = auth()->user()->id;
                $historico->interno = 1;
                //tipo usuario 2: cuando es agente y 1: cuando es cliente
                $historico->tipo_usuario = 1;
                $historico->estado_ticket = $request->idestado;
                $historico->save(); 
                

                if($updateTicket->tecnico !=0 && $updateTicket->tecnico != auth()->user()->id){

                   
                    $sendMail->usuarioTipo="Ejecutivo";

                    Mail::to($updateTicket->tecnicos->email)->send(new EstadosMail($sendMail));


                    $notificacion = new notificaciones();
                    $notificacion->titulo = "Nuevo mensaje interno - ticket " . $updateTicket->numeroDeCaso;
                    $notificacion->mensaje = "interno";
                    $notificacion->idticket = $updateTicket->idticket;
                    $notificacion->status = 2;
                    $notificacion->save();


                }
                
                if($updateTicket->comercial !=0 && $updateTicket->comercial != auth()->user()->id){

                    $sendMail->usuarioTipo="Tecnico";
                    Mail::to($updateTicket->ejecutivo->email)->send(new EstadosMail($sendMail));


                    $notificacion = new notificaciones();
                    $notificacion->titulo = "Nuevo mensaje interno - ticket " . $updateTicket->numeroDeCaso;
                    $notificacion->mensaje = "interno-";
                    $notificacion->idticket = $updateTicket->idticket;
                    $notificacion->status = 2;
                    $notificacion->save();

                }


            }


                



            }
            // 1-si es cambio de estado
            if ($request->estado_actual != $request->idestado) {
                $historico = new historico();
                $historico->Ticket_idticket = $request->idticket;
                $historico->descripcion = "";
                $historico->tipo = 1;
                $historico->usuario = auth()->user()->id;
                //tipo usuario 2: cuando es agente y 1: cuando es cliente
                $historico->tipo_usuario = 1;
                $historico->estado_ticket = $request->idestado;
                $historico->save();

                // 6- si se cierre
                if ($request->idestado == 5) {
                    $historico = new historico();
                    $historico->Ticket_idticket = $request->idticket;
                    $historico->descripcion = "";
                    $historico->tipo = 6;
                    $historico->usuario = auth()->user()->id;
                    //tipo usuario 2: cuando es agente y 1: cuando es cliente
                    $historico->tipo_usuario = 1;
                    $historico->estado_ticket = $request->idcierre;
                    $historico->save();
                }

                // 6- si se cierre
                if ($request->idestado == 6) {
                    $historico = new historico();
                    $historico->Ticket_idticket = $request->idticket;
                    $historico->descripcion = "";
                    $historico->tipo = 7;
                    $historico->usuario = auth()->user()->id;
                    //tipo usuario 2: cuando es agente y 1: cuando es cliente
                    $historico->tipo_usuario = 1;
                    $historico->estado_ticket = $request->idestado;
                    $historico->save();
                }

                $plantilla = estados::find($request->idestado);

                // Enviar correo de creacioón de ticket
                $sendMail = new \stdClass();
                $sendMail->numeroDeCaso     = $updateTicket->numeroDeCaso;
                $sendMail->tipo             = $plantilla->idplantilla;
                $sendMail->fecha            = date("F j, Y", strtotime($updateTicket->created_at));
                $sendMail->hora             = date("H:i:s", strtotime($updateTicket->created_at));
                $sendMail->descripcion      = $updateTicket->descripcion;
                $sendMail->tecnico          = $updateTicket->tecnicos->nombres;
                $sendMail->prioridad        = $updateTicket->prioridad->nombre;
                $sendMail->estado           = $updateTicket->estado->nombre;
                $sendMail->titulo           = $updateTicket->titulo;
                $sendMail->moderador        = "";
                $sendMail->nota             = "";
                $sendMail->tecnico_anterior = "";
                $sendMail->respuesta_cliente = "";
                $sendMail->respuesta        = $request->respuesta;
                $sendMail->subject          = $updateTicket->titulo;
                if (!empty($request->file)) {

                    $file = $request->file;

                    $sendMail->file = $file->getClientOriginalName();
                } else {
                    $sendMail->file = "";
                }

                if (!empty($request->pdf)) {

                    $pdf = $request->pdf;

                    $sendMail->pdf = $pdf->getClientOriginalName();
                } else {
                    $sendMail->pdf = "";
                }

                Mail::to($updateTicket->cliente->email)->send(new EstadosMail($sendMail));

                Mail::to($updateTicket->tecnicos->email)->send(new EstadosMail($sendMail));
            }
            // 3 si es cambio de prioridad
            if ($request->prioridad_actual != $request->prioridad) {
                $historico = new historico();
                $historico->Ticket_idticket = $request->idticket;
                $historico->descripcion = "";
                $historico->tipo = 3;
                $historico->usuario = auth()->user()->id;
                //tipo usuario 2: cuando es agente y 1: cuando es cliente
                $historico->tipo_usuario = 1;
                $historico->estado_ticket = $request->idestado;
                $historico->save();
            }
            // 4- si es cambio de tecnico
            if ($request->tecnico_actual != $request->tecnico) {
                $historico = new historico();
                $historico->Ticket_idticket = $request->idticket;
                $historico->descripcion = "";
                $historico->tipo = 4;
                $historico->usuario = $request->tecnico;
                //tipo usuario 2: cuando es agente y 1: cuando es cliente
                $historico->tipo_usuario = 1;
                $historico->created_by = Auth::user()->id;
                $historico->estado_ticket = $request->idestado;
                $historico->save();

                $tecnico_actual = usuario::find($request->tecnico_actual);
                
                // Enviar correo de creacioón de ticket
                $sendMail = new \stdClass();
                $sendMail->numeroDeCaso     = $updateTicket->numeroDeCaso;
                $sendMail->tipo             = 4;
                $sendMail->fecha            = date("F j, Y", strtotime($updateTicket->created_at));
                $sendMail->hora             = date("H:i:s", strtotime($updateTicket->created_at));
                $sendMail->descripcion      = $updateTicket->descripcion;
                $sendMail->tecnico          = $updateTicket->tecnicos->nombres;
                $sendMail->prioridad        = $updateTicket->prioridad->nombre;
                $sendMail->estado           = $updateTicket->estado->nombre;
                $sendMail->titulo           = $updateTicket->titulo;
                $sendMail->moderador        = "";
                $sendMail->nota             = "";
                $sendMail->tecnico_anterior = $tecnico_actual->nombres;
                $sendMail->respuesta        = "";
                $sendMail->subject          = $updateTicket->titulo;
                if (!empty($request->file)) {
                    $file = $request->file;
                    $sendMail->file = $file->getClientOriginalName();
                } else {
                    $sendMail->file = "";
                }
                
                if (!empty($request->pdf)) {

                    $pdf = $request->pdf;

                    $sendMail->pdf = $pdf->getClientOriginalName();
                } else {
                    $sendMail->pdf = "";
                }



                Mail::to($updateTicket->tecnicos->email)->send(new EstadosMail($sendMail));


                //notificacion de que se le ha asignado un ticket
                $notificacion = new notificaciones();

                $notificacion->titulo = "Se te ha asignado el ticket " . $updateTicket->numeroDeCaso;
                $notificacion->mensaje = "Asignacion";
                $notificacion->idticket = $updateTicket->idticket;
                $notificacion->status = 2;
                $notificacion->save();
            }

            //tipo 10 - si es cambio de ejecutivo
            if ($request->ejecutivo_actual != $request->ejecutivo) {
                $historico = new historico();
                $historico->Ticket_idticket = $request->idticket;
                $historico->descripcion = "";
                $historico->tipo = 20;
                $historico->usuario = $request->tecnico;
                //tipo usuario 2: cuando es agente y 1: cuando es cliente
                $historico->tipo_usuario = 1;
                $historico->created_by = Auth::user()->id;
                $historico->estado_ticket = $request->idestado;
                $historico->save();

                // Enviar correo de creacioón de ticket
                $sendMail = new \stdClass();
                $sendMail->numeroDeCaso     = $updateTicket->numeroDeCaso;
                $sendMail->tipo             = 3;
                $sendMail->fecha            = date("F j, Y", strtotime($updateTicket->created_at));
                $sendMail->hora             = date("H:i:s", strtotime($updateTicket->created_at));
                $sendMail->descripcion      = $updateTicket->descripcion;
                $sendMail->tecnico          = $updateTicket->ejecutivo->nombres;
                $sendMail->prioridad        = $updateTicket->prioridad->nombre;
                $sendMail->estado           = $updateTicket->estado->nombre;
                $sendMail->titulo           = $updateTicket->titulo;
                $sendMail->moderador        = "";
                $sendMail->nota             = "";
                $sendMail->tecnico_anterior = $request->tecnico_actual;
                $sendMail->respuesta        = "";
                $sendMail->subject          = $updateTicket->titulo;
                if (!empty($request->file)) {

                    $file = $request->file;

                    $sendMail->file = $file->getClientOriginalName();
                } else {
                    $sendMail->file = "";
                }

                if (!empty($request->pdf)) {

                    $pdf = $request->pdf;

                    $sendMail->pdf = $pdf->getClientOriginalName();
                } else {
                    $sendMail->pdf = "";
                }



                Mail::to($updateTicket->ejecutivo->email)->send(new EstadosMail($sendMail));                

                //notificacion de que se le ha asignado un ticket
                // $notificacion = new notificaciones();

                // $notificacion->titulo = "Se te ha asignado el ticket " . $updateTicket->numeroDeCaso;
                // $notificacion->mensaje = "Asignacion";
                // $notificacion->idticket = $updateTicket->idticket;
                // $notificacion->status = 2;
                // $notificacion->save();

            }
            // 5- si es comentario
            if (!empty($request->comentario)) {
                $historico = new historico();
                $historico->Ticket_idticket = $request->idticket;
                $historico->descripcion = $request->comentario;
                $historico->tipo = 5;
                $historico->usuario = auth()->user()->id;
                //tipo usuario 2: cuando es agente y 1: cuando es cliente
                $historico->tipo_usuario = 1;
                $historico->estado_ticket = $request->idestado;
                $historico->save();
            }

            echo "se guardo el historial";
            return redirect('/ticket/' . $request->idticket . '#coment');
        }
        return redirect()->route('ticket.show')->with('danger', 'Error: Intente Nuevamente');
    }

    public function updateTiempo($id, $tiempo)
    {

        $historico = new historico();
        $historico->Ticket_idticket = $id;
        $historico->descripcion = $tiempo;
        $historico->tipo = 10;
        $historico->usuario = auth()->user()->id;
        //tipo 10 cuando es cronometro
        $historico->tipo_usuario = 1;
        $historico->estado_ticket = 1;
        if ($historico->save()) {
            echo "Guardo historial";
        } else {
            echo "no guardo";
        }
    }

    public function insertarTiempoInicio($id)
    {


        $ticket = ticket::find($id);


        if ($ticket->conteoInicio == 0) {
            echo $ticket->conteoInicio;
            $ticket->conteoInicio = 1;
            $ticket->inicioTiempo = DB::raw('CURRENT_TIMESTAMP');

            if ($ticket->save()) {
                echo "Guardo estdo de trabajo ticket1";
            } else {
                echo "no guardo estdo de trabajo ticket";
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */






    public function edit($id)
    {
        //
        $ticket = ticket::find($id);
        return view('Ticket.edit', compact('ticket'));
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['nombres' => 'required', 'apellidos' => 'required', 'email' => 'required', 'telefono' => 'required']);

        ticket::find($id)->update($request->all());
        return redirect()->route('ticket.index')->with('success', 'Registro actualizado satisfactoriamente');
    }

    public function destroy($id)
    {
        //
        $ticket = ticket::find($id);

        $ticket->archivado = 1;

        $ticket->save();
        return redirect()->route('ticket.index')->with('success', 'Registro eliminado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validar($id)
    {
        
        //
        $ticket = ticket::find($id);
        
        $tipo = 1;

        if($ticket->tipo_ingreso==1){            
            $tipo = 2;
        }

        $ticket->tipo_ingreso = 1;

        $tecnicos = usuario::whereIn('role', [3,4])->where('status',1)->orderBy('role', 'ASC')->first();

        if (empty($tecnicos)) {
            $tecnicoTicket = 1;
        }else{
            if(auth()->user()->role == 3 || auth()->user()->role == 4){
                $tecnicoTicket = auth()->user()->id;
            }else{
                $tecnicoTicket = $tecnicos->id;
            }
        }

        $ticket->tecnico = $tecnicoTicket;
        $ticket->save();
        // Enviar correo de creacioón de ticket
        $sendMail = new \stdClass();
        $sendMail->numeroDeCaso     = $ticket->numeroDeCaso;
        $sendMail->tipo             = $tipo;
        $sendMail->fecha            = date("F j, Y", strtotime($ticket->created_at));
        $sendMail->hora             = date("H:i:s", strtotime($ticket->created_at));
        $sendMail->descripcion      = $ticket->descripcion;
        $sendMail->tecnico          = $ticket->tecnicos->nombres;
        $sendMail->prioridad        = $ticket->prioridad->nombre;
        $sendMail->estado           = $ticket->estado->nombre;
        $sendMail->titulo           = $ticket->titulo;
        $sendMail->moderador        = "";
        $sendMail->nota             = "";
        $sendMail->tecnico_anterior = "";
        $sendMail->respuesta        = "";
        $sendMail->subject          = $ticket->titulo;

        Mail::to($ticket->cliente->email)->send(new EstadosMail($sendMail));

        // Enviar correo de creacioón de ticket
        $sendMail = new \stdClass();
        $sendMail->numeroDeCaso     = $ticket->numeroDeCaso;
        $sendMail->tipo             = 3;
        $sendMail->fecha            = date("F j, Y", strtotime($ticket->created_at));
        $sendMail->hora             = date("H:i:s", strtotime($ticket->created_at));
        $sendMail->descripcion      = $ticket->descripcion;
        $sendMail->tecnico          = $ticket->tecnicos->nombres;
        $sendMail->prioridad        = $ticket->prioridad->nombre;
        $sendMail->estado           = $ticket->estado->nombre;
        $sendMail->titulo           = $ticket->titulo;
        $sendMail->moderador        = "";
        $sendMail->nota             = "";
        $sendMail->tecnico_anterior = "";
        $sendMail->respuesta        = "";
        $sendMail->subject          = $ticket->titulo;

        Mail::to($ticket->tecnicos->email)->send(new EstadosMail($sendMail));

        return redirect('mails')->with('success', 'Su Ticket fue validado');
    }


    public function show($id)
    {

        $tickets            = ticket::find($id);
       if( $tickets->abierto != 1){ 

        // if($tickets->tipo_ingreso != 2){
        //     self::validar($id);
        // }
        $tickets->abierto   = 1;
        // verificar status del tecnico
        $is_active = usuario::where('id', $tickets->tecnico)->first();

        if ($is_active->status != 1) {
            $tickets->tecnico = 1;
        }

        $tickets->save();
    
        }

        $tecnicos = usuario::where('status', 1)->whereIn('role', [3,4,2])->get(['id', 'nombres', 'apellidos']);
        if (!count($tecnicos)) {
            return redirect()->back()->with('danger', 'No hay Tecnicos activos');
        }



        $ejecutivos = usuario::where('status', 1)->where('role',5)->get(['id', 'nombres', 'apellidos']);

        if (!count($ejecutivos)) {
            return redirect()->back()->with('danger', 'No hay Ejecutivos Activos');
            $ejecutivos = "No hay ejecutivos activos";
        }


        //return $contrato;
        $prioridades = prioridad::all(['idPrioridadTicket', 'nombre']);
        $historico = historico::where('Ticket_idticket', $id)->orderBy('created_at', 'DESC')->get();
        $estados = estados::where('tipo_estado', 1)->get(['idEstadoTicket', 'nombre']);
        $cierres = estados::where('tipo_estado', 2)->get(['idEstadoTicket', 'nombre']);

        $hora = 0;
        $min = 0;
        $seg = 0;

        foreach ($historico as $value) {
            if ($value->tipo == 10) {
                $time = explode(':', $value->descripcion);
                $hora = $hora + $time[0];
                $min = $min + $time[1];
                $seg = $seg + $time[2];
            }
        }


        $res = $seg % 60;
        $div = $seg / 60;


        for ($i = 0; $i < $div - 1; $i++) {


            if ($seg >= 60) {

                $min++; //1
                $seg = $seg - 60;
            }
        }


        $tiempo_ticket = $hora . " horas," . $min . " min," . $seg . " seg";



        // $fecha1 = new DateTime("2010-07-28 01:15:52");
        // $fecha2 = new DateTime("2012-11-30 02:33:45");
        // $fecha = $fecha1->diff($fecha2);
        // printf('%d años, %d meses, %d días, %d horas, %d minutos', $fecha->y, $fecha->m, $fecha->d, $fecha->h, $fecha->i);

        //return $historico;
        return  view('Ticket.view', compact('tickets'))
            ->with('prioridades', $prioridades)
            ->with('tecnicos', $tecnicos)
            ->with('ejecutivos', $ejecutivos)
            ->with('historico', $historico)
            ->with('estados', $estados)
            ->with('cierres', $cierres)
            ->with('tiempo_t', $tiempo_ticket);
    }

}
