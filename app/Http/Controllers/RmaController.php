<?php

namespace App\Http\Controllers;

use App\Rma;
use App\Marca;
use App\empresa;
use App\HistorialRma;
use Illuminate\Http\Request;

class RmaController extends Controller
{
    public function __construct() { 
        $this->middleware('preventBackHistory');
        $this->middleware('auth'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $rmas = Rma::where('estado_rma','!=', 5)->orderBy('codigo_rma','DESC')->get();
        return view('Rma.index',compact('rmas')); 
    }

    public function indexArchivados()
    {
        
        $rmas = Rma::where('estado_rma', 5)->orderBy('codigo_rma','DESC')->get();
        return view('Rma.archivados',compact('rmas')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createE()
    {
        $codigo = Rma::orderBy('created_at', 'DESC')->first();
        ($codigo) ? $rut = $codigo->codigo_rma  : $rut = 0;
        $rut += 1;

        $empresas   = empresa::where('estado', 1)->get( ['idempresa','nombre']);
        $marcas     = Marca::where('estado',1)->get(['idmarca','nombre']);

        return view('Rma.create')->with('empresas', $empresas)->with('marcas',$marcas)->with('tipo', 1)->with('rut', $rut);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createS()
    {
        $codigo = Rma::orderBy('created_at', 'DESC')->first();
        ($codigo) ? $rut = $codigo->codigo_rma  : $rut = 0;
        $rut += 1;

        $empresas = empresa::where('estado', 1)->get( ['idempresa','nombre']);
        $marcas = Marca::all( ['idmarca','nombre']);
        return view('Rma.create')->with('empresas', $empresas)->with('marcas',$marcas)->with('tipo', 2)->with('rut', $rut);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        echo $request;
        $this->validate($request,[
            'tipo'             => 'required|int|min:1'
        ]);



        if ($request->tipo == 1) {
            $this->validate($request,[
                'fecha'             => 'required|date', 
                'idcliente'         => 'required|int|min:1',
                'falla'             => 'required|string|max:255', 
                'accesorio'         => 'required|int|min:1',
                'marca'             => 'required|string|min:1|max:255', 
                'modelo'            => 'required|string|max:255',
                'serie'             => 'required|string|max:255', 
                'comentario'        => 'required|string|max:255', 
                'describir'         => 'required_if:accesorio,1|string|max:255',
                'estado'            => 'required|int|min:1',
                'estado'            => 'required|int|min:1',
                'descripcion'       => 'required|string|max:500'
            ]);

            $rma = new Rma();

            $rma->fecha               = $request->fecha;
            $rma->idcliente           = $request->idcliente;
            $rma->descripcion_falla   = $request->falla;
            $rma->accesorio           = $request->accesorio;
            $rma->describir           = $request->describir;
            $rma->marca               = $request->marca;
            $rma->modelo              = $request->modelo;
            $rma->numero_serie        = $request->serie;
            $rma->comentario          = $request->comentario;
            $rma->tipo_rma            = $request->tipo;
            $rma->estado_rma          = $request->estado;
            $rma->descripcion         = $request->descripcion;

            if ( $rma->save() ) {

                $historial                      = new HistorialRma();
                $historial->comentario          = 'Registro de RMA';
                $historial->id_rma              = $rma->codigo_rma;
                $historial->id_user             = auth()->user()->id;
                $historial->id_estado_anterior  = $request->estado;
                $historial->id_estado_actual    = $request->estado;
                $historial->fecha               = date('Y-m-d');
                $historial->hora                = date('H');
                $historial->save();

                return redirect('/entrada')->with('success','Registro creado satisfactoriamente');
            }else{
                return redirect('/entrada')->with('danger','Registro creado satisfactoriamente');
            }
        }else{
            $this->validate($request,[
                'fecha'             => 'required|date', 
                'idcliente'         => 'required|int|min:1',
                'falla'             => 'required|string|max:255', 
                'accesorio'         => 'required|int|min:1',
                'marca'             => 'required|string|min:1|max:255', 
                'modelo'            => 'required|string|max:255',
                'serie'             => 'required|string|max:255', 
                'comentario'        => 'required|string|max:255',
                'describir'         => 'required_if:accesorio,1|string|max:255',
                'estado'            => 'required|int|min:1',
                'estado'            => 'required|int|min:1',
                'devolucion'        => 'required|int|min:1',
                'descripcion'       => 'required|string|max:500'
            ]);

            $rma = new Rma();

            $rma->fecha               = $request->fecha;
            $rma->idcliente           = $request->idcliente;
            $rma->descripcion_falla   = $request->falla;
            $rma->accesorio           = $request->accesorio;
            $rma->marca               = $request->marca;
            $rma->modelo              = $request->modelo;
            $rma->numero_serie        = $request->serie;
            $rma->comentario          = $request->comentario;
            $rma->describir           = $request->describir;
            $rma->tipo_rma            = $request->tipo;
            $rma->estado_rma          = $request->estado;
            $rma->tipo_devolucion     = $request->devolucion;
            $rma->descripcion         = $request->descripcion;

            if ( $rma->save() ) {

                $historial                      = new HistorialRma();
                $historial->comentario          = 'Registro de RMA';
                $historial->id_rma              = $rma->codigo_rma;
                $historial->id_user             = auth()->user()->id;
                $historial->id_estado_anterior  = $request->estado;
                $historial->id_estado_actual    = $request->estado;
                $historial->fecha               = date('Y-m-d');
                $historial->hora                = date('H');
                $historial->save();

                return redirect('/salida')->with('success','Registro creado satisfactoriamente');
            }else{
                return redirect('/salida')->with('danger','Registro creado satisfactoriamente');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rma  $rma
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rma= Rma::find($id);
        $empresas = empresa::all( ['idempresa','nombre']);
        $marcas = Marca::all( ['idmarca','nombre']);

        return view('Rma.view',compact('rma'))->with('empresas', $empresas)->with('marcas', $marcas);
    }

    public function historial($id)
    {
        $rma        = Rma::find($id);

        return view('Rma.historial',compact('rma'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rma  $rma
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rma                = Rma::find($id);
        $empresas           = empresa::where('estado', 1)->get( ['idempresa','nombre']);
        $marcas             = Marca::all( ['idmarca','nombre']);

        // actualizamos fecha de edicion
        $rma->updated_at    = date('Y-m-d H:i:s');
        $rma->save();

        return view('Rma.edit',compact('rma'))->with('empresas', $empresas)->with('marcas', $marcas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rma  $rma
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {


        $this->validate($request,[
            'tipo'             => 'required|int|min:1'
        ]);

        if ($request->tipo == 1) {
            $this->validate($request,[
                'fecha'                 => 'required|date', 
                'idcliente'             => 'required|int|min:1',
                'falla'                 => 'required|string|max:255', 
                'accesorio'             => 'required|int|min:1',
                'marca'                 => 'required|string|max:255', 
                'modelo'                => 'required|string|max:255',
                'serie'                 => 'required|string|max:255', 
                'comentario'            => 'required|string|max:255',
                'estado'                => 'required|int|min:1',
                'estado'                => 'required|int|min:1',
                'descripcion'           => 'required|string|max:500',
                'comentario_historial'  => 'required|max:500'
            ]);

            $rma = Rma::find($id);

            $find_historial           = HistorialRma::where('id_rma',$id)->orderBy('created_at','DESC')->first();

            $rma->fecha               = $request->fecha;
            $rma->idcliente           = $request->idcliente;
            $rma->descripcion_falla   = $request->falla;
            $rma->accesorio           = $request->accesorio;
            $rma->marca               = $request->marca;
            $rma->modelo              = $request->modelo;
            $rma->numero_serie        = $request->serie;
            $rma->comentario          = $request->comentario;
            $rma->tipo_rma            = $request->tipo;
            $rma->estado_rma          = $request->estado;
            $rma->descripcion         = $request->descripcion;

            if ( $rma->save() ) {

                $historial                      = new HistorialRma();
                $historial->comentario          = $request->comentario_historial;
                $historial->id_rma              = $rma->codigo_rma;
                $historial->id_user             = auth()->user()->id;
                $historial->id_estado_anterior  = ($find_historial) ? $find_historial->id_estado_anterior : $request->estado;
                $historial->id_estado_actual    = $request->estado;
                $historial->fecha               = date('Y-m-d');
                $historial->hora                = date('H');
                $historial->save();

                return redirect('rma')->with('success','Registro modificado satisfactoriamente');
            }else{
                return redirect('rma')->with('danger','Registro modificado satisfactoriamente');
            }
        }else{
            $this->validate($request,[
                'fecha'                 => 'required|date', 
                'idcliente'             => 'required|int|min:1',
                'falla'                 => 'required|string|max:255', 
                'accesorio'             => 'required|int|min:1',
                'marca'                 => 'required|string|max:255', 
                'modelo'                => 'required|string|max:255',
                'serie'                 => 'required|string|max:255', 
                'comentario'            => 'required|string|max:255',
                'estado'                => 'required|int|min:1',
                'estado'                => 'required|int|min:1',
                'devolucion'            => 'required|int|min:1',
                'descripcion'           => 'required|string|max:500',
                'comentario_historial'  => 'required|max:500'
            ]);

            $rma = Rma::find($id);
            
            $find_historial           = HistorialRma::where('id_rma',$id)->orderBy('created_at','DESC')->first();

            $rma->fecha               = $request->fecha;
            $rma->idcliente           = $request->idcliente;
            $rma->descripcion_falla   = $request->falla;
            $rma->accesorio           = $request->accesorio;
            $rma->marca               = $request->marca;
            $rma->modelo              = $request->modelo;
            $rma->numero_serie        = $request->serie;
            $rma->comentario          = $request->comentario;
            $rma->tipo_rma            = $request->tipo;
            $rma->estado_rma          = $request->estado;
            $rma->tipo_devolucion     = $request->devolucion;
            $rma->descripcion         = $request->descripcion;

            if ( $rma->save() ) {

                $historial                      = new HistorialRma();
                $historial->comentario          = $request->comentario_historial;
                $historial->id_rma              = $rma->codigo_rma;
                $historial->id_user             = auth()->user()->id;
                $historial->id_estado_anterior  = ($find_historial) ? $find_historial->id_estado_anterior : $request->estado;
                $historial->id_estado_actual    = $request->estado;
                $historial->fecha               = date('Y-m-d');
                $historial->hora                = date('H');
                $historial->save();

                return redirect('rma')->with('success','Registro modificado satisfactoriamente');
            }else{
                return redirect('rma')->with('danger','Registro modificado satisfactoriamente');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rma  $rma
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        $rma = Rma::find($id);
        $rma->estado_rma = 5;
        $rma->save();
        return redirect()->route('rma.index')->with('success','Registro Archivado satisfactoriamente');
    }
}
