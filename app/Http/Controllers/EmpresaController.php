<?php

namespace App\Http\Controllers;

use DB;
use App\cliente;
use App\empresa;
use App\tipoContrato;
use App\ComentarioEmpresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    public function __construct() { 
        $this->middleware('preventBackHistory');
        $this->middleware('auth'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = empresa::orderBy('idempresa','DESC')->get();

        return view('Empresa.index',compact('empresas')); 
    }

    public function validateNombre($nombre){
        $empresa = empresa::where('nombre',$nombre)->count();

        if($empresa){ return 1; }else{ return 0; }
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tipoContratos = tipoContrato::all();
        return view('Empresa.create', compact('tipoContratos'));
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        // DB::beginTransaction();
 
        // try {
            $this->validate($request,[
                'nombre'        => 'required|string|unique:empresa,nombre', 
                'razon'         => 'required|string:max:255', 
                'rut'           => 'required', 
                'telefono1'     => 'required|string|min:8|max:8', 
                'telefono2'     => 'nullable|string|min:8|max:8', 
                'direccion1'    => 'required|string|max:255', 
                'direccion2'    => 'nullable|string|max:255', 
                'fechaInicio'   => 'nullable|date', 
                'fechaFin'      => 'nullable|date',
                'ticket'        => 'nullable|integer', 
                'email'         => 'required|email|unique:empresa,email',
                'pais'          => 'required|string|max:255', 
                'ciudad'        => 'required|string|max:255', 
                'cp'            => 'required', 
                'notas'         => 'nullable|string|max:255',
                'idtipocontrato'=> 'required|int|min:1',
                'estado'        => 'required|int|min:1|max:2'
            ],[
                'estado.min'    => 'Debe seleccionar el estado',
                'estado.max'    => 'Debe seleccionar el estado',
                'idtipocontrato.min'    => 'Debe seleccionar tipo de contrato'
            ]);

            empresa::create($request->all());        

            // DB::commit();
            return redirect()->route('empresa.index')->with('success','Registro creado satisfactoriamente');

        // } catch (\Exception $e) {

        //     // DB::rollback();
        //     return redirect()->back()->with('danger','Error: intente nuevamente');

        // }
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = empresa::find($id);
        return  view('Empresa.show',compact('empresa'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $empresa        = empresa::find($id);
        $tipoContratos  = tipoContrato::all();
        $clientes       = cliente::where('idempresa', $id)->get();

        return view('Empresa.edit',compact('empresa'))->with('tipoContratos', $tipoContratos)->with('clientes', $clientes);
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    
    {
        $this->validate($request,[
                'nombre'            => 'required|string|string', 
                'razon'             => 'required|string:max:255', 
                'rut'               => 'required', 
                'telefono1'         => 'required|string|min:8|max:8', 
                'telefono2'         => 'nullable|string|min:8|max:8', 
                'direccion1'        => 'required|string|max:255', 
                'direccion2'        => 'nullable|string|max:255', 
                'fechaInicio'       => 'required|date', 
                'fechaFin'          => 'required|date', 
                'ticket'            => 'nullable|integer', 
                'email'             => 'required|email',
                'pais'              => 'required|string|max:255', 
                'ciudad'            => 'required|string|max:255', 
                'cp'                => 'required', 
                'notas'             => 'nullable|string|max:255',
                'idtipocontrato'    => 'required|int|min:1',
                'estado'            => 'required|int|min:1'
        ]);

        empresa::find($id)->update($request->except('comentario'));

        if(!empty($request->comentario)){

            $comentario = new ComentarioEmpresa();
            $comentario->comentario = $request->comentario;
            $comentario->id_empresa = $id;
            $comentario->id_user    = auth()->user()->id;
            $comentario->save();
        }


        return redirect()->route('empresa.index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        empresa::find($id)->delete();
        return redirect()->route('empresa.index')->with('success','Registro eliminado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function desactivar($id)
    {
        //
        $empresa = empresa::find($id);

        $empresa->estado = 2;

        $empresa->save();

        return redirect()->route('empresa.index')->with('success','Empresa deshabilitada');
    }

    public function activar($id)
    {
        //
        $empresa = empresa::find($id);

        $empresa->estado = 1;

        $empresa->save();

        return redirect()->route('empresa.index')->with('success','Empresa Activada');
    }
}
