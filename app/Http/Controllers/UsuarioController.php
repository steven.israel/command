<?php

namespace App\Http\Controllers;

use App\usuario;
use App\empresa;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    public function __construct() { 
        $this->middleware('preventBackHistory');
        $this->middleware('auth'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index(Request $request)
    {   
        $usuarios = usuario::get();
        return view('Usuario.index',compact('usuarios')); 
    }

    public function validateUsername($username){
        $user = usuario::where('username',$username)->count();

        if($user){ return 1; }else{ return 0; }
    }

    public function validateCorreo($email){
        $user = usuario::where('email',$email)->count();

        if($user){ return 1; }else{ return 0; }
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $empresas = empresa::where('estado', 1)->get( ['idempresa','nombre'] );
        return view('Usuario.create', compact('empresas'));
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'username'  => 'required', 
            'password'  => 'required_with:c-password|same:c-password',
            'email'     => 'required|email|unique:user,email', 
            'role'      => 'required|integer',
            'status'    => 'required|integer|min:1|max:2',
            'nombres'   => 'required|string',
            'apellidos' => 'required|string'
        ]);

        $usuario = new usuario();

        $usuario->username  = $request->username;
        $usuario->password  = bcrypt($request->password);
        $usuario->email     = $request->email;
        $usuario->role      = $request->role;
        $usuario->status    = $request->status;
        $usuario->nombres   = $request->nombres;
        $usuario->apellidos = $request->apellidos;
        $usuario->avatar    = "fav.png";

        $usuario->save();
        return redirect()->route('usuario.index')->with('success','Registro creado satisfactoriamente');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuarios= usuario::find($id);
        return  view('Usuario.show',compact('usuarios'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $empresas = empresa::where('estado', 1)->get( ['idempresa','nombre'] );
        $usuario= usuario::find($id);
        return view('Usuario.edit',compact('usuario'))->with('empresas', $empresas);
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {
      
        
        $this->validate($request,['username'=>'required', 'email'=>'required', 'role'=>'nullable','status'=>'required','nombres'=>'required','apellidos'=>'required']);
        
        $usuario = usuario::find($id);

        $usuario->username  = $request->username;
        $usuario->password  = bcrypt($request->password);
        $usuario->email     = $request->email;
        $usuario->status    = $request->status;
        $usuario->nombres   = $request->nombres;
        $usuario->apellidos = $request->apellidos;

        $usuario->update();
        return redirect()->route('usuario.index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        usuario::find($id)->delete();
        return redirect()->route('usuario.index')->with('success','Registro eliminado satisfactoriamente');
    }

    public function desactivar($id)
    {
        //
        $usuario = usuario::find($id);

        $usuario->status = 2;

        $usuario->save();

        return redirect()->route('usuario.index')->with('success','Usuario deshabilitado');
    }

    public function activar($id)
    {
        //
        $usuario = usuario::find($id);

        $usuario->status = 1;

        $usuario->save();

        return redirect()->route('usuario.index')->with('success','Usuario Activado');
    }
}
