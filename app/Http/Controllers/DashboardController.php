<?php

namespace App\Http\Controllers;

use DB;
use App\user;
use App\ticket;
use App\estados;
use App\empresa;
use App\cliente;
use App\Ingreso;
use App\prioridad;
use App\notificaciones;
use App\programacion_vista;
use App\contrato;
use App\usuario;
use App\historico;
use App\programacion;
use App\tipoContrato;
use Illuminate\Http\Request;
use App\Mail\EstadosMail;
use Illuminate\Support\Facades\Mail;
use Khill\Lavacharts\Lavacharts;
use DateTime;
use Illuminate\Support\Facades\Validator;

use Webklex\IMAP\Client;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('preventBackHistory');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $chart_area = '';
        $tickets = DB::table('ticket')
                     ->selectRaw('estadoticket.nombre,count(*) as total')
                     ->join('estadoticket', 'estadoticket.idEstadoTicket', '=', 'ticket.EstadoTicket_idEstadoTicket')
                     ->groupBy('estadoticket.nombre')
                     ->get();

        $estados = estados::all();

        //grafica de ticket abiertos vrs cerrados
        $grafica = DB::select('SELECT COUNT(case when EstadoTicket_idEstadoTicket = 1 then 1 end) as "abiertos",
                    COUNT(case when EstadoTicket_idEstadoTicket = 5 then 1 end) as "cerrados", created_at as fecha, day(created_at) as dia
                    from ticket GROUP BY dia');

        // structura de graficos
        if (!empty($grafica)) {
            foreach ($grafica as $value) {
                $chart_area .= "{ titulo:'".$value->fecha."', abiertos :".$value->abiertos.", cerrados:" . $value->cerrados. "}, ";
            }
        }

        $chart_area = substr($chart_area, 0, -2);

        //grafica de barras por estado de ticket
        $lava = new Lavacharts; // See note below for Laravel

        $votes  = $lava->DataTable();

        $votes->addStringColumn('Food Poll')
              ->addNumberColumn('Total');

        foreach ($tickets as  $value) {
            $votes->addRow([$value->nombre,  $value->total]);
        }

        $lava->BarChart('total', $votes);

        return view('dashboard',compact('tickets'))->with('chart_area', $chart_area)->with('lava',$lava)->with('estados', $estados) ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Request
     */
    public function indexFecha(Request $request)
    {
        $estados = estados::all();

         //return $request->start . ' ' . $request->end;
        $from = date('Y-m-d' . ' 00:00:00', strtotime($request->start)); //need a space after dates.
        $to = date('Y-m-d' . ' 23:59:59', strtotime($request->end));

        //
        $chart_area = '';
        if (!empty($request->estado)) {
            $tickets = DB::table('ticket')
                     ->selectRaw('estadoticket.nombre,count(*) as total')
                     ->join('estadoticket', 'estadoticket.idEstadoTicket', '=', 'ticket.EstadoTicket_idEstadoTicket')
                     ->where('estadoticket.idEstadoTicket', $request->estado)
                     ->whereBetween('ticket.created_at', array($from, $to))
                     ->get();
        }else{
            $tickets = DB::table('ticket')
                     ->selectRaw('estadoticket.nombre,count(*) as total')
                     ->join('estadoticket', 'estadoticket.idEstadoTicket', '=', 'ticket.EstadoTicket_idEstadoTicket')
                     ->groupBy('estadoticket.nombre')
                     ->whereBetween('ticket.created_at', array($from, $to))
                     ->get();
        }



        //grafica de ticket abiertos vrs cerrados
        $grafica = DB::select('SELECT COUNT(case when EstadoTicket_idEstadoTicket = 1 then 1 end) as "abiertos",
                    COUNT(case when EstadoTicket_idEstadoTicket = 5 then 1 end) as "cerrados", created_at as fecha
                    from ticket where created_at between "'.$from.'" and "'.$to.'" GROUP BY fecha');

        // structura de graficos
        if (!empty($grafica)) {
            foreach ($grafica as $value) {
                $chart_area .= "{ titulo:'".$value->fecha."', abiertos :".$value->abiertos.", cerrados:" . $value->cerrados. "}, ";
            }
        }

        $chart_area = substr($chart_area, 0, -2);

        //grafica de barras por estado de ticket
        $lava = new Lavacharts; // See note below for Laravel

        $votes  = $lava->DataTable();

        $votes->addStringColumn('Food Poll')
              ->addNumberColumn('Total');

        foreach ($tickets as  $value) {
            $votes->addRow([$value->nombre,  $value->total]);
        }

        $lava->BarChart('total', $votes);

        return view('dashboard',compact('tickets'))->with('chart_area', $chart_area)->with('lava', $lava)->with('estados', $estados) ;
    }






    public function busquedaTicket($busqueda){


         $busqueda = ticket::select('ticket.*', 'cliente.idcliente','cliente.idempresa', 'empresa.*')
         ->join('cliente', 'ticket.Cliente_idcliente', '=', 'cliente.idcliente')
         ->join('empresa', 'cliente.idempresa', '=', 'empresa.idempresa')
         ->where('numeroDeCaso','like','%'.$busqueda.'%')    
         ->orWhere('empresa.codigo','like','%'.$busqueda.'%')
         ->orWhere('empresa.nombre','like','%'.$busqueda.'%')
         ->where('ticket.archivado', 0)
         ->where('ticket.programado', 0)
         ->orderBy('ticket.created_at', 'DESC')
         ->get();
        

        if(count($busqueda)<0){
            $busqueda = "Sin coincidencias";
        }

        return $busqueda;


    }


    
    public function busqueda(Request $request){


        $search = $request->headerSearch;
        
        $rol = auth()->user()->role;


        $tickets = ticket::where('numeroDeCaso','like','%'.$search.'%')->orderBy('created_at', 'DESC')->get();
        

        if(count($tickets)<0){
            $tickets = "Sin coincidencias";
        }


        return view('Ticket.index', compact('tickets'))->with('titulo', "Busqueda");



    }
    


}
