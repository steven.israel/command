<?php

namespace App\Http\Controllers;

use App\prioridad;
use Illuminate\Http\Request;

class PrioridadController extends Controller
{
    public function __construct() { 
        $this->middleware('preventBackHistory'); 
        $this->middleware('auth'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prioridades = prioridad::orderBy('idprioridadTicket','DESC')->paginate(5);
        return view('Prioridades.index',compact('prioridades')); 
    }

    public function validateNombre($nombre){
        $prioridad = prioridad::where('nombre',$nombre)->count();

        if($prioridad){ return 1; }else{ return 0; }
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Prioridades.create');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,['nombre'=>'required|string|unique:prioridadticket,nombre', 'descripcion'=>'required|string', 'tiempoMinimoContacto'=>'required|int', 'tiempoMaximoContacto'=>'required|int','color'=>'required','tiempo_notificacion'=>'required']);
        prioridad::create($request->all());
        return redirect()->route('prioridad.index')->with('success','Registro creado satisfactoriamente');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $libros= prioridad::find($id);
        return  view('Prioridades.show',compact('libros'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $prioridad= prioridad::find($id);
        return view('Prioridades.edit',compact('prioridad'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {
        //
        $this->validate($request,['nombre'=>'required', 'descripcion'=>'required', 'tiempoMinimoContacto'=>'required', 'tiempoMaximoContacto'=>'required','color'=>'required','tiempo_notificacion'=>'required']);
        prioridad::find($id)->update($request->all());
        return redirect()->route('prioridad.index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        prioridad::find($id)->delete();
        return redirect()->route('prioridad.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
