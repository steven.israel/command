<?php

namespace App\Http\Controllers;

use DB;
use App\user;
use App\ticket;
use App\estados;
use App\empresa;
use App\cliente;
use App\Ingreso;
use App\prioridad;
use App\notificaciones;
use App\programacion_vista;
use App\contrato;
use App\usuario;
use App\historico;
use App\programacion;
use App\tipoContrato;
use Illuminate\Http\Request;
use App\Mail\EstadosMail;
use Illuminate\Support\Facades\Mail;
use Khill\Lavacharts\Lavacharts;
use DateTime;
use Illuminate\Support\Facades\Validator;

use Webklex\IMAP\Client;

class MailRememberController extends Controller
{

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function recordarRespuesta(){


        echo 'estoy funcionando<br>';

        $horadecontrato =  DB::table('prioridadticket')->get();


        //traigo los ticket no respondidos (estadorepuesta=0)
        //los almacenoen una lista (tentativo)
        //por cada ticket no respondido traigo su prioridad para ver cada cuanto le envio el correo de persistencia y sus datos a quien
        $ticketSR = DB::table('ticket')->where('estadorespuesta',0)->where('EstadoTicket_idEstadoTicket','<>',5)->get();





        foreach($ticketSR as $sr){
            $descripcion="";
            //SELECT * from historialticket h WHERE Ticket_idticket = 1 and tipo_usuario = 2
            $numeroMensajesCliente = DB::table('historialticket')->where('Ticket_idticket',$sr->idticket)->where('tipo_usuario',2);




            if(1>=$numeroMensajesCliente->count()){//Si el ultimo mensaje es de la empresa entra, verificar de quien es el ultimo mensaje a partir de un count
            //hecho a ls mensajes del cliente tipo 2 en el ticket



            $HoraRespuestaEmpresa = DB::table('historialticket')->where('Ticket_idticket', $sr->idticket)->where('tipo_usuario',1)->orderBy('created_at')->limit(1)->first();//cuando la empresa respondio a la peticion de ticket por correo
            $diaAhora = date("Y-m-d H:i:s"); // 1:30

            if(!$HoraRespuestaEmpresa){
                continue;
            }
                
            $day1 = strtotime($HoraRespuestaEmpresa->created_at);
            $day2 = strtotime($diaAhora);
            $diffHours = round(($day2 - $day1) / 3600);


           switch($sr->PrioridadTicket_idPrioridadTicket){


            case '4' :


                echo '<br>Empieza ticket<br>';
                echo 'Titulo: '. $HoraRespuestaEmpresa->descripcion .'<br>';
                echo 'Resta de horas: '.$diffHours .'<br>';
                echo 'Caso: cuatro<br>';
                $horaPrioridad=0;
                $cliente = DB::table('cliente')->where('idcliente', $sr->Cliente_idcliente)->get();
                $email ="";
                foreach($cliente as $c){

                    $email = $c->email;
                }
                foreach($horadecontrato as $hc){

                    if($hc->idPrioridadTicket == $sr->PrioridadTicket_idPrioridadTicket){
                        $horaPrioridad = $hc->tiempo_notificacion;
                    }
                }
                echo 'Tiempo prioridad del ticket: '.$horaPrioridad .'<br>';
                echo 'termina ticket<br><br>';


                if($diffHours >= $horaPrioridad){

                    //entra la primera vez
                    //primer correo recordatorio Envia:empresa al cliente que solicito el ticket asunto: Primer aviso
                    if($sr->intentosrespuesta==0){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 1
                        echo $sr->Cliente_idcliente . '<br>';
                        echo $horaPrioridad . '<br>';
                        echo $diffHours . '<br>';
                        echo $sr->numeroDeCaso . '<br>';
                        


                        // echo  $email. '<br>';


                                // Enviar correo de creacioón de ticket
                         //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 2
                        $descripcion="Estimado usuario este es el primer llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." para posteriormente seguir el proceso de resolucion, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 1;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 4 (Urgente)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        $sendMail->subject          = $sr->titulo;
                        

                        echo $sr->numeroDeCaso . '<br>';
                        


                        Mail::to($email)->send(new EstadosMail($sendMail));





                    }
                    //segundo correo recordatorio Envia:empresa al cliente que solicito el ticket
                    if($sr->intentosrespuesta==1 && $diffHours >= $horaPrioridad * 2 ){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 2
                        $descripcion="Estimado usuario este es el segundo llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." para posteriormente seguir el proceso de resolucion, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 2;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 4 (Urgente)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));



                    }
                    
                    //tercer correo recordatorio Envia:empresa al cliente que solicito el ticket
                    if($sr->intentosrespuesta==2 && $diffHours >= $horaPrioridad * 3){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 3
                        $descripcion="Estimado usuario este es el tercer llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." si en esta ocasion no es enviada una respuesta, su ticket sera puesto como cerrado por la ausencia de su respuesta, esperamos su respuesta, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 3;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 4 (Urgente)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));
                    }
                    //cuatro correo se actualiza el ticket al estado Esperando Respuesta Cliente
                    if($sr->intentosrespuesta==3 && $diffHours >= $horaPrioridad * 4 ){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 1
                        //actualizar el campo estadoticketde la tabla ticket a cerrado por ausencia de respuesta por parte del cliente
                        $descripcion="Estimado usuario se le informa que su ticket ". $sr->numeroDeCaso ." ha sido Cerrado debido a una falta de respuesta, si necesita resolucion al problema por e cual se contacto a la empresa, le recomendamos enviar otro correo con su necesidad, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 4;
                        $ticketP->EstadoTicket_idEstadoTicket = 5;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 100;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 4 (Urgente)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));

                    }

                    if($sr->intentosrespuesta!=5){

                        if($sr->intentosrespuesta==4){
                        $ticketP= ticket::findOrFail($sr->idticket);
                            $ticketP->intentosrespuesta = 5;
                        $ticketP->save();
                        }
                        


                    }

                }

                break;


            case '3':

                echo '<br>Empieza ticket<br>';
                echo 'Titulo: '. $HoraRespuestaEmpresa->descripcion .'<br>';
                echo 'Resta de horas: '.$diffHours .'<br>';



                echo 'entré a caso tres<br>';
                $horaPrioridad=0;
                $cliente = DB::table('cliente')->where('idcliente', $sr->Cliente_idcliente)->get();
                $email ="";
                foreach($cliente as $c){

                    $email = $c->email;
                }
                foreach($horadecontrato as $hc){

                    if($hc->idPrioridadTicket == 3){
                        $horaPrioridad = $hc->tiempo_notificacion;
                    }
                }
                echo 'Tiempo de prioridad de ticket: '. $horaPrioridad .'<br>';
                if($diffHours >= $horaPrioridad){

                    //entra la primera vez
                    //primer correo recordatorio Envia:empresa al cliente que solicito el ticket asunto: Primer aviso
                    if($sr->intentosrespuesta==0){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 1
                        echo $sr->Cliente_idcliente . '<br>';
                        echo $horaPrioridad . '<br>';
                        echo $diffHours . '<br>';
                        echo $sr->numeroDeCaso . '<br>';
                        


                        // echo  $email. '<br>';


                                // Enviar correo de creacioón de ticket
                         //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 2
                        $descripcion="Estimado usuario este es el primer llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." para posteriormente seguir el proceso de resolucion, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 1;
                        $ticketP->save();

                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 3 (Alta o Urgente)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));





                    }
                    
                    //segundo correo recordatorio Envia:empresa al cliente que solicito el ticket
                    if($sr->intentosrespuesta==1 && $diffHours >= $horaPrioridad * 2){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 2
                        $descripcion="Estimado usuario este es el segundo llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." para posteriormente seguir el proceso de resolucion, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 2;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 3 (Alta o Urgente)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));



                    }
                    //tercer correo recordatorio Envia:empresa al cliente que solicito el ticket
                    if($sr->intentosrespuesta==2 && $diffHours >= $horaPrioridad * 3){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 3
                        $descripcion="Estimado usuario este es el tercer llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." si en esta ocasion no es enviada una respuesta, su ticket sera puesto como cerrado por la ausencia de su respuesta, esperamos su respuesta, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 3;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 3 (Alta o Urgente)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));
                    }
                    //cuatro correo se actualiza el ticket al estado Esperando Respuesta Cliente
                    if($sr->intentosrespuesta==3 && $diffHours >= $horaPrioridad * 4){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 1
                        //actualizar el campo estadoticketde la tabla ticket a cerrado por ausencia de respuesta por parte del cliente
                        $descripcion="Estimado usuario se le informa que su ticket ". $sr->numeroDeCaso ." ha sido Cerrado debido a una falta de respuesta, si necesita resolucion al problema por e cual se contacto a la empresa, le recomendamos enviar otro correo con su necesidad, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 4;
                        $ticketP->EstadoTicket_idEstadoTicket = 5;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 100;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 3 (Alta o Urgente)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));

                    }

                    if($sr->intentosrespuesta!=5){

                        if($sr->intentosrespuesta==4){
                        $ticketP= ticket::findOrFail($sr->idticket);
                            $ticketP->intentosrespuesta = 5;
                        $ticketP->save();
                    }


                    }

                }//Fin del if


                break;

            case '2' :

                echo '<br>Empieza ticket<br>';
                echo 'Titulo: '. $HoraRespuestaEmpresa->descripcion .'<br>';
                echo 'Resta de horas: '.$diffHours .'<br>';

                echo 'entré a caso dos<br>';
                $horaPrioridad=0;
                $cliente = DB::table('cliente')->where('idcliente', $sr->Cliente_idcliente)->get();
                $email ="";
                foreach($cliente as $c){

                    $email = $c->email;
                }
                foreach($horadecontrato as $hc){

                    if($hc->idPrioridadTicket == 2){
                        $horaPrioridad = $hc->tiempo_notificacion;
                    }
                }
                echo 'Tiempo de prioridad de ticket: '. $horaPrioridad .'<br>';
                if($diffHours >= $horaPrioridad){

                    echo 'entré si ya es menor la hora<br>';

                    //entra la primera vez
                    //primer correo recordatorio Envia:empresa al cliente que solicito el ticket asunto: Primer aviso
                    if($sr->intentosrespuesta==0){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 1
                        echo $sr->Cliente_idcliente . '<br>';
                        echo $horaPrioridad . '<br>';
                        echo $diffHours . '<br>';
                        echo $sr->numeroDeCaso . '<br>';
                        


                        // echo  $email. '<br>';


                                // Enviar correo de creacioón de ticket
                         //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 2
                        $descripcion="Estimado usuario este es el primer llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." para posteriormente seguir el proceso de resolucion, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 1;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 2 (Normal)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));





                    }
                    
                    //segundo correo recordatorio Envia:empresa al cliente que solicito el ticket
                    if($sr->intentosrespuesta==1 && $diffHours >= $horaPrioridad * 2){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 2
                        $descripcion="Estimado usuario este es el segundo llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." para posteriormente seguir el proceso de resolucion, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 2;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 2 (Normal)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));



                    }
                    //tercer correo recordatorio Envia:empresa al cliente que solicito el ticket
                    if($sr->intentosrespuesta==2 && $diffHours >= $horaPrioridad * 3){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 3
                        $descripcion="Estimado usuario este es el tercer llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." si en esta ocasion no es enviada una respuesta, su ticket sera puesto como cerrado por la ausencia de su respuesta, esperamos su respuesta, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 3;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 2 (Normal)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));
                    }
                    //cuatro correo se actualiza el ticket al estado Esperando Respuesta Cliente
                    if($sr->intentosrespuesta==3 && $diffHours >= $horaPrioridad * 4){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 1
                        //actualizar el campo estadoticketde la tabla ticket a cerrado por ausencia de respuesta por parte del cliente
                        $descripcion="Estimado usuario se le informa que su ticket ". $sr->numeroDeCaso ." ha sido Cerrado debido a una falta de respuesta, si necesita resolucion al problema por e cual se contacto a la empresa, le recomendamos enviar otro correo con su necesidad, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 4;
                        $ticketP->EstadoTicket_idEstadoTicket = 5;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 100;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 2 (Normal)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));

                    }

                    if($sr->intentosrespuesta!=5){
                        if($sr->intentosrespuesta==4){
                        $ticketP= ticket::findOrFail($sr->idticket);
                            $ticketP->intentosrespuesta = 5;
                        $ticketP->save();
                    }
                    

                    }

                }

                break;

            case '1' :

                echo $HoraRespuestaEmpresa->descripcion .'<br>';
                echo $diffHours .'<br>';
                echo 'entré a caso uno<br>';
                $horaPrioridad=0;
                $cliente = DB::table('cliente')->where('idcliente', $sr->Cliente_idcliente)->get();
                $email ="";
                foreach($cliente as $c){

                    $email = $c->email;
                }
                foreach($horadecontrato as $hc){

                    if($hc->idPrioridadTicket == 1){
                        $horaPrioridad = $hc->tiempo_notificacion;
                    }
                }
                echo 'Tiempo de prioridad de ticket: '. $horaPrioridad .'<br>';
                if($diffHours >= $horaPrioridad){

                    //entra la primera vez
                    //primer correo recordatorio Envia:empresa al cliente que solicito el ticket asunto: Primer aviso
                    if($sr->intentosrespuesta==0){




                                // Enviar correo de creacioón de ticket
                         //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 2
                        $descripcion="Estimado usuario este es el primer llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." para posteriormente seguir el proceso de resolucion, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 1;
                        $ticketP->save();

                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 1 (Bajo o Sin Empresa)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));




                    }
                    
                    //segundo correo recordatorio Envia:empresa al cliente que solicito el ticket
                    if($sr->intentosrespuesta==1 && $diffHours >= $horaPrioridad * 2){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 2
                        $descripcion="Estimado usuario este es el segundo llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." para posteriormente seguir el proceso de resolucion, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 2;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 1 (Bajo o Sin Empresa)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));


                    }
                    //tercer correo recordatorio Envia:empresa al cliente que solicito el ticket
                    if($sr->intentosrespuesta==2 && $diffHours >= $horaPrioridad * 3){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 3
                        $descripcion="Estimado usuario este es el tercer llamado para retomar su resolucion, se le informa que debe de responder a la brevedad para retomar su caso con ticket ". $sr->numeroDeCaso ." si en esta ocasion no es enviada una respuesta, su ticket sera puesto como cerrado por la ausencia de su respuesta, esperamos su respuesta, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 3;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 6;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 1 (Bajo o Sin Empresa)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));
                    }
                    //cuatro correo se actualiza el ticket al estado Esperando Respuesta Cliente
                    if($sr->intentosrespuesta==3 && $diffHours >= $horaPrioridad * 4){

                        //codigo de enviar correo primer intento
                        //hacer update al campo intentosrespuesta de la tabla ticket a = 1
                        //actualizar el campo estadoticketde la tabla ticket a cerrado por ausencia de respuesta por parte del cliente
                        $descripcion="Estimado usuario se le informa que su ticket ". $sr->numeroDeCaso ." ha sido Cerrado debido a una falta de respuesta, si necesita resolucion al problema por e cual se contacto a la empresa, le recomendamos enviar otro correo con su necesidad, buen dia!";
                        $ticketP= ticket::findOrFail($sr->idticket);
                        $ticketP->intentosrespuesta = 4;
                        $ticketP->EstadoTicket_idEstadoTicket = 5;
                        $ticketP->save();
                        $sendMail = new \stdClass();
                        $sendMail->numeroDeCaso     = $sr->numeroDeCaso;
                        $sendMail->tipo             = 100;
                        $sendMail->fecha            = date("F j, Y", strtotime($sr->created_at));
                        $sendMail->hora             = date("H:i:s", strtotime($sr->created_at));
                        $sendMail->descripcion      = $descripcion;
                        $sendMail->tecnico          = "";
                        $sendMail->prioridad        = "Nivel 1 (Bajo o Sin Empresa)";
                        $sendMail->estado           = "Unknow";
                        $sendMail->titulo           = $sr->titulo;
                        $sendMail->moderador        = "";
                        $sendMail->nota             = "";
                        $sendMail->tecnico_anterior = "";
                        $sendMail->respuesta        = "";
                        
                        
                            echo $sr->numeroDeCaso . '<br>';
                        
                        
                        $sendMail->subject          = $sr->titulo;
                        Mail::to($email)->send(new EstadosMail($sendMail));

                    }

                    if($sr->intentosrespuesta!=5){

                        if($sr->intentosrespuesta==4){
                        $ticketP= ticket::findOrFail($sr->idticket);
                            $ticketP->intentosrespuesta = 5;
                        $ticketP->save();
                    }

                       

                    }

                }

                break;










           }









        }else{
            echo "actualizar estado respuesta=1 <br>";
            //no entró en el if porque elcliente ya tiene mas de dos o mas mensajes en el hilo de este ticket
            //se procede a actualizarlo en el campo ticket a respondido estadorespuesta = 1
            //actualizar estadorespuesta
            $ticketP = ticket::findOrFail($sr->idticket);
            $ticketP->estadorespuesta = 1;
            $ticketP->save();




        }




    }//fin foreach






}




}
