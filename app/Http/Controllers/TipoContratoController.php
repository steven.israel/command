<?php

namespace App\Http\Controllers;

use App\prioridad;
use App\tipoContrato;
use Illuminate\Http\Request;

class TipoContratoController extends Controller
{
    public function __construct() { 
        $this->middleware('preventBackHistory'); 
        $this->middleware('auth'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tipoContratos=tipoContrato::orderBy('idtipoContrato','DESC')->paginate(5);
        return view('TipoContrato.index',compact('tipoContratos')); 
    }

    public function validateNombre($nombre){
        $tipo = tipoContrato::where('nombre',$nombre)->count();

        if($tipo){ return 1; }else{ return 0; }
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $prioridades = prioridad::all( ['idPrioridadTicket','nombre'] );
        return view('TipoContrato.create', compact('prioridades'));
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'nombre'                            =>'required|unique:tipocontrato,nombre', 
            'descripcion'                       =>'required', 
            'prioridadticket_idprioridadticket' =>'required|exists:prioridadticket,idPrioridadTicket', 
            'color'                             =>'required'
        ],[
            'nombre.required'                           => 'Campo requerido',
            'nombre.unique'                             => 'El nombre ya existe',
            'descripcion.required'                      => 'Campo requerido',
            'prioridadticket_idprioridadticket.exists'  => 'La prioridad seleccionada no existe',
            'color.required'                            => 'Campo requerido'
        ]);
        tipoContrato::create($request->all());
        return redirect()->route('tipoContrato.index')->with('success','Registro creado satisfactoriamente');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $libros= tipoContrato::find($id);
        return  view('TipoContrato.show',compact('libros'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $prioridades = prioridad::all( ['idPrioridadTicket','nombre'] );
        $tipoContrato= tipoContrato::find($id);
        return view('TipoContrato.edit',compact('tipoContrato'))->with('prioridades', $prioridades);
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {
        //
        $this->validate($request,['nombre'=>'required', 'descripcion'=>'required', 'prioridadticket_idprioridadticket'=>'required', 'color'=>'required']);
 
        tipoContrato::find($id)->update($request->all());
        return redirect()->route('tipoContrato.index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        tipoContrato::find($id)->delete();
        return redirect()->route('tipoContrato.index')->with('success','Registro eliminado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function desactivar($id)
    {
        //
        $tipoContrato = tipoContrato::find($id);

        $tipoContrato->estado = 2;

        $tipoContrato->save();

        return redirect()->back()->with('success','Tipo de Contrato Deshabilitada');
    }

    public function activar($id)
    {
        //
        $tipoContrato = tipoContrato::find($id);

        $tipoContrato->estado = 1;

        $tipoContrato->save();

        return redirect()->back()->with('success','Tipo de Contrato Activada');
    }
}
