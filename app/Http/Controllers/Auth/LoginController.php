<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function showLogin()
    {
        // Verificamos que el usuario no esté autenticado
        if (Auth::check())
        {
            // Si está autenticado lo mandamos a la raíz donde estara el mensaje de bienvenida.
            return Redirect('/dashboard');
        }
        // Mostramos la vista login.blade.php (Recordemos que .blade.php se omite.)
        return View('auth/login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function validarLogin(Request $request)
    {

        $this->validate($request, [
            'email'         => 'required|string|max:255',
            'password'      => 'required|string|max:255',
        ]);

        $credentials = $request->only('email', 'password');

        $validate = Auth::attempt($credentials);

        if ($validate) {

            return redirect()->intended('/dashboard');
        }

        $request->session()->flash('alert-warning', 'Usuario o Contraseña incorrectas');
        return redirect('/');
    }

    public function logOut(Request $request)
    {

        $request->session()->regenerate(true);

        Auth::logout();
        return Redirect('/');
    }

}
