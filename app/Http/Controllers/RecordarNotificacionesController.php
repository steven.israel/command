<?php

namespace App\Http\Controllers;

use App\ticket;
use App\historico;
use App\programacion;
use App\notificaciones;
use Illuminate\Http\Request;

class RecordarNotificacionesController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }
    public function programados()
    {
        // $hoy = date('2021-02-10');
        $hoy = date('Y-m-d', strtotime(now()));
        $ticket_programado = programacion::where('tiempo', 1)
            ->where('fecha_programacion', $hoy)
            ->orderBy('created_at', 'DESC')->get();

        if (count($ticket_programado) > 0) {
            foreach ($ticket_programado as $value) {
                //cambiar estado de ticket programado a 0
                echo "cambio estado a ticket <br>";
                $ticket = ticket::find($value->idticket);
                $ticket->programado = 0;
                $ticket->save();

                // se crea la notificacion
                echo "creo la notificacion";
                $notificacion = new notificaciones();
                $notificacion->idticket = $value->idticket;
                $notificacion->titulo = $value->descripcion;
                $notificacion->mensaje = "Programado";
                $notificacion->status = 0;
                $notificacion->visto = 0;
                $notificacion->save();
            }
        } else {
            echo "no hay programados para este dia";
        }
    }

    protected function notificacionesTicketProgramado($tiempo)
    {
        $dia = 24;
        $semana = 168;
        $quincena = 360;
        $mes = 720;
        $semestre = 1440;
        $anual = 8760;
        switch ($tiempo) {
            case 1:
                return $dia;
                break;
            case 2:
                return $semana;
                break;
            case 3:
                return $quincena;
                break;
            case 4:
                return $mes;
                break;
            case 5:
                return $semestre;
                break;
            case 6:
                return $anual;
                break;
            default:
                echo "no entro en ninguno";
                break;
        }
    }

    public function ticketFrecuencia()
    {
        $tiempo = 0;
        $ticketsP = programacion::select('*')
            ->join('ticket', 'ticket_programado.idticket', '=', 'ticket.idticket')
            ->orderBy('ticket_programado.created_at', 'ASC')
            ->where('ticket.programado', 1)
            ->get();

        if (count($ticketsP) > 0) {
            foreach ($ticketsP as $value) {
                $diaInicial = strtotime($value->fechaIni);
                $diaFinal = strtotime($value->fechaFin);
                $diaAhora =  date("Y-m-d", strtotime(now()));
                // $diaAhora = date('2021-02-24');
                $resta2 = round((strtotime($diaAhora) - $diaInicial) / 3600);
                $inicio = strtotime($diaAhora) >= $diaInicial ? true : false;
                $fin = strtotime($diaAhora) <= $diaFinal ? true : false;
                $cantidad = $value->conteoNotificacion;
                $tiempo = $value->tiempo;
                $hora = $this->notificacionesTicketProgramado($tiempo);
                if (($inicio  && $fin) && $value->conteoNotificacion > 0) {
                    echo "entre al el if de notificaciones<br>";
                    for ($i = 1; $i <= $cantidad + 1; $i++) {
                        echo $i . "= resta: " . $resta2 . "  hora:  " . $hora * $i . "<br>";
                        if ($i <= 0) {
                            break;
                        }
                        if ($resta2 == ($hora * $i)) {
                            echo "crear notificacion <br>";
                            $historico = new historico();
                            $historico->Ticket_idticket = $value->idticket;
                            $historico->descripcion = "Ticket " . $value->numeroDeCaso . " Programado";
                            $historico->tipo = 8;
                            $historico->usuario = 1;
                            //tipo usuario 2: cuando es agente y 1: cuando es cliente
                            $historico->tipo_usuario = 2;
                            $historico->estado_ticket = 1;
                            $historico->save();
                            $value->conteoNotificacion -= 1;
                            $value->save();


                            $notificacion = new notificaciones();
                            $notificacion->titulo = $historico->descripcion;
                            $notificacion->mensaje = "Programado";
                            $notificacion->idticket = $value->idticket;
                            $notificacion->save();
                        } else {
                            echo '<br>  No entro para crear la notificacion<br>';
                        }
                    }
                }
                if (strtotime($diaAhora) > $diaFinal) {
                    echo "me pase de la fecha, cambiando estado programado a 0 <br>";
                    $ticket = ticket::findOrFail($value->idticket);
                    $ticket->programado = 0;
                    $ticket->save();
                }
            }
        } else {
            echo "no hay objetos para recorrer";
        }
    }
}
