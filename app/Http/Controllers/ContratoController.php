<?php

namespace App\Http\Controllers;

use App\contrato;
use App\cliente;
use App\tipoContrato;
use Illuminate\Http\Request;

class ContratoController extends Controller
{
    public function __construct() { 
        $this->middleware('preventBackHistory');
        $this->middleware('auth'); 
    }
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $contratos = contrato::orderBy('idcontrato','DESC')->paginate(5);
        return view('Contrato.index',compact('contratos')); 
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tipoContratos = tipoContrato::all( ['idtipoContrato', 'nombre'] ); 
        $clientes = cliente::where('estado', 1)->get();
        return view('Contrato.create')->with('tipoContratos', $tipoContratos)->with('clientes', $clientes);
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,['fechaInicio'=>'required', 'fechaFin'=>'required', 'TipoContrato_idtipoContrato'=>'required', 'Cliente_idcliente'=>'required']);
        contrato::create($request->all());
        return redirect()->route('contrato.index')->with('success','Registro creado satisfactoriamente');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $libros= contrato::find($id);
        return  view('Contrato.show',compact('libros'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tipoContratos = tipoContrato::all( ['idtipoContrato', 'nombre'] );
        $clientes = cliente::where('estado', 1)->get();
        $contrato= contrato::find($id);
        return view('Contrato.edit',compact('contrato'))->with('tipoContratos', $tipoContratos)->with('clientes', $clientes);
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {
        //
        $this->validate($request,['fechaInicio'=>'required', 'fechaFin'=>'required', 'TipoContrato_idtipoContrato'=>'required', 'Cliente_idcliente'=>'required']);
 
        contrato::find($id)->update($request->all());
        return redirect()->route('contrato.index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        contrato::find($id)->delete();
        return redirect()->route('contrato.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
