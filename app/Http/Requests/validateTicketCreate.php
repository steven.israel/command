<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validateTicketCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numeroDeCaso'      =>'required',
            'titulo'            =>'required|max:255',
            'Cliente_idcliente' =>'required|int|min:1',
            'descripcion'       =>'required|max:255',
            'comentario'        =>'required|max:255'
        ];
    }
    public function messages()
    {
        return [
            'numeroDeCaso.required' => 'El numero de caso es requerido',
            'titulo.required' => 'El titulo es requerido',
            'titulo.max' => 'El titulo debe de contener un maximo de 255 caracteres',
            'Cliente_idcliente.required' =>'El cliente es requerido',
            'Cliente_idcliente.required' =>'El id del cliente debe ser un entero',
            'Cliente_idcliente.min' =>'Debe seleccionar al menos un cliente',
            'descripcion.required' =>'La descripción es requerida',
            'descripcion.max' =>'La descripción debe de contener un maximo de 255 caracteres',
            'comentario.required' => 'El comentario es requerido',
            'comentario.max' => 'El comentario debe de contener un maximo de 255 caracteres'
        ];
    }
}
