<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class empresa extends Model
{
    //
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'empresa';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idempresa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','razon','ticket','rut','telefono1','telefono2','direccion1','direccion2','email','codigo','ciudad','pais','cp','web','notas','fechaInicio','fechaFin','idtipocontrato','estado',
    ];

    public function tipoContrato()
    {
        return $this->belongsTo('App\tipoContrato', 'idtipocontrato', 'idtipoContrato');
    }

    public function historial()
    {
        return $this->hasMany('App\ComentarioEmpresa', 'id_empresa', 'idempresa');
    }

    public function is_delete(){
        $cliente    = $this->hasMany('App\cliente','idempresa','idempresa')->count();
        $user       = $this->hasMany('App\usuario','idempresa','idempresa')->count();

        if ($cliente || $user) {
            return false;
        }else{
            return true;
        }
    }

}
