<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioEmpresa extends Model
{
    protected $table = 'comentario_empresas';

    protected $fillable = ['comentario','id_user','id_empresa','created_at','updated_at'];

    public function user(){
        return $this->hasOne('App\usuario','id','id_user');
    }

    public function empresa(){
        return $this->hasOne('App\empresa','idempresa','id_empresa');
    }
}
