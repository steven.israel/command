<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipoContrato extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipocontrato';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idtipoContrato';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','descripcion','prioridadticket_idprioridadticket','color',
    ];

    public function prioridad()
    {
        return $this->belongsTo('App\prioridad', 'prioridadticket_idprioridadticket', 'idPrioridadTicket');
    }

    public function is_delete()
    {
        $contrato   = $this->hasMany('App\contrato','TipoContrato_idtipoContrato','idtipoContrato')->count();
        $empresa    = $this->hasMany('App\empresa','idtipocontrato','idtipoContrato')->count();

        if ($contrato || $empresa) {
            return false;
        }else{
            return true;
        }
    }
}
