<?php
/*ES LA PRUEBA 2*/
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable; 
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait ;

class usuario extends \Eloquent implements Authenticatable
{
    use AuthenticableTrait, Sortable;

    protected $table = 'user';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','email','avatar','role','status','nombres','apellidos','idempresa','deleted_at','created_at','updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $sortable = ['username','email','role','status','created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rol()
    {
        return $this->belongsTo('App\rol', 'idrol', 'idrol');
    }

    public function is_delete()
    {
        $historial  = $this->hasMany('App\historico','usuario','id')->count();
        $ticket     = $this->hasMany('App\ticket','idusuario','id')->count();

        if ($historial || $ticket) {
            return false;
        }else{
            return true;
        }
    }
}
