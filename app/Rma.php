<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rma extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'rma';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'codigo_rma';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fecha','idcliente','descripcion_falla','accesorio','marca','modelo','numero_serie','comentario','describir','tipo_rma','tipo_devolucion','estado_rma','created_at','updated_at'
    ];

     public function cliente()
    {
        return $this->belongsTo('App\cliente', 'idcliente', 'idcliente');
    }
    public function marcas()
    {
        return $this->belongsTo('App\Marca', 'marca', 'idmarca');
    }

    public function historial()
    {
        return $this->hasMany('App\HistorialRma', 'id_rma', 'codigo_rma')->orderby('created_at','DESC');
    }
}
