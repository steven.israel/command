<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contrato extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'contrato';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idcontrato';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fechaInicio','FechaFin','TipoContrato_idtipoContrato','Cliente_idcliente',
    ];

    public function tipoContrato()
    {
        return $this->belongsTo('App\tipoContrato', 'TipoContrato_idtipoContrato', 'idtipoContrato');
    }

    public function cliente()
    {
        return $this->belongsTo('App\cliente', 'Cliente_idcliente', 'idcliente');
    }

}
