<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ticket';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idticket';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['numeroDeCaso','PrioridadTicket_idPrioridadTicket','tipoCierre_idtipoCierre','Cliente_idcliente','EstadoTicket_idEstadoTicket','descripcion','titulo','tecnico','comercial','idusuario','comentario','archivado','tipo_ingreso', 'idingreso', 'abierto',
    ];

    public function prioridad()
    {
        return $this->belongsTo('App\prioridad', 'PrioridadTicket_idPrioridadTicket', 'idPrioridadTicket');
    }
    public function usuario()
    {
        return $this->belongsTo('App\usuario', 'idusuario', 'id');
    }
    public function tecnicos()
    {
        return $this->belongsTo('App\usuario', 'tecnico', 'id');
    }

    public function ejecutivo()
    {
        return $this->belongsTo('App\usuario', 'comercial', 'id');
    }

    
    public function ingreso(){
        return $this->belongsTo('App\Ingreso', 'idingreso', 'idingreso');
    }

    // public function cierre()
    // {
    //     return $this->belongsTo('App\cierre', 'tipoCierre_idtipoCierre', 'idPrioridadTicket');
    // }
    public function cliente()
    {
        return $this->belongsTo('App\cliente', 'Cliente_idcliente', 'idcliente');
    }
    public function estado()
    {
        return $this->belongsTo('App\estados', 'EstadoTicket_idEstadoTicket', 'idEstadoTicket');
    }
}
