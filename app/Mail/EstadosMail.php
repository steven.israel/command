<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EstadosMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mail;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if (!empty($this->mail->file) && empty($this->mail->pdf)) {
            return $this->from('soportetickets2018@gmail.com')
                    ->subject($this->mail->subject)
                    ->attach("./files/".$this->mail->file)
                    ->view('plantillas.plantilla');
        }
        if (empty($this->mail->file) && !empty($this->mail->pdf)) {
            return $this->from('soportetickets2018@gmail.com')
                    ->subject($this->mail->subject)
                    ->attach("./files/".$this->mail->pdf)
                    ->view('plantillas.plantilla');
        }
        if (!empty($this->mail->file) && !empty($this->mail->pdf)) {
            return $this->from('soportetickets2018@gmail.com')
                    ->subject($this->mail->subject)
                    ->attach("./files/".$this->mail->file)
                    ->attach("./files/".$this->mail->pdf)
                    ->view('plantillas.plantilla');
        }
        if (empty($this->mail->file) && empty($this->mail->pdf)) {
            return $this->from('soportetickets2018@gmail.com')
                    ->subject($this->mail->subject)
                    ->view('plantillas.plantilla');
        }

    }
}
