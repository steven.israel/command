<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'marca';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idmarca';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','estado	','created_at','updated_at'];

    public function is_delete(){
        $rma = $this->hasMany('App\Rma','marca','idmarca')->count();

        if ($rma) {
            return false;
        }else{
            return true;
        }
    }
}
