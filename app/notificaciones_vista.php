<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notificaciones_vista extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'notificacion_vista';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idnotificacion_vista';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['iduser','idnotificacion','created_at','updated_at',];
}
