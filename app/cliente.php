<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cliente';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idcliente';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombres','apellidos','email','password','telefono','idempresa','celular','cargo','estado'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function empresa()
    {
        return $this->belongsTo('App\empresa', 'idempresa', 'idempresa');
    }



    public function is_delete(){
        $contrato   = $this->hasMany('App\contrato','Cliente_idcliente','idcliente')->count();
        $rma        = $this->hasMany('App\Rma','idcliente','idcliente')->count();
        $ticket     = $this->hasMany('App\ticket','Cliente_idcliente','idcliente')->count();

        if ($contrato || $rma || $ticket) {
            return false;
        }else{
            return true;
        }
    }
}
