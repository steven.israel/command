<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prioridad extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'prioridadticket';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idPrioridadTicket';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','descripcion','tiempoMinimoContacto','tiempoMaximoContacto','color','tiempo_notificacion',
    ];

    public function is_delete()
    {
        $ticket         = $this->hasMany('App\ticket','PrioridadTicket_idPrioridadTicket','idPrioridadTicket')->count();
        $tipocontrato   = $this->hasMany('App\tipoContrato','prioridadticket_idprioridadticket','idPrioridadTicket')->count();

        if ($ticket || $tipocontrato) {
            return false;
        }else{
            return true;
        }
    }
}
