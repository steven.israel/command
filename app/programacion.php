<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class programacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ticket_programado';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idprogramado';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idticket','descripcion','fecha_programacion','fechaIni','fechaFin','created_at','updated_at',
    ];

    public function ticket()
    {
        return $this->hasOne('App', 'id', 'id_ticket');
    }


}
