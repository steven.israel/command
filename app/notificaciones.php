<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notificaciones extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'notificaciones';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idnotificaciones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idticket','titulo','mensaje','created_at','status','updated_at',];


    public function ticket()
    {
        return $this->belongsTo('App\ticket', 'idticket', 'idticket');
    }
}
