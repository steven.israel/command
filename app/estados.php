<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estados extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'estadoticket';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idEstadoTicket';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','descripcion','idplantilla','color',];
}
