<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class historico extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'historialticket';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idHistorialTicket';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Ticket_idticket','descripcion','tipo','usuario','tipo_usuario','estado_ticket','created_by',
    ];

    public function user()
    {
        return $this->belongsTo('App\usuario', 'usuario', 'id');
    }

     public function ticket()
    {
        return $this->belongsTo('App\ticket', 'Ticket_idticket', 'idticket');
    }

     public function estado()
    {
        return $this->belongsTo('App\estados', 'estado_ticket', 'idEstadoTicket');
    }

     public function priori()
    {
        return $this->belongsTo('App\prioridad', 'estado_ticket', 'idPrioridadTicket');
    }


    public function creador()//campo created_by
    {
        return $this->belongsTo('App\usuario', 'created_by', 'id');
    }
}
